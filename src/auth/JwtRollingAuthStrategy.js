export default class JwtRollingAuthStrategy {
  constructor(auth, options) {
    this.$auth = auth;
    this.name = options._name;

    this.renewalTimer = null;

    this.options = Object.assign({}, DEFAULTS, options);
  }

  _setToken(token) {
    if (this.options.globalToken) {
      // Set Authorization token for all axios requests
      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, token);
    }
  }

  _clearToken() {
    if (this.options.globalToken) {
      // Clear Authorization token for all axios requests
      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, false);
    }
  }

  mounted() {
    if (this.options.tokenRequired) {
      const token = this.$auth.syncToken(this.name);
      this._setToken(token);

      const futureToken = this.$auth.syncToken(this.name + '_future');

      if (futureToken) {
        this._scheduleTokenRenewal(futureToken);
      }
    }

    return this.$auth.fetchUserOnce();
  }

  async login(endpoint) {
    if (!this.options.endpoints.login) {
      return;
    }

    // Ditch any leftover local tokens before attempting to log in
    await this._logoutLocally();

    this._clearRenewalTimer();

    const result = await this.$auth.request(
      endpoint,
      this.options.endpoints.login
    );

    const data = this._getDataFromPath(result);

    const currentToken = data.currentToken;
    const futureToken = data.futureToken;

    this.setNewToken(currentToken, futureToken);
    this._scheduleTokenRenewal(futureToken);

    return this.fetchUser();
  }

  _clearRenewalTimer() {
    if (this.renewalTimer) {
      clearTimeout(this.renewalTimer);
      this.renewalTimer = null;
    }
  }

  setNewToken(currentToken, futureToken) {
    if (this.options.tokenRequired) {
      const token = this.options.tokenType
        ? this.options.tokenType + ' ' + currentToken
        : currentToken;

      this.$auth.setToken(this.name, token);
      this.$auth.setToken(this.name + '_future', futureToken);
      this._setToken(token);
    }
  }

  _scheduleTokenRenewal(futureToken) {
    const exploded = futureToken.split('.');

    const claims = JSON.parse(atob(exploded[1]));

    const clockSkewBufferTime = Math.floor(
      (claims.exp - claims.nbf) * 1000 * 0.25
    );

    const timeToSwitch =
      clockSkewBufferTime + (claims.nbf * 1000 - new Date().getTime());

    this.renewalTimer = setTimeout(
      this._renewToken,
      Math.max(1, timeToSwitch),
      this,
      futureToken
    );
  }

  async _renewToken(self, futureToken, endpoint) {
    self.setNewToken(futureToken);

    if (self._isTokenExpired(futureToken)) {
      self.logout(endpoint);
      return;
    }

    const result = await self.$auth
      .request(endpoint, self.options.endpoints.renew)
      .catch((error) => {
        if (
          error.response &&
          (error.response.status === 401 || error.response.status === 403)
        ) {
          self.logout(endpoint);
        } else {
          setTimeout(self._renewToken, 5000, self, futureToken);
        }
      });

    if (result) {
      const data = self._getDataFromPath(result);

      if (data && data.futureToken) {
        self.setNewToken(data.currentToken, data.futureToken);

        self._scheduleTokenRenewal(data.futureToken);
      }
    }
  }

  _isTokenExpired(futureToken) {
    const exploded = futureToken.split('.');

    const claims = JSON.parse(atob(exploded[1]));

    return claims.exp * 1000 - 500 < new Date().getTime();
  }

  async setUserToken(tokenValue) {
    // Ditch any leftover local tokens before attempting to log in
    await this._logoutLocally();

    if (this.options.tokenRequired) {
      const token = this.options.tokenType
        ? this.options.tokenType + ' ' + tokenValue
        : tokenValue;

      this.$auth.setToken(this.name, token);
      this._setToken(token);
    }

    return this.fetchUser();
  }

  async fetchUser(endpoint) {
    // Token is required but not available
    if (this.options.tokenRequired && !this.$auth.getToken(this.name)) {
      return;
    }

    // User endpoint is disabled.
    if (!this.options.endpoints.user) {
      this.$auth.setUser({});
      return;
    }

    // Try to fetch user and then set
    const user = await this.$auth.requestWith(
      this.name,
      endpoint,
      this.options.endpoints.user
    );
    this.$auth.setUser(this._getDataFromPath(user));
  }

  async logout(endpoint) {
    // Only connect to logout endpoint if it's configured
    if (this.options.endpoints.logout) {
      await this.$auth
        .requestWith(this.name, endpoint, this.options.endpoints.logout)
        .catch(() => {});
    }

    this._clearRenewalTimer();

    // But logout locally regardless
    return this._logoutLocally();
  }

  // eslint-disable-next-line require-await
  async _logoutLocally() {
    if (this.options.tokenRequired) {
      this._clearToken();
    }

    return this.$auth.reset();
  }

  _getDataFromPath(dataObject) {
    if (this.options.dataPath) {
      return dataObject[this.options.dataPath];
    }

    return dataObject;
  }
}

const DEFAULTS = {
  tokenRequired: true,
  tokenType: 'Bearer',
  globalToken: true,
  tokenName: 'Authorization',
};
