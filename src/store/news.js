export const state = () => ({
  news: [],
  allDataIsLoaded: false,
});

export const mutations = {
  SET_NEWS(state, news) {
    state.news = news;
  },
  UPDATE_LOADING_STATUS(state) {
    state.allDataIsLoaded = true;
  },
};

export const getters = {
  getNews(state) {
    return state.news;
  },
  getNewsLoadingStatus(state) {
    return state.allDataIsLoaded;
  },
};

export const actions = {
  async loadNews({ commit, state }) {
    const result = await this.$axios
      .$get(`/news?pageNumber=0&pageSize=4`)
      .catch(() => {});

    if (result && result.content) {
      commit('SET_NEWS', result.content);
      commit('UPDATE_LOADING_STATUS');
    }
  },
};
