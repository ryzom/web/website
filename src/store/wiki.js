export const state = () => ({
  wikiLinks: [],
  allDataIsLoaded: false,
});

export const mutations = {
  SET_WIKILINKS(state, wikiLinks) {
    state.wikiLinks = wikiLinks;
  },
  UPDATE_LOADING_STATUS(state) {
    state.allDataIsLoaded = true;
  },
};

export const getters = {
  getWikiLinks(state) {
    return state.wikiLinks;
  },
  getWikiLinksLoadingStatus(state) {
    return state.allDataIsLoaded;
  },
};

export const actions = {
  async loadWikiLinks({ commit, state }, { displayLanguage }) {
    const result = await this.$axios
      .$get(`/wiki/random-links?amount=15&language=${displayLanguage}`)
      .catch(() => {});

    if (result) {
      commit('SET_WIKILINKS', result);
      commit('UPDATE_LOADING_STATUS');
    }
  },
};
