export const state = () => ({
  events: [],
  allDataIsLoaded: false,
});

export const mutations = {
  SET_EVENTS(state, events) {
    state.events = events;
  },
  UPDATE_LOADING_STATUS(state) {
    state.allDataIsLoaded = true;
  },
};

export const getters = {
  getEvents(state) {
    return state.events;
  },
  getEventsLoadingStatus(state) {
    return state.allDataIsLoaded;
  },
};

export const actions = {
  async loadEvents({ commit, state }) {
    const result = await this.$axios
      .$get(`/event?pageNumber=0&pageSize=50`)
      .catch(() => {});

    if (result && result.content) {
      commit('SET_EVENTS', result.content);
      commit('UPDATE_LOADING_STATUS');
    }
  },
};
