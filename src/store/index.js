export const state = () => ({
  activePage: 'home',
});

export const mutations = {
  SET_ACTIVE_PAGE(state, activePage) {
    state.activePage = activePage;
  },
};

export const getters = {
  getActivePage(state) {
    return state.activePage;
  },

  isAuthenticated(state) {
    return state.auth.loggedIn;
  },

  loggedInUser(state) {
    return state.auth.user;
  },

  hasRole: (state) => (role) => {
    if (
      !state.auth.loggedIn ||
      !state.auth.user ||
      !state.auth.user.adminRoles
    ) {
      return false;
    }
    const userRoles = state.auth.user.adminRoles.split('::');

    return userRoles.some((userRole) => {
      return role === userRole;
    });
  },

  hasAnyRole: (state) => (roles) => {
    if (
      !state.auth.loggedIn ||
      !state.auth.user ||
      !state.auth.user.adminRoles
    ) {
      return false;
    }
    const userRoles = state.auth.user.adminRoles.split('::');

    let found = false;
    roles.forEach((role) => {
      const foundLocal = userRoles.some((userRole) => {
        return role === userRole;
      });

      if (foundLocal) {
        found = true;
        return true;
      }
    });

    console.log(roles, found, userRoles, 'x');

    return found;
  },

  hasAllRoles: (state) => (roles) => {
    if (
      !state.auth.loggedIn ||
      !state.auth.user ||
      !state.auth.user.adminRoles
    ) {
      return false;
    }
    const userRoles = state.auth.user.adminRoles.split('::');

    let found = true;
    roles.forEach((role) => {
      found = userRoles.some((userRole) => {
        return role === userRole;
      });

      if (!found) {
        return false;
      }
    });

    console.log(roles, found, userRoles);

    return found;
  },
};
