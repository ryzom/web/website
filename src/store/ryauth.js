export const state = () => ({
  loggedIn: false,
  user: null,
});

export const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },
  SET_LOGGED_IN(state, loggedIn) {
    state.loggedIn = loggedIn;
  },
};

export const getters = {
  isAuthenticated(state) {
    return state.loggedIn;
  },

  loggedInUser(state) {
    return state.user;
  },
};

export const actions = {
  async login({ commit, state }, { username, password }) {
    const request = new FormData();

    request.set('character', username);
    request.set('password', password);

    let error = null;

    const result = await this.$axios
      .$post('https://app.ryzom.com/api/auth.php', request, {
        withCredentials: true,
      })
      .catch((err) => {
        if (!err.response) {
          error = 'login.error.server';
        } else {
          error = 'login.error.credentials';
        }
      });

    if (result) {
      commit('SET_USER', result);
      commit('SET_LOGGED_IN', true);
    }

    return error;
  },
  async fetchUser({ commit, state }) {
    const result = await this.$axios
      .$get(`https://app.ryzom.com/api/user.php`, { withCredentials: true })
      .catch(() => {
        commit('SET_LOGGED_IN', false);
        commit('SET_USER', null);
      });

    if (result) {
      commit('SET_USER', result);
      commit('SET_LOGGED_IN', true);
    }
  },
  async logout({ commit, state }) {
    await this.$axios
      .$post(`https://app.ryzom.com/api/logout.php`, null, {
        withCredentials: true,
      })
      .catch(() => {});

    commit('SET_LOGGED_IN', false);
    commit('SET_USER', null);
  },
};
