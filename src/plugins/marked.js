import markedReal from 'marked';

function marked(text) {
  const sanitized = text.replace(/<[^>]*>?/gm, '');

  return markedReal(sanitized);
}

export default function(ctx, inject) {
  inject('marked', marked);
}
