import BrickDefinition from 'lib-nuxt-i18n/src/components/BrickDefinition';
import BrickTranslation from 'lib-nuxt-i18n/src/components/BrickTranslation';
import TranslationTemplate from 'lib-nuxt-i18n/src/components/TranslationTemplate';
import Vue from 'vue';

Vue.component('BrickDefinition', BrickDefinition);
Vue.component('BrickTranslation', BrickTranslation);
Vue.component('TranslationTemplate', TranslationTemplate);
