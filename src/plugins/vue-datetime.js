import Vue from 'vue';
import Datetime from 'vue-datetime';

Number.isNaN =
  Number.isNaN ||
  function(value) {
    // eslint-disable-next-line no-self-compare
    return value !== value;
  };

Vue.use(Datetime);
