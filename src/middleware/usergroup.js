import { routeOption } from '../../.nuxt/auth/utilities';

export default function(ctx) {
  // console.log(ctx);

  if (routeOption(ctx.route, 'auth', false)) {
    return;
  }

  if (routeOption(ctx.route, 'auth', 'guest')) {
    return;
  }

  let prefix = '';
  if (ctx.store.$i18n.locale !== ctx.store.$i18n.defaultLocale) {
    prefix = '/' + ctx.store.$i18n.locale;
  }

  if (!ctx.$auth.$state.user || !ctx.$auth.$state.user.adminRoles) {
    ctx.redirect(prefix + '/unauthorized');
  }

  // console.log(ctx.app.router.matcher.match('/'));

  // const allRoles = getRecursiveRoles(ctx);

  // console.log(allRoles, 'a');

  const roles = rolesOption(ctx.route);
  const userRoles = ctx.$auth.$state.user.adminRoles.split('::');

  // console.log(roles, userRoles, hasRole(roles, userRoles));

  if (roles.length === 0) {
    return;
  }

  if (!hasRole(roles, userRoles)) {
    ctx.redirect(prefix + '/unauthorized');
  }
}

export function hasRole(roles, userRoles) {
  if (!roles || !userRoles) {
    return false;
  }

  return roles.some((role) => {
    return userRoles.some((userRole) => {
      return role === userRole;
    });
  });
}

const rolesOption = (route) => {
  let roles = [];

  // console.log(route.matched);

  route.matched.forEach((m) => {
    if (process.client) {
      // Client
      Object.values(m.components).forEach((component) => {
        // console.log(component.options);
        if (component.options && component.options.roles) {
          roles = roles.concat(component.options.roles);
        }
      });
    } else {
      // SSR
      Object.values(m.components).forEach((component) => {
        Object.values(component._Ctor).forEach((ctor) => {
          if (ctor.options && ctor.options.roles) {
            roles = roles.concat(ctor.options.roles);
          }
        });
      });
    }
  });

  // console.log(roles);

  return roles.filter(onlyUnique);
};

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}
