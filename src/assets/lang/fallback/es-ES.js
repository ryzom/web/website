// 2020-05-09 15:00
export default {
  game: {
    atys: {
      text:
        'Atys es un mundo encantador y único, formado por el crecimiento masivo de vegetación con enormes raíces. Poblado por plantas inteligentes, herbívoros inofensivos y temibles depredadores; a sus maravillas se suman sus peligros.',
      title: 'Atys: un mundo vivo!',
    },
    mobs: {
      text:
        'Un poderoso sistema de A.I. permite un comportamiento realista de la vida silvestre: los carnívoros atacan a las manadas de herbívoros, los movimientos realistas y dinámicos de los animales los hacen vivaces, a veces entrañables, otras veces preocupantes. También hay migraciones masivas en los cambios de estación y un comportamiento diferente para las distintas especies. Tómese su tiempo para sentarse y observar la vida a su alrededor, es una vista inolvidable. Pero ten cuidado con los depredadores!',
      title: 'Muchedumbres vivas',
    },
    roleplay: {
      text:
        'Ryzom ofrece eventos interactivos oficiales en los que participan PNJs con capacidad de respuesta en vivo. Tan variados como regulares, su progreso y resultado dependen de tus acciones: ¡nada está predestinado!\n\n¿Te gustaría crear tu propio evento? El Equipo de Animación está siempre a tu disposición y con gusto te apoyará y ayudará en la logística.\n\nAdemás, muchos gremios están dispuestos a abrir sus puertas para ayudar a que tu personaje evolucione!\n\nPara los amantes de los lenguajes imaginarios, los jugadores y el Equipo de Animación crean lenguajes únicos. La inmersión en el juego de rol está garantizada!',
      title: 'Juego de roles para la victoria! Te dejará alucinado.',
    },
    customization: {
      text:
        'Apariencia de los personajes totalmente personalizable en el momento de la creación: ¡dale forma a tu avatar como te guste!',
      title: 'Personalización de caracteres completos',
    },
    classless: {
      text:
        'Los personajes de Ryzom son inusuales en el sentido de que no están vinculados a una clase. Esto significa que no tienes que elegir entre ser un guerrero, un mago, un cosechador o un artesano. Puedes mejorar tus habilidades en todas estas clases con un solo personaje; no necesitas enfocarte en un solo camino. Por ejemplo, puedes, en tu tiempo libre, entrenar el manejo de la pica, ponerte tus amplificadores mágicos para curar o lanzar hechizos ofensivos, recoger tu pica y cosechar materiales, o entrar en la elaboración de una nueva armadura o arma.\n\nTienes libertad absoluta!',
      title: 'Sin clase',
    },
    multiplatform: {
      text:
        'Ryzom, Gratis para jugar, puede descargarse para Windows, Linux y Mac.',
      title: 'Windows, Linux y Mac',
    },
    forage: {
      text:
        'Ryzom cuenta con un avanzado sistema de cosecha y elaboración.\n\nCosecha - te permite excavar materiales del suelo dependiendo de la estación y el clima y de tu habilidad. No es tan simple una vez que los depredadores se involucran!\n\nArtesanía - La cuidadosa combinación de materiales dependiendo de sus características y colores, permite la producción de armas y armaduras únicas. Miles de recetas son posibles.',
      title: 'Forraje y cosecha avanzada',
    },
    pvp: {
      text:
        'Puedes luchar en todas partes, pero sólo cuando quieras hacerlo. Duelos, batallas de puestos de avanzada, peleas de facción o de región... Eres libre de participar en ellos... ¡o de abstenerte!',
      title: 'PvP consensuado',
    },
    community: {
      text:
        'Ryzom está disponible en alemán, inglés, español, francés y ruso, y cuenta con una gran comunidad multilingüe madura, servicial y amigable. Todo el mundo puede hablar y actuar en su propio idioma, por lo que no es obligatorio hablar un idioma extranjero.\n\nÚnete a nosotros para ser parte de la familia!',
      title: 'Una auténtica comunidad',
    },
    title:
      'Entra en un MMORPG de fantasía científica única y sumérgete en un mundo vivo orgánico único!',
    invasions: {
      text:
        'Imagínese, frente a una puerta de la ciudad, mirando una colina lejana y viendo a los exploradores de un enjambre de Kitin. Ahora imagínese a cientos de Kitins siguiéndolos, moviéndose como un ejército unido y en enjambre.\n\nLas grandes batallas requieren la cooperación de una gran parte de la comunidad para contrarrestar estas amenazantes invasiones. ¿Y si un día los Kitin ganaran de nuevo, amenazando la hombría por tercera vez?',
      title: 'Invasiones',
    },
    environment: {
      text:
        'Ryzom es un mundo tridimensional persistente que consiste en cinco ecosistemas exuberantes y variados. El paisaje, el comportamiento de la vida silvestre y la disponibilidad de material del suelo cambian según la estación, el clima y el tiempo.\n\nDescubra la belleza del bosque en primavera, haga la guerra en la selva bajo un cielo de verano tormentoso y lleno de relámpagos, piérdase en las dunas del desierto en otoño o relájese disfrutando de los reflejos de un sol de invierno en el agua pura de los lagos.',
      title: 'Entornos dinámicos',
    },
    freedom: {
      text:
        'En Ryzom, no hay clases para elegir. Desarrolla todas las habilidades de tu personaje de forma armoniosa o elige concentrarte en una habilidad y convertirte en un maestro. No estás obligado a elegir una facción cuando creas tu personaje. Únete a una facción durante tus viajes, o no.\n\nNo tienes que luchar en PvP, pero puedes hacerlo. Únete a un gremio cuando estés listo para ello y crea alianzas, o simplemente comienza tus aventuras solo o con tus amigos.\n\nBusca los materiales más valiosos en los Atys para crear tu propio equipo único con el que te será más fácil defenderte de tus enemigos. Caza a los jefes más poderosos en Atys. Conquista un puesto de avanzada y perfora materiales para tu gremio.\n\nHaz lo que quieras o hazte notar en la política. Desarrolla tu personaje como quieras y empieza tu propia historia.\n\nDepende de ti, decidir qué hacer. Siéntete libre de hacerlo.',
      title: 'Libertad: ¡nada es más valioso!',
    },
    opensource: {
      text:
        'Ryzom es uno de los pocos MMORPGs de calidad comercial que son completamente de código abierto: cliente, servidor, herramientas y medios de comunicación. Ser de código abierto significa que Ryzom se basa en una comunidad vibrante que proporciona correcciones y funcionalidad más allá de lo que el equipo puede proporcionar. Ofrece a los jugadores una oportunidad única de involucrarse en el desarrollo del juego.\n\nPara más información sobre Ryzom Core, visite el [Blog de Ryzom Core](https://ryzomcore.atlassian.net/wiki/spaces/RC/overview). Para ver los recursos artísticos, visite el [Ryzom Media Repository](https://github.com/ryzom/ryzomcore-assets).',
      title: 'Código abierto',
    },
  },
  website: {
    meta: {
      description:
        'Un MMORPG sin clases y orientado a la libertad, en un entorno de fantasía científica centrado en el juego de roles, eventos en vivo, artesanías complejas y entornos dinámicos.',
    },
    title:
      'Ryzom - Free to Play MMORPG de código abierto | Windows, Mac y Linux',
  },
  footer: {
    owner: 'Winch Gate Property Limited',
    copyright: 'Copyright © {año}. Todos los derechos reservados.',
    pegi: {
      online: 'Juego online',
      violence: 'Violencia',
    },
    privacy: 'Política de privacidad',
    tos: 'Términos de Servicio',
    community: {
      bmsite: 'Ballistic Mystix',
      armory: 'Armería Ryzom',
      forge: 'Ryzom Forge',
      wiki: 'Ryzom Wiki',
      title: 'Comunidad',
    },
    follow: {
      youtube: 'Youtube',
      twitter: 'Twitter',
      vimeo: 'Vimeo',
      flickr: 'Flickr',
      facebook: 'Facebook',
      title: 'Sigue a Ryzom',
    },
    support: {
      feedback: 'Comentarios',
      forums: 'Foros',
      contact: 'Contacto',
      title: 'Soporte técnico',
    },
  },
  index: {
    calendar: {
      title: 'próximos eventos',
      viewMore: 'ver todos los eventos',
    },
    news: {
      noNews:
        'Actualmente no hay noticias que mostrar. Por favor, vuelva más tarde.',
      title: 'últimas noticias y actualizaciones',
      viewAll: 'ver todas las noticias',
    },
    features: {
      atys: 'Atys: un mundo vivo!',
      roleplay: 'Juego de roles para la victoria! Te dejará alucinado.',
      freedom: 'Libertad: ¡nada es más valioso!',
      community: 'Una auténtica comunidad',
      title: 'explorar ryzom',
      viewAll: 'ver todas las funciones',
    },
    forge: {
      playersCreate: 'Los jugadores crean',
      openSource: 'código abierto',
      supportRyzom: 'apoyar a ryzom',
    },
    wiki: {
      visit: 'visite Ryzom Wiki',
      title: 'Descubre',
    },
  },
  privacy: {
    preamble: {
      title: 'Privacidad y Protección de Datos',
      content:
        'Winch Gate Property Limited (a la que se hace referencia en el presente como "Winch Gate"), ubicada en 3030 LIMASSOL (Chipre), administra el juego "Ryzom" y el sitio web "[www.ryzom.com](https://www.ryzom.com)". Winch Gate se adhiere a la Regulación General de Protección de Datos (GDPR, por sus siglas en inglés, aplicada antes del 25 de mayo de 2018), respeta la privacidad de sus visitantes en línea y reconoce la importancia de proveer un entorno seguro para la información que recopila.',
    },
    policy: {
      title: 'Política de privacidad',
      content:
        'Winch Gate sólo recogerá los datos personales que sean absolutamente necesarios para la creación y mantenimiento de cuentas y personajes dentro de Ryzom. Winch Gate nunca compartirá con terceros los datos personales de sus visitantes en línea y/o usuarios del juego.\n\n## ¿Qué son los datos personales?\nPor datos personales se entiende cualquier información relativa a una persona física identificada o identificable. Esto incluye direcciones IP y datos de cookies.\n\n## Derechos de los usuarios\nEl usuario debe ser informado de si Winch Gate está recolectando datos, por qué se recolectan los datos, cómo se procesan y utilizan. El usuario debe dar su consentimiento (lea más en "Consentimiento"). Además, el usuario tiene derecho a obtener información sobre sus datos almacenados si así lo solicita. Este servicio es gratuito y puede ser solicitado en cualquier momento por cualquier usuario (lea más en "Derecho de acceso a los datos"). En caso de una fuga, el usuario será informado dentro de 72 horas, así como la oficina de protección de datos responsable (leer más en "Notificación de violación"). El usuario también tiene el derecho a ser olvidado, lo que significa que Winch Gate tiene que borrar todos los datos personales a petición del usuario (más información en "Derecho al olvido").\n\n## Consentimiento\nTiene que haber una solicitud de consentimiento, en un lenguaje comprensible y fácil. Debe estar en una forma fácilmente accesible y se debe adjuntar la finalidad del tratamiento de los datos. El consentimiento tiene que ser distinguible de otros servicios, tiene que ser dado libremente y tiene que ser fácil de retirar, tan fácilmente como fue dado.\n\n## ¿Qué datos estamos recogiendo?\nWinch Gate recopila datos personales principalmente para el sistema de pago del juego "Ryzom". Es necesario, para que el usuario pueda obtener toda la experiencia de una llamada "cuenta premium". Este servicio es voluntario. Además, Winch Gate también recoge datos de vez en cuando para encuestas/encuestas y estadísticas relacionadas con el juego. Las encuestas son voluntarias y en su mayoría anónimas. Sin embargo, el usuario será informado del proceso de recopilación de datos antes de que comience la encuesta y deberá dar un claro consentimiento. Las estadísticas de "Ryzom" están declaradas en el EULA y tienen que ser aceptadas por el usuario. Winch Gate informará al usuario si está recopilando datos personales para las estadísticas de otros servicios ("[www.ryzom.com](https://www.ryzom.com)") y solicitará el consentimiento, en el marco del servicio relacionado.\n\n## ¿Por qué estamos recogiendo datos?\nA través de estas recopilaciones de datos, Winch Gate puede desarrollar estrategias futuras, mejorar la experiencia del usuario, ampliar su oferta y dirigirse a un grupo más específico. En general, se utiliza para optimizar la experiencia de los clientes y usuarios de Winch Gates. Además, algunos datos son necesarios para el proceso de pago.\n\n## Derecho de acceso a los datos\nLos usuarios tienen derecho a acceder a sus datos almacenados. Pueden enviar una solicitud en cualquier momento a [https://support.ryzom.com](https://support.ryzom.com). Winch Gate responderá con información sobre todos los datos personales almacenados lo antes posible. Este servicio es 100% gratuito y no tiene límite de uso. Tenga en cuenta que Winch Gate tiene el derecho de rechazar varias solicitudes relacionadas con la misma persona física (o cuenta) en un período de tiempo muy corto.\n\n## Derecho a ser olvidado\nEl usuario tiene el derecho a ser olvidado, lo que significa que Winch Gate tiene que borrar todos los datos personales almacenados a petición del usuario. Tenga en cuenta que Winch Gate tiene el derecho (y el deber) de oponerse a ello, en el caso de actividades criminales, hasta que las autoridades competentes se involucren y den su consentimiento para el borrado de los datos personales. Además, en caso de que ejerza su derecho a ser olvidado, es posible que algunos productos de Winch Gate ya no estén disponibles para usted, debido a la pérdida de los datos necesarios (por ejemplo, "cuentas premium"). Winch Gate informará al usuario de las consecuencias antes de borrar todos los datos personales de forma permanente, y requerirá el consentimiento del usuario.\n\n## Notificación de infracción\nEn caso de una fuga (hacking), Winch Gate enviará una notificación de incumplimiento a cualquier usuario involucrado y a la oficina de protección de datos responsable, dentro de las 72 horas de haber tenido conocimiento del incumplimiento. Esta notificación contendrá información sobre cuándo se detectó la violación, qué persona física y qué datos están afectados, así como qué medidas pueden haberse tomado ya.\n',
    },
  },
  login: {
    password: 'Password',
    error: {
      server: 'error del servidor',
      credentials: 'credenciales inválidas',
    },
    username: 'Nombre de la cuenta',
  },
  menu: {
    forums: 'foros',
    gameInfo: 'el juego',
    universe: 'universo',
    wiki: 'wiki',
    events: 'eventos',
    home: 'Inicio',
  },
  button: {
    downloadRyzom: 'Descarga Ryzom',
    continueSaga: 'Continúa la Saga',
    login: 'iniciar sesión',
    signUp: 'Crear una cuenta',
  },
  download: {
    systemRequirements: {
      os: {
        title: 'Sistema operativo',
      },
      linux: {
        sound: 'Compatible con OpenAL',
        cpu: 'Un núcleo de 1 GHz',
        hdd: '10 GB de espacio disponible',
        graphics: 'OpenGL 1.2 con 32 MB VRAM',
        title: 'Linux',
        ram: '2 GB de RAM',
      },
      sound: {
        title: 'Tarjeta de sonido',
      },
      cpu: {
        title: 'Procesador',
      },
      hdd: {
        title: 'Almacenamiento',
      },
      graphics: {
        title: 'Gráficos',
      },
      title: 'Requisitos del sistema',
      windows: {
        os: 'Windows Vista',
        sound: 'Compatible con DirectX',
        cpu: 'Un procesador de 1GHz',
        hdd: '10 GB de espacio disponible',
        graphics: 'OpenGL 1.2 o DirectX 9.0 con 32 MB VRAM',
        title: 'Windows',
        directx: 'Versión 9.0c',
        ram: '2 GB de RAM',
      },
      minimum: 'mínimo',
      directx: {
        title: 'DirectX',
      },
      mac: {
        os: 'OS X 10.8',
        sound: 'Compatible con OpenAL',
        cpu: 'Un procesador de 1GHz',
        hdd: '10 GB de espacio disponible',
        graphics: 'OpenGL 1.2 con 32 MB VRAM',
        title: 'Mac OS',
        ram: '2 GB de RAM',
      },
      ram: {
        title: 'Memoria',
      },
    },
    title: 'Descargar Ryzom',
  },
  premium: {
    button: {
      manageSub: 'Administre su suscripción',
    },
    sub: {
      button: 'añadir una suscripción',
      title: 'Ya  \n¿Jugando?',
    },
    compare: {
      title: '¡Compare! La elección es tuya.',
    },
    comparison: {
      premium: {
        maxlevel: 'Nivel máximo 250',
        xp: 'Doble XP',
        maxquality: 'Máxima calidad del equipo 250',
        storage:
          '* Una montura y tres empaquetadores\n* Un apartamento\n* Una sala del gremio',
        title: 'premium',
        support:
          '* Asistencia del equipo de soporte\n* Restauración de cuentas antiguas (suscritas antes de la fusión de la\nservidores de juegos) para recuperar los personajes\n* Restauración del carácter o los caracteres en caso de supresión voluntaria o accidental\n* Transferencia de carácter(es) entre dos cuentas Premium pertenecientes a la misma persona\n* Se permite un cambio de seudónimo cada seis meses para cada personaje\n\n',
      },
      free: {
        maxlevel: 'Nivel máximo 125',
        xp: 'XP normal',
        maxquality: 'Máxima calidad del equipo 150',
        storage: 'Una montura',
        title: 'gratis',
        support:
          ' * Asistencia del equipo de soporte\n * Se permite un cambio de seudónimo para cada personaje',
      },
    },
    intro: {
      text:
        'Ryzom ha abierto el juego al mundo de lo gratuito, liberando su código y convirtiéndose en el único MMORPG de código abierto, jugable en Windows, Linux y Mac.\n\nSin embargo, el mantenimiento de Ryzom implica costes, como la compensación del pequeño equipo de empleados y el pago de los servidores. Por eso, aunque Ryzom se puede jugar como Free to Play, las suscripciones son vitales para que continúe.\n\nPara agradecer a los jugadores que deciden suscribirse, se desbloquean ciertas características del juego y se les ofrecen ciertos bonos: la oportunidad de nivelar sus personajes hasta el nivel 250 en lugar de 125, el duplicar todos los puntos de experiencia ganados y el acceso a varios otros medios de almacenamiento.\n\n**Para ayudar a Ryzom, elige la opción de suscribirse.',
      title: '¿Por qué pagar?',
    },
    rates: {
      perMonth: 'mes',
      m1: {
        title: 'Suscripción de 1 mes',
      },
      m3: {
        title: 'Suscripción de 3 meses',
      },
      y1: {
        title: 'Suscripción de 1 año',
      },
      m6: {
        title: 'Suscripción de 6 meses',
      },
      title: 'Tarifas',
    },
    donate: {
      button: 'aportar una donación',
      title: 'Ayude a la Saga  \n¡Continúa!',
    },
    title: 'Apoya a Ryzom y benefíciate!',
    signup: {
      button: 'Regístrate y empieza a jugar',
      title: 'Interesado en  \n¿Ryzom?',
    },
  },
  returning: {
    download: {
      text:
        '¿Necesitas conseguir el juego de nuevo? Descarga Ryzom GRATIS y continúa tus aventuras en Atys hoy mismo!',
      title: 'Descargar  \nJuego',
      modal: {
        onSteam: 'ryzom en steam',
        forMac: 'para mac',
        forWin: 'para windows',
        forLinux: 'para linux',
      },
    },
    recovery: {
      text:
        '¿No puede recordar su nombre de cuenta o su contraseña? Nosotros podemos ayudarle!',
      title: 'Cuenta  \nRecuperación',
    },
    resub: {
      link: 'Leer más',
      text:
        'Aunque no necesitas una suscripción activa para iniciar sesión y chatear con tu gremio y amigos, sí necesitas una para jugar con personajes de nivel superior a 125.',
      title: 'Renovar  \nSuscripción',
    },
    login: {
      title: 'Cuenta  \nlogin',
    },
    title:
      '¡Es bueno tenerte de vuelta! Si tienes algún problema, los tres enlaces de abajo te ayudarán. Ahora vete, tus amigos te están esperando!',
    headline: 'Entrando en el juego',
    register: {
      text:
        '¿Eres nuevo en Ryzom? Haz clic aquí para registrarte y crear una nueva cuenta. ¡Estarás listo para jugar en minutos!',
      title: 'Crear  \nnueva cuenta',
    },
  },
  universe: {
    inhabitants: {
      fauna: {
        text:
          'Conocerás varias especies animales dentro de los diversos Ecosistemas Atysianos.\n\nSi te tomas el tiempo de observarlos, descubrirás una fauna viva y realista, con sus pacíficas manadas de herbívoros que migran con las estaciones, depredadores que intentan regularmente ataques aislados, la presa que se defiende en su mejor momento. Usted mismo podrá estar en el centro de la acción como el depredador... o la presa!',
        title: 'Fauna',
      },
      races: {
        fyros: {
          text:
            'Los Fyros dicen ser el pueblo guerrero de los Atys. Ansiosos por la aventura, luchando y cazando contra aquellos que consideran dignos de desafiar. También están hambrientos de conocimiento y deseosos de descubrir los misterios del pasado de Atys. De naturaleza poco ferviente, su relación con la religión siempre estuvo marcada por su pragmatismo.\n\nLa mayoría de los Fyros habitan el desierto, dentro del cual establecieron su imperio. Valoran la Verdad, el Honor, la Disciplina y la Justicia. Cumplir con su rito de ciudadanía permite que uno se convierta en un patriota de los Fyros y se beneficie de varios bienes y recompensas.\n\nSu lengua ancestral es el fyrk.',
          title: 'Fyros',
        },
        matis: {
          text:
            'Los Matis son un pueblo sofisticado, culto y ambicioso. Dominan la botánica y construyen sus casas y herramientas a partir de plantas vivas. Tanto artistas como conquistadores, creen que cada homínido debe ganarse su lugar dentro de la sociedad. Son los más fieles seguidores del Karavan.\n\nLa mayoría de los Matis viven dentro del Bosque, dentro del cual establecieron su Reino, una monarquía de derecho divino. Valoran la Lealtad al Rey y a Jena, el logro estético y la Competencia. El rito de ciudadanía permite convertirse en vasallo Matis y beneficiarse de varios bienes y recompensas.\n\nSu idioma es el Mateis.',
          title: 'Matis',
        },
        text:
          'Atys está habitado por muchos seres sensibles. Allí viven los Kamis, misteriosas criaturas transformadoras dedicadas a la protección de la naturaleza; los apóstoles del Karavan, humanoides acorazados que controlan una tecnología desconocida; y finalmente, los homins, que, a excepción de un puñado de tribus primitivas, son como ellos diseminados en cuatro naciones: Fyros, Matis, Tryker y Zoraï.',
        title: 'naciones',
        tryker: {
          text:
            'Los más pequeños por su altura, los Trykers son pacíficos, dedicados a la libertad y bon vivant. Sueñan con un mundo pacífico sin amos ni esclavos. Su curiosidad los hace excelentes exploradores e inventores, y dominan completamente la tecnología de los molinos de viento. Su interpretación personal de la religión a menudo despierta la ira de los dogmáticos.\n\nLa mayoría de los Trykers llaman a las Tierras de los Lagos su hogar, donde establecieron su Federación, encabezada por un Gobernador. Ellos valoran la Libertad, la Igualdad y el Compartir. El hecho de cumplir con su rito de ciudadanía permite que uno se convierta en un ciudadano de los Trykers y disfrute de varios beneficios y recompensas.\n\nTienen su propio idioma, el Tryker Tyll.',
          title: 'Tryker',
        },
        zorai: {
          text:
            'Por su naturaleza tranquila y espiritual, los Zoraïs son los más altos de todos los homínidos. La máscara les cubre el rostro, un regalo sagrado de los Kami, a los que sirven fervientemente. Su deber sagrado es asegurar el equilibrio de la naturaleza y luchar contra el Goo, la plaga de la selva. Siempre buscando el conocimiento, dominan el magnetismo.\n\nLa mayoría de los Zoraï permanecen en la selva, donde establecieron su teocracia, dirigida por el Gran Sabio. Valoran los logros espirituales, la sabiduría y el respeto por la naturaleza. El rito de la ciudadanía permite convertirse en un Iniciado Zoraï y beneficiarse de diversos bienes y recompensas.\n\nHablan el Taki Zoraï.',
          title: 'Zoraï',
        },
      },
      flora: {
        text:
          'Atys alberga una abundante flora, incluyendo varias especies de plantas inteligentes.\n\nEstas plantas te proporcionarán materiales útiles para tus manualidades, pero tendrás que aprender a escapar de sus formidables ataques, ¡incluso los mágicos!',
        title: 'Flora',
      },
      floraFauna: {
        text:
          'Atys está poblado por muchas especies animales y vegetales. Su caza (o "recolección") permite a los homínidos recoger las materias primas necesarias para la fabricación de artesanía: pieles, colmillos, huesos, cuernos, ligamentos, pezuñas, ojos, uñas... Todo sirve para fabricar ropa, armas, joyas...\n\nPara aprovechar estos restos tendrás que aprender, por ti mismo o de tus mayores, diferentes técnicas de caza, ya que cada especie animal o vegetal tiene su propio comportamiento en el combate.',
        title: 'La fauna y la flora',
      },
      kitins: {
        text:
          'Los animales más peligrosos para los homíns son sin duda los kitins. Organizados, poderosos y despiadados, estos gigantescos insectívoros con una elaborada organización social han asolado Atys en dos ocasiones, matando a los homínidos indiscriminadamente y aniquilando civilizaciones enteras.\n\nDesde entonces, los Homins se reconstruye paso a paso, pero el peligro acecha constantemente...',
        title: 'Kitins',
      },
      title: 'Los habitantes',
    },
    planet: {
      text:
        'Explorar este mundo te llevará a través de sus múltiples ecosistemas, desde el ardiente desierto hasta los sublimes lagos, desde el majestuoso bosque hasta la exuberante jungla devastada por una plaga mortal, e incluso hasta las oscuras profundidades del planeta, surcadas por ríos de savia.\n\nSalga a conquistar Atys o venga a descubrir sus misterios y sus leyendas! Pero ten cuidado, porque el peligro acecha en cada curva de estos paisajes idílicos.',
      title: 'El planeta',
    },
    continents: {
      forest: {
        text:
          'El bosque es el dominio de los Matis, que han construido allí su reino y han modelado su capital, Yrkanis, en los árboles vivos.\n\nConocido como las Alturas de Verdant, este continente está cubierto de impresionantes y majestuosos árboles y se beneficia de un clima templado puntuado por cuatro estaciones pronunciadas.',
        title: 'Bosque',
      },
      jungle: {
        text:
          'Los Zoraïs han construido sus ciudades en el laberinto vegetal de la selva, alrededor de su capital, Zora.\n\nA este continente también se le llama el Witherings, ya que está infectado por el hedor del Goo, un cáncer que erosiona la vida de manera inexorable, devastando el hábitat y mutando la flora y la fauna en criaturas horribles.',
        title: 'Jungla',
      },
      lakes: {
        text:
          'El archipiélago de los Lagos incluye una miríada de islas habitadas por los Trykers. Aquí han fundado su Federación y construido ciudades flotantes, cuya capital es Fairhaven.\n\nEste continente, conocido como Aeden Aqueous, ofrece un paisaje idílico con sus playas de aserrín blanco, sus magníficas cascadas y sus aguas cristalinas hasta donde alcanza la vista. El clima es soleado durante todo el año.\n',
        title: 'Tierras de los lagos',
      },
      roots: {
        text:
          'Las Raíces Primarias son el ecosistema más misterioso de los Atys. Sumergido en las profundidades del planeta se encuentra el enorme continente subterráneo, su suelo, paredes y techo están poblados por plantas fosforescentes y extrañas criaturas.\n\nTan bellas como peligrosas, las Raíces Primarias esconden en sí mismas los enigmáticos restos del pasado y albergan preocupantes nidos de Kitins. El clima es permanentemente fresco y húmedo.',
        title: 'Raíces Primarias',
      },
      desert: {
        text:
          'Este continente, conocido como el Desierto Ardiente, es el dominio de los Fyros que han fundado su Imperio, alrededor de la fortaleza capital de Pyr.\n\nFuertes y violentos vientos, largos períodos de sequía, temperaturas sofocantes durante el día y heladas por la noche, hacen de estas dunas humeantes uno de los lugares más inhóspitos de los Atys.',
        title: 'Desierto',
      },
      title: 'Los continentes',
    },
    factions: {
      kami: {
        text:
          'Incluye a todos los Discípulos de los Kamis, los cuerpos protectores constantemente ligados al Kami supremo, Ma-Duk.\n\nCualquier homín que desee entrar en la facción de los Kami debe completar un rito de lealtad y jurar respetar las Leyes de Ma-Duk.\n\nLos Kami ofrecen varios servicios a sus Discípulos, siendo los dos más importantes la resurrección y la teletransportación en todas las regiones de Atys. También ofrecen recompensas únicas a aquellos que luchan contra los seguidores de Karavan en su nombre.\n\nLos Kamis se enfrentan regularmente a los Karavan durante las batallas de los puestos avanzados, con el objetivo de explotar sus valiosos recursos, utilizando los impresionantes y ecológicos agujeros en los árboles proporcionados por los Kami.',
        title: 'Kami',
      },
      ranger: {
        text:
          "Incluye a todos los Homins con el deseo de liberar a Hominidad de la amenaza Kitin.\n\nEl gremio de los Rangers de Atys, creado tras el Primer Gran Enjambre, está impulsado por un ideal de hermandad. Los Rangers creen que los Homins deben vivir en paz, sin divisiones; por lo tanto, se prohíben participar en las peleas entre homins, y se niegan a ponerles la mano encima, excepto en caso de defensa propia.\n\nAdemás de patrullar interminablemente en los Atys, los Rangers también tienen bases estratégicas en la isla de Silan y en el bosque de Almati. El campamento de Silan, gestionado por Chiang, acoge a los refugiados que llegan a las Nuevas Tierras. El campamento en el bosque de Almati, dirigido por el guardabosques Orphie Dradius, se usa como centro logístico y vigila la cercana Kitin's Lair.\n\nDurante el Segundo Gran Enjambre, los Rangers ayudaron a salvar a Hominity. Sus planes de batalla permitieron un rescate seguro de la población convenciendo a los Kamis, los Karavan, las Naciones y los Trytonistas para que colaboraran.\n\nMientras hacen todo lo posible para evitar un tercer Gran Enjambre, los Rangers vigilan de cerca a los Kitins, eliminan a sus curiosos exploradores y desarrollan nuevas técnicas de lucha y tácticas discrecionales.",
        title: 'Ranger',
      },
      neutral: {
        text:
          'Algunos homins han elegido no aliarse con ninguna de las dos Potencias, pero no luchar contra ellas: son religiosamente neutrales. \n\nAunque oficialmente no constituyen una facción, la neutralidad religiosa que los une lejos de Ma-Duk y Jena se ve a menudo como tal.',
        title: 'Neutral',
      },
      karavan: {
        text:
          'Incluye a todos los seguidores del Karavan, los discípulos homin de la diosa Jena.\n\nCualquier homínido que quiera entrar en la facción de Karavan debe cumplir con un rito de lealtad y jurar respetar las Leyes de Jena.\n\nEl Karavan ofrece varios servicios a sus seguidores, siendo los dos más importantes la resurrección y el teletransporte en todas las regiones de Atys. También ofrecen recompensas únicas a aquellos que luchan contra los Discípulos Kami en su nombre.\n\nLos Karavan se enfrentan a los Kamis regularmente durante las batallas de los puestos avanzados, con el objetivo de explotar sus valiosos recursos, utilizando sus enormes taladros metálicos proporcionados por los Karavan.',
        title: 'Karavan',
      },
      text:
        'Dos Poderes, diferentes pero ambos misteriosos, buscan controlar el destino de los Atys: los Karavan, que velan por los homíns y propagan la palabra de la diosa Jena, y los Kamis, guardianes de las plantas y del equilibrio del mundo. Ambos poseen poderes únicos, a través de los cuales protegen y ayudan a los homíns que siguen sus leyes divinas. Durante siglos, estas dos Potencias se han observado mutuamente sin enfrentarse. Su protección será de gran ayuda para ti durante tu viaje en los Atys, y su ira puede ser mortal... Por lo tanto, escoge tu lado con cuidado.\n\nSin embargo, si eliges, bajo tu propio riesgo, no servir a ninguno de los dos Poderes, otras puertas están abiertas para ti. Puedes elegir la Neutralidad Religiosa, y no tomar partido en la lucha religiosa, o convertirte en un Trytonista y luchar en las sombras para liberar a la Hombría de las Potencias, o incluso unirte a las filas de los Marauders y luchar tanto contra las Potencias como contra las Naciones. Tu camino también podría llevarte al de los Rangers, que confían en la unificación de las Potencias y las Naciones para luchar contra el enemigo Kitin.',
      title: 'las facciones',
      marauder: {
        text:
          'Incluye a todos los Homins cuya ambición es conquistar los Atys revelándose contra la dominación de las Potencias y las Naciones. La Facción Marauder nació de los clanes de los Homins abandonados en las Tierras Antiguas durante el Primer Gran Enjambre.\n\nEsta sociedad, regida por la supervivencia del más fuerte, se rige por la Combatividad, la Conquista y la Supervivencia. Con el tiempo, los Marauders han desarrollado su propio lenguaje: Marund.\n\nLos Marauders no son dueños de ningún territorio en las Nuevas Tierras; por lo tanto, han levantado un campamento en una región reclamada tanto por los Fyros como por los Matis.\n\nCualquier homín dispuesto a entrar en la facción Marauder debe completar un rito de lealtad y jurar responder a las llamadas de Melkiar, el Varín Negro, líder de guerra de los Clanes Marauder. A cambio, se benefician de servicios específicos para la resurrección y la teletransportación alrededor de Atys y a sus guerreros se les ofrecen recompensas únicas.\n\nLos Marauders intentan regularmente conquistar puestos avanzados a través de batallas, con el objetivo de explotar sus valiosos recursos.',
        title: 'Marauder',
      },
      trytonist: {
        text:
          'Incluye a todos los Homins que quieren liberar a los Atys del yugo de los Poderes, considerando las atractivas recompensas que ofrecen como meros señuelos para esclavizar a la Hominidad.\n\nEl guía de los Trytonistas es Elias Tryton, un misterioso ser considerado por algunos como un renegado de Karavan, y por otros como el esposo de la diosa Jena.\n\nDebido a que son perseguidos por los Karavan y apenas tolerados por los Kamis, los Trytonistas tienen que esconderse para poder sobrevivir. Los Trytonistas están actualmente esparcidos a través de gremios de todas las religiones y naciones, y han desarrollado una red secreta usando nombres en código.\n\nLos Trytonistas reciben sus instrucciones del Gremio de Elías, cuyo líder, Nicho, está en contacto directo con Elias Tryton.\n\nAdvertencia: esta facción es actualmente jugable sólo a través de juegos de rol.',
        title: 'Trytonist',
      },
    },
  },
  header: {
    forums: 'Foros',
    logout: 'cerrar sesión',
    playNow: '¡juega ahora!',
    login: 'iniciar sesión',
    cookies: {
      button: {
        decline: 'Continúe sólo con las galletas necesarias.',
        accept: 'Acepta todas las galletas.',
      },
      text:
        'Ryzom está utilizando cookies con fines de seguimiento y comercialización.',
    },
    webig: 'Web-Apps',
    billing: 'Cuenta',
  },
  tos: {
    coc: 'Código de Conducta',
    intro:
      'Las siguientes normas cubren las situaciones más comunes, pero no son exhaustivas. El equipo de Soporte tiene derecho a enmendar estas normas en caso de que surja una nueva situación que requiera mayor aclaración.\n\nSi un caso es demasiado específico para aparecer en el reglamento, pero que sin embargo requiere de la intervención del Soporte, este tendrá capacidad de actuar y de promulgar, en cualquier momento, advertencias o sanciones, sin que esté obligado a informar a toda la comunidad.\n\nSi alguien observa que  se saltó el reglamento, debe informar al Soporte mediante uno de los siguientes medios:\n- el sistema de ticket en el juego\n- un mensaje a un miembro del servicio de asistencia técnica (llamado en abreviación: CSR)\n- en el juego ( para saber que CSR esta conectado teclea */who gm*, y luego */tell &lt;un CSR presente&gt;*)\n- el chat.ryzom.com\n- un correo electrónico enviado a support@ryzom.com',
    title: 'Términos de Servicio',
    content:
      '## El equipo de Soporte\nCompuesto de jugadores voluntarios bajo la cláusula de no divulgación (NDA), sus miembros no tienen el derecho de revelar su identidad a otros jugadores ni siquiera a los otros miembros de cualquier otro equipo de Ryzom. Si un jugador piensa conocer la identidad de jugador detrás de un miembro del servicio de asistencia técnica (CSR) o detrás de otro miembro de un equipo de Ryzom bajo la cláusula de no divulgación, está prohibido divulgarlo, bajo pena de sanciones (violación mayor, véase B. Advertencias y sanciones).\n\nSus miembros son los encargados de resolver los diferentes problemas relacionados con el juego (técnica, disciplina, etc.). Ellos deben comportarse con amabilidad y cortesía en todo momento. Si a pesar de todo alguien tienes una disputa con un miembro del servicio de asistencia técnica, se puede presentar una queja debidamente documentado al support@ryzom.com que será remitido al Responsable del Soporte. Cualquier denuncia abusiva en contra de un miembro del Soporte puede ser sujeto a una sanción.\n\nCuando un miembro del Soporte entra en comunicación contigo, hay que seguir sus instrucciones y responder con amabilidad y cortesía. Cualquier violación será sancionada de acuerdo a la escala de sanciones previstas (véase B. Advertencias y sanciones).\n\nRecordamos que los miembros del equipo del Soporte están dando de su tiempo (tanto en el juego como en la vida real) para que todos puedan jugar en paz :trate de tener esto en cuenta cuando contacte a un miembro del Equipo. Esto es, por supuesto, válido para todos los equipos: CSR, dev, animación, etc.\n\nEl equipo de Soporte (CSR) está sujeto a normas estrictas que prohíben a sus miembros presente en el juego, de meterse en equipo de jugadores, de intercambiar, de luchar en un duelo con jugadores y de resucitarlos. Estas reglas también prohíbe a cualquier miembro del Soporte utilizar sus poderes para cualquier propósito distinto de los previstos en el marco de su función bajo la pena de destierro de su cuenta de CSR, e incluso de su cuenta de jugador. Además, el CSR no tiene el derecho a arreglar los tickets en los cuales participen sus propios jugadores o gremios.\nDe acuerdo con estas reglas, herramientas de control graban todos los comandos utilizados por el CSR (así como por los miembros de otros equipos) y los muestran internamente para permitir un control en tiempo real. Cada conexión de un miembro del Soporte (y otros equipos) es también visible para que todo el mundo puede comprobar que nadie ha usurpado su cuenta.\n\nTodas las decisiones tomadas por el equipo de Soporte son decisiones colectivas y así varios de sus miembros pueden contactar el jugador por el mismo problema, dependiendo de su naturaleza.\n\n## A. Disputa con un jugador\nEn el caso de una disputa con otro jugador, aconsejamos de empezar por intentar a dialogar y resolver el conflicto de forma amistosa con el jugador en cuestión. Un ticket debe ser un recurso de última instancia, sólo si es imposible establecer el diálogo y que es necesario que un mediador intervenga.\n\nSi se tiene que llegar al ticket, nos parece indispensable que una captura de pantalla sea añadida al ticket como referencia. **Es esencial que esa captura de pantalla muestra la pantalla completa del juego y que no ha sido editado (por censura o otro motivo) con el fin de ser admisible**. Se debe mostrar claramente el abuso que se desea señalar. Siempre hay que tener en cuenta que una captura de pantalla mostrando el mapa abierto permite de ver la fecha Atysiena la cual puede fácilmente dar la fecha real. También aceptamos videos. Para cualquier duda, se puede contactar con el equipo en el juego a través de */tell &lt;CSR nombre&gt;* si un CSR está conectado, o a través de Rocket chat (https://chat.ryzom.com/direct/CSR_nombre), o, por último, a través de correo electrónico a la siguiente dirección: support@ryzom.com\n\n**NOTA**: Cualquier abuso del sistema de tickets será documentado y sancionado.\n\n## B. Advertencias y sanciones\nEl equipo de Soporte se reserva el derecho a imponer advertencias o sanciones de acuerdo a la gravedad de la infracción, la reacción del jugador acusado con respecto al equipo de Soporte y sus pasivos (sanciones o advertencias previas). Así, un jugador puede recibir una simple advertencia, una advertencia formal, una suspensión que van desde las 24 horas hasta varios días, o una prohibición permanente.\n\n### Escalas de sanciones\n\n#### Infracciones menores\nLa escala de sanciones para **infracciones menores** empieza con una advertencia oficial, luego de una suspensión de 24 horas, tres días, una semana, y finalmente dos semanas. Puede estar acompañadas de un mudo (imposibilidad de escribir en un chat) de duración creciente en el lugar de la infracción (canales del juego, Rocket.Chat, foros, etc.).\n\nEsta escala de sanciones se restablece después de un año sin penalización.\n\n#### Infracciones mayores\nLa escala de sanciones para **infracciones mayores** empieza con una suspensión de una semana, un mes, finalmente un destierro permanente de la cuenta. Puede estar acompañada de una expulsión desde el servidor si es necesario.\n\nEsta escala de sanciones se restablece después de tres años sin penalización (a excepción de la prohibición permanente).\n\n#### Infracciones crítica\nEl **destierro permanente** de la cuenta después de un aviso es notificado al jugador por e-mail.\n\n**NOTA**: En caso de sanción grave e inmediata (suspensión o destierro), el jugador infractor no puede ser contactado en el juego, pues lo será a través de un correo electrónico en la dirección de correo electrónico indicada en la cuenta personal, de ahí la importancia de proporcionar una buena dirección de correo electrónico durante el proceso de registro. Si el jugador en cuestión no recibe el correo electrónico o si se quiere discutir de la sanción, él puede ponerse en contacto con el equipo de Soporte en la siguiente dirección: support@ryzom.com\n\n### Avisos legales\nLas sanciones previstas en el Código de Conducta de Ryzom se asignarán de la siguiente manera:\n- El *silencio (mute)* y las advertencias verbales pueden ser aplicadas por todos los CSR.\n- Las advertencias oficiales (por correo electrónico) pueden ser aplicadas por todos los CSR desde el grado GM.\n- Las suspensiones sólo pueden ser aplicadas por los CSR a nivel de SGM.\n- Las prohibiciones de cuentas sólo pueden ser decididas y aplicadas por Winch Gate Limited.\n\n## C. Normas del Código de Conducta\n### I. Normas de cortesía\n\n#### I. 1. El acoso de un jugador o de un miembro del Equipo de Ryzom (infracción mayor o menor: dependiendo de la gravedad de los hechos, el equipo de Soporte decidirá al caso-por-caso)\n- La provocación y el hostigamiento, ya sea verbal o por acciones en el juego, están prohibidas en el juego mismo y en los canales de chat de Ryzom : insultos, amenazas de cualquier tipo (tras la forma de juego de rol o no), la provocación a través de animaciones (*emote*) u otros, sino también por el *chain kill* (matar en cadena) a un personaje, sus hogueras y sus mektubs (excluyendo batallas de puesto avanzado), pulls de mobs (atraer criaturas sobre alguien), todo eso , regularmente o a repetición sobre el mismo personaje para molestarle e impedirle de jugar normalmente... Esta lista no es exhaustiva.\n- El equipo de Apoyo se reserva el derecho a elaborar un dossier sobre el caso si el acoso se considera efectivo, así como a tomar las decisiones necesarias en consecuencia de la gravedad de los hechos.\n\n#### I. 2. La calumnia o la propagación de rumores en contra de un miembro del Equipo Ryzom o de Winch Gate (infracción mayor o menor: dependiendo de la gravedad de los hechos, el equipo de Soporte decidira al caso-por-caso)\n- El hecho de que un rumor, falsas acusaciones o mentiras, en contra de un miembro del Equipo Ryzom o de Winch Gate con el propósito de dañarles de cualquiera manera que sea, está estrictamente prohibido.\n- El equipo de Apoyo se reserva el derecho de tomar las decisiones necesarias en vista de la gravedad de los hechos.\n\n#### I. 3. Los insultos, la grosería, la falta de respeto hacia un jugador o un miembro del Equipo de Ryzom (delito menor)\n- Está estrictamente prohibido insultar, ser grosero o faltar de respeto hacia otros jugadores, ya sea directamente o indirectamente, con el pretexto de jugar el papel por ejemplo, o en un idioma que no se entiende. El equipo de Apoyo se reserva el derecho a determinar la gravedad de los insultos intercambiados, y tomar medidas proporcionadas.\n\n#### I. 4. La no-obediencia a las directrices de un miembro del equipo de Soporte (delito menor)\n- Cuando un miembro del equipo de Soporte comunica con un jugador, esté debe responder tan rápidamente como sea posible y seguir las instrucciones que le fueron dadas, de lo contrario se incurrirá en una sanción.\n\n#### I. 5. La perturbación de un evento en el juego (delito menor)\n- Por respeto a los jugadores que participan en un evento y el Equipo de Animación que lo ha preparado, se pide a los jugadores ajenos a este evento no venga a molestar con palabras o con acciones desplazadas o apartado del tema. Cualquier uso indebido será sancionado.\n- Si uno desea interactuar con un evento programado, por favor que se ponga en contacto con el Equipo de Animación (events@ryzom.com), que juzgará la admisibilidad de su solicitud.\n\n#### I. 6. Compartir cuenta (no delito)\n- Compartir su cuenta está totalmente desaconsejado. El Soporte es responsable por completo de los problemas que se deriven de tales intercambios (robo de objetos, explotación de la cuenta de otro jugador, etc).\n\n#### I. 7. El mal comportamiento en un canal de chat del juego (Servidor de juego, IRC, foros, y Rocket gato incluido) (delito menor)\n- Los canales universo (en cualquier idioma) son en primer lugar canales de ayuda mutua (preguntas y respuestas sobre el juego, solicitud de ayuda, de orientación, de resurrección, etc.). También están abiertas a cualquier discusión relevante para la mayor parte de la comunidad. Para la discusión de cualquier otra naturaleza, por favor, utilice el [/tell] o un canal privado.\n- Spam y flood (comportamiento abusivo por la repetición desmesurada de algún mensaje en un corto espacio de tiempo) son moderados. Troll y flame están prohibidos.\n- Acosos, amenazas y cualquier otro acto que molesta a otro jugador o miembro de un equipo de Ryzom es prohibido.\n- Palabras groseras, abusivas, calumniosas, difamatorias, obscenas, xenófobas, antisemitas, racistas, etc. están prohibidas. Igualmente, casi cualquier palabras sexualmente explícitas, están prohibidas.\n- No puedes sustituirte a cualquier empleado de Winch Gate, CSR o otro miembro de los equipos de Ryzom.\n- No debes violar ninguna de las leyes, sean locales, nacionales, europeas o internacionales.\n- La transferencia ilegal de documentos, archivos o cualquier otro está prohibido a través de la red de los sitios de Winch Gate.\n- Cuando, en el juego, o en cualquier otro servicio de Ryzom, un miembro del equipo de Soporte se comunica contigo, debes responderle y seguir sus instrucciones.\n- No puedes utilizar los servicios de Winch Gate para actividades distintas de las permitidas en el mundo del juego.\n- Si la información de tu perfil es falsa, incorrecta o incompleta, en algunos casos, no te sera possible de aprovechar de un apoyo, porque los CSR no serán capaz de identificarte con certeza.\n- No debes revelar los datos personales de otros jugadores, ya sea en el juego, en los foros de Winch Gate, a través del IRC o en el Rocket.chat.\n\n\n##### Cortesía en el juego:\n- No debes violar el Código de Conducta.\n- El acoso, en cualquiera de sus formas, está prohibido.\n- No debes molestar otro jugador durante sus fases de juego.\n- Debes tener un comportamiento respetuoso hacia los otros jugadores y los miembros del Equipo de Ryzom.\n- No tiene que esconderte detrás de un "papel" para transgredir las normas de cortesía.\n\n##### Reglas en los foros:\n- No debe violar el Código de Conducta.\n- Tienes que hablar con claridad, y comportarse en persona civilizada y educada.\n- No debes molestar o insultar a otros jugadores, ni a un miembro del Equipo de Ryzom.\n- El cross posting (publicación del mismo mensaje varias veces para atraer la atención) está prohibido.\n- Tienes que informar sobre cualquier texto ilegal o fuera de la carta.\n- La publicidad de los sitios u otras cosas no relacionados con Ryzom es prohibido.\n\n### II. Carta De Nombres\nLa política de nombres define las reglas que rigen la elección del nombre de tu personaje cuando se crea. Se la establece por el equipo de soporte bajo la dirección de su responsable.\n\n#### Elección del nombre\nAl crear tu personaje, en la fase final tendrás que elegir un nombre para él/ella. Es aconsejable inspirarse en la Lore para tener un personaje que se adhiera mejor al universo de los Atys. Si no tiene ideas o los nombres que elija ya están tomados, puede utilizar el generador de nombres aleatorios disponible en este paso. No obstante, se aceptará un nombre menos juego de roles, pero de acuerdo con la siguiente carta.\n\n#### Cambio de nombre\n- En cualquier momento, un miembro del equipo de soporte puede contactarlo y pedirle que elija otro nombre si no cumple con las reglas a continuación.\n- Puede solicitar un cambio de nombre a un miembro de Soporte por motivos como el juego de roles, desea abandonar un nombre que atrae la burla de otros jugadores, el anonimato ... (esta lista no es exhaustiva). Su solicitud debe ser razonado y será validada por el Equipo de soporte.\n- La cantidad máxima de cambios de seudónimo dependerá del tipo de su cuenta:\n    - Cuenta de Free to Play: 1 cambio de nombre permitido para cada personaje.\n    - Cuenta premium: 1 cambio de nombre permitido cada seis meses para cada personaje.\n \n#### Política de nombres y sanciones por infracciones\n**Nota**: Si un jugador crea varios personajes con nombres que violan la política de nombres de la sección "Nombres muy ofensivos" que aparece a continuación, puede estar sujeto a las sanciones previstas en el Código de Conducta.\n\n##### 1. Nombres muy ofensivos\nEl personaje se renombre inmediatamente con un nombre predeterminado y se envía un correo electrónico al jugador para informarle del cambio de nombre.\n- No puede usar nombres groseros, racistas u ofensivos desde el punto de vista étnico, obscenos, sexuales, de drogas o homónimos, o nombres que incluyan nombres comunes abusivos, refiriéndose a una parte de la anatomía y esto cubre cualquier lenguaje utilizado en el juego.\n- No puede utilizar un nombre elegido para dañar, de ninguna manera, o hacerse pasar por otro jugador, incluidos los empleados de Winch Gate Property Limited o los voluntarios del equipo de Ryzom.\n\n##### 2. Nombres ofensivos\nEl personaje se renombra con 24h de retraso para dar tiempo al jugador para proponer un nuevo nombre. Esto para los personajes creados después de la publicación de la presente Política de Nombres.\n- No puede utilizar nombres que sean fonéticamente ofensivos o que vayan en contra de la Política de Nombres.\n- No se pueden utilizar nombres de origen religioso, oculto o histórico significativo (por ejemplo: Jesús, Lucifer, Nosferatu).\n- No se pueden utilizar nombres que contengan títulos como: Maestro, Rey, Reina, Señor, Señor, Madre, Dios... (esta lista no es exhaustiva).\n\n##### 3. Nombres prohibidos:\nEl personaje se renombra con 3 días de retraso para que el jugador pueda proponer un nuevo nombre. Esto para los caracteres creados después de la publicación de la presente Política de Nombres.\n- No puede utilizar nombres con derechos de autor, marcas comerciales o nombres de materiales o productos (por ejemplo: Fujitsu, Whiskey, Tylenol, Toshiba).\n- No se pueden utilizar nombres de personajes famosos, políticos y personajes de ficción populares (por ejemplo: Angelina Jolie, Donald Trump, James Bond).\n- No puedes usar nombres propios de Ryzom (por ejemplo: Jena, Tryton, Yrkanis, Mabreka).\n- No puede utilizar un nombre que sea la ortografía inversa de un nombre en contradicción con la Política de Nombres (por ejemplo: Anej, Notyrt, Sinakry, etc.).\n- No se pueden utilizar nombres fácilmente reconocibles que procedan de otro universo, como la fantasía o la ciencia ficción medieval u otros, ya sean ficticios o no ficticios, aunque estén ocultos por una ortografía ligeramente diferente (por ejemplo: Skywolker, Gandalf, Flintstains, Obywan).\n- No se pueden usar palabras o frases comunes que no pertenezcan al universo de Ryzom, como nombres (por ejemplo: teléfono celular, microondas, camión).\n\n#### Características de los nombres del gremio:\n- En la mayoría de los casos, los nombres de los gremios deben cumplir con todas las reglas de nombres y están sujetos a las mismas reglas que los nombres de los personajes en términos de renombramiento, dependiendo de si los elementos que componen el nombre del gremio son muy ofensivos, ofensivos o simplemente prohibidos.\n- Sin embargo, hay varias excepciones:\n- El nombre de un gremio puede utilizar el sustantivo propio de los lugares en los Atys para facilitar el juego de roles (por ejemplo: Los Protectores de Fairhaven).\n- El nombre de un gremio puede tener en su título, a efectos de juegos de rol, el nombre de un personaje de la tradición si el conjunto no rompe ningún punto de la Política de Nombramientos y es coherente con la tradición. Así, "Los hijos de Elías", "Los discípulos de Mabreka" se ajustan a la carta de nombres, pero "El tarro de Tryton" no.\n- Si crees que el nombre de un jugador es inapropiado, perturbador o incompatible con el juego, puedes contactar a nosotros por medio de un ticket o directamente en el juego contactando a un miembro del Equipo de Soporte (la lista de CSRs en juego en este momento se muestra bajo la pestaña SYSTEM INFO de la ventana de chat cuando escribes /who gm).\n- Si desea impugnar una solicitud de cambio de nombre (cualquier abuso será sancionado), puede ponerse en contacto con el jefe de soporte por correo electrónico a support@ryzom.com\n\nCualquier solicitud será estudiada cuidadosamente, cualquier abuso será registrado en el perfil del jugador y, si es necesario, sancionado.\n\n### III. Infracción de la integridad del juego\n\n#### III.1. Datos de servidores pirateados, descifrados o asimilados (Delito Crítico)\nCambiar el cliente o su medio de comunicación con el servidor con el fin de obtener una ventaja está estrictamente prohibido y se castiga con una prohibición permanente.\n\n#### III.2. Piratería de Cuentas (Delito Crítico)\nTomar el control de una cuenta que no le pertenece, por cualquier medio informático, está estrictamente prohibido y está sujeto a una prohibición permanente.\n\n#### III.3. Utilización de un bot (delito grave)\n- Uso de un programa de terceros para reproducir acciones de scripts.\n- Se prohíbe estrictamente el uso de macros que no forman parte del macro sistema integrado de Ryzom para que su personaje sea total o parcialmente independiente de su control directo.\n- Esta regla también se aplica a las Ruedas de la Fortuna de las ciudades capitales.\n- El control de varios personajes mediante el uso de un programa de terceros está estrictamente prohibido. Si, durante un control, el Equipo de soporte no puede discernir si un jugador está abusando de esta regla, el jugador deberá proporcionar la prueba de que no está usando ningún software de terceros para controlar a varios personajes al mismo tiempo.\n\n#### III.4. Multi-boxing (para más de 4 caracteres) (Delito Mayor)\nEl número de cuentas conectadas al mismo tiempo para una dirección IP está limitado a cuatro. Sin embargo, el servicio de atención al cliente puede hacer excepciones en casos individuales (como una familia numerosa, un ordenador público, etc.). Si cree que debe quedar exento, póngase en contacto con el servicio de asistencia técnica en support@ryzom.com, indicando el motivo de su solicitud y los nombres de todas las cuentas afectadas.\n\n#### III.5. Explotar (Ofensa Crítica, Mayor o Menor, dependiendo del caso)\n- El concepto de exploit cubre cualquier uso de un programa de terceros, una modificación o del re compilación del cliente o un bug para obtener una ventaja sobre los otros jugadores.\n- Los exploits están prohibidos y son castigados por el Equipo de Atención al Cliente, que tiene el derecho de decidir la gravedad del exploit notado. Sobre este tema, las ofensas repetidas son un factor agravante.\n- Si sospecha del uso de un exploit, debe ponerse en contacto con nosotros a través de uno de los medios mencionados anteriormente.\n\n#### III.6.Arrastrar Bestias fuera de las batallas del puestos avanzados (Ofensa Menor)\n- Está prohibido arrastrar intencionadamente una o varias bestias hacia otro jugador para matarlo o acosarlo. Si un jugador ve o es informado de que ha cometido involuntariamente arrastre de bestias, debe disculparse y hacer todo lo posible para resucitar a los personajes en cuestión, si es necesario.\n- Si eres víctima de un arrastre intencional y puedes probarlo (por ejemplo, una captura de pantalla de un indicador que muestre que, al ser contactado por ti, la persona que arrastró no mostró ningún signo de remordimiento y no quería volver y revivirte), puedes contactar con el Equipo de Soporte a través de una de las formas mencionadas anteriormente. Entonces abriremos un archivo y actuaremos en consecuencia. También en este caso se tendrán en cuenta las reincidencias.\n- Sólo se tolera el arrastre de bestias durante una batalla de puestos avanzados (ver III.9. Falta de respeto a las reglas del PvP).\n\n#### III.7. Robo de Mobs (Ofensa Menor)\n- Matar a una mob que ya está siendo atacado por otro jugador o grupo de jugadores se considera como un robo y está prohibido.\n- Si, después de tratar de resolver la situación entre las partes involucradas, no se encuentra ninguna solución, puede ponerse en contacto con el Equipo de soporte a través de los medios mencionados anteriormente. Sin embargo, antes de ponerse en contacto con cualquier CSR, ¡primero intente comunicarse con los jugadores involucrados y resuélvalos vosotros mismo!\n- En el caso de un Boss(nivel 270) o un Named mob(nivel 260), consulte el siguiente párrafo.\n\n\n#### III.8. Normas con respecto Boss y Named mobs (Ofensa Menor)\n\na) Robo de Boss o Named\n- Un Boss o Named pertenece al primer equipo que lo ha bloqueado, siempre y cuando el bloqueo esté activo. (Ver https://app.ryzom.com/app_forum/index.php?page=topic/view/27212 para más información).\n- Un segundo equipo puede asumir el Jefe o Nombrado sólo si todavía no está bloqueado o si está desbloqueado. Para hacer esto, puede exclusivamente:\n    - Lucha contra el primer equipo en PvP;\n    - Aprovechar la salida del primer equipo, que desbloquea automáticamente el Boss o Named. Tenga en cuenta que si sólo queda un jugador vivo o presente del equipo que ha bloqueado al Boss o al Named, se desbloqueará.\n- El arrastre de mobs con el objetivo de debilitar al otro equipo está prohibido y, por lo tanto, es puede castigar (véase III.6).\n- Un Boss o Named se considera libre siempre que no esté bloqueado por ningún equipo.\n- El tirón de Boss o de Named por una sola persona, es decir sin equipo, es posible pero insuficiente para causar el bloqueo. Volver a re adquirir el Boss o Named en este caso no se considera robar.\n\nb) Acampar de Boss o Named\n- Acampar en Boss o mobs Named en su lugares no está regulado porque a menudo es imposible de probar.\n\n#### III.9. Falta de respeto a las reglas de PvP (ofensa menor)\n\nEn Ryzom, el PvP es siempre consensual. Activar tu etiqueta PvP, entrar en una zona PvP o participar en una batalla de puestos avanzados implica que aceptas las consecuencias. En pocas palabras, si no quieres participar en la acción de PvP, es tu responsabilidad no activar tu tag o no entrar en las zonas de PvP.\n\na) Facción PvP\n- Un jugador que activa su etiqueta PvP acepta las consecuencias, sean cuales sean las circunstancias.Por ejemplo, ser asesinado mientras forrajeaba, en los establos, mientras afk, en un punto de reaparición cerca de un vórtice.\n- Si, a pesar de su etiqueta, desea evitar el combate, puede esperar a que se produzca la detención, esconderse, utilizar un TP, cambiar de lugar o región, etc.\n\nb) Zona PvP\n- El jugador que entra en una zona PvP acepta las consecuencias, cualesquiera que sean las circunstancias. Sin embargo, como excepción específica, los asesinatos en puntos de reproducción cerca de vórtices están prohibidos y se pueden castigar si se prueban. Si estás etiquetado para la Zona PvP y eres víctima de una muerte en una reaparición de vórtice, puedes contactarnos a través de una de las formas descritas anteriormente. En este caso, se recomienda encarecidamente que añada una captura de pantalla para apoyar su reclamación (asegúrese de que la ventana INFO SISTEMA esté visible en la captura).\n\n##### Normas sobre puestos avanzados (PA)\na) Por regla general, los CSR no estarán presente en las batallas de puestos avanzados. Sin embargo, si observa una violación de las normas de decencia y comportamiento, puede ponerse en contacto inmediatamente con el equipo de soporte.\n\nb) Si te has quedado atascado en el paisaje de una región donde está teniendo lugar una batalla de puestos avanzados, primero intenta teletransportación Si no tienes éxito, debe ponerse en contacto con el servicio de atención al cliente, describiendo su situación. Una CSR le ayudará a liberarse.\n\nc) Todo lo que cae dentro de la mecánica normal del juego está permitido en las zonas de combate, siempre que todos los involucrados estén etiquetados para PvP.\n- Un Mektoub de carga puede morir. Al traerlo a una zona de combate, lo estás exponiendo a riesgos.\n- Acampar es casi imposible debido a la zona de seguridad alrededor de los puntos de reaparición, y por lo tanto está permitido. Si mueres y tus enemigos están cerca de tu punto de reaparición, tendrás que esperar a la regeneración en la zona segura o elegir otro punto de reaparición / pacto de teletransportación para volver a la batalla.\n- Durante las batallas de puestos avanzados, el uso de arrastra mobs y matando a Yelks cerca de la pelea es válido y permite la estrategia del juego.\n\nd) Bloqueo de puestos avanzados\n\n- Declarar la guerra a su propio PA o al de uno de sus aliados, o hacer que sus aliados declaren la guerra a su PA con el fin de evitar que alguien más le declare la guerra, se considera una hazaña y está prohibido.\n- Se permite la transferencia de la propiedad de un PA de un gremio a otro. Por lo tanto, está prohibido elevar el umbral, al igual que la entrega del PA varias veces seguidas, lo que se considera un bloqueo del PA.\n\ne) Declaraciones falsas repetidas\n\n- Al declarar la guerra en varias posiciones a la vez, la falta de participación en cualquiera de estos ataques anunciados con un número suficiente de jugadores para alcanzar el umbral solo contra escuadras se considera acoso. Lo mismo se aplica a las declaraciones de guerra falsas repetidas en un solo puesto de avanzada.\n\n**Nota:** Cada uno de los puntos anteriores tiene por objeto dar un cierto grado de flexibilidad a las opciones tácticas durante los ataques, manteniendo al mismo tiempo un equilibrio con el derecho de los propietarios de PA a no ser objeto de acoso. Si crees que tu gremio puede ser acusado de alguna de estas acciones prohibidas, toma una captura de pantalla que muestre el número de jugadores presentes contigo durante el ataque en caso de que necesites mostrársela al Equipo de Apoyo.\n\n### IV. Publicidad inadecuada\n#### IV.1. Venta de oro (Delito Crítico)\nLos vendedores de oro no tienen nada que ver con el juego y su entorno. Por lo tanto, están estrictamente prohibidos. El jugador culpable será inmediatamente pateado y expulsado.\n\n#### IV.2. Publicidad de un producto o de un sitio web no relacionado con Ryzom (Delito Mayor/Menor: dependiendo del tipo de publicidad, el equipo de soporte examinará todos los casos individualmente).\nLa publicidad de sitios web no relacionados con Ryzom y su entorno está prohibida. El jugador culpable será inmediatamente silenciado y sancionado según corresponda.\nEl equipo de soporte revisará cada violación y decidirá sobre la sanción dependiendo de la naturaleza del anuncio publicado.\n\n### V. Fraude en general\n#### V.1. Hacerse pasar por un miembro de un equipo Ryzom&reg o Winch Gate&reg; (ofensa mayor)\nEstá estrictamente prohibido fingir ser miembro de los equipos de juego o de Winch Gate, ya sea en el juego, en el foro de Ryzom, en IRC, en Rocket Chat o en cualquier otro lugar de la web.\n\n#### V.2. Intentar engañar a un miembro del Equipo Ryzom (Ofensa Mayor si el jugador obtiene un beneficio)\n\nIntentar engañar a un miembro del Equipo Ryzom, ya sea para recuperar elementos ya ganados (por ejemplo: recompensa del evento, artículo de la misión, botín de Boss, pieza o plan de Marauder Boss ...), o para cualquier otro propósito, está estrictamente prohibido. . Cualquier jugador condenado por esta transgresión será castigado de acuerdo con el párrafo B. Advertencias y sanciones.\n\n#### V.3. Suplantar a otro jugador (Ofensa menor)\n- Está prohibido hacerse pasar por otro jugador de cualquier manera (incluyendo fingir que es su repetición (alt) o su otro yo (alt) y hablar en su nombre).\n\n#### V.3. Reclamar una relación especial con un miembro de un equipo de Ryzom (Ofensa Menor)\n- Está terminantemente prohibido pretender tener una relación privilegiada con un miembro del equipo de Ryzom (incluido un miembro del equipo de apoyo), ya sea para hacerle daño, para hacerse ver bien a otros jugadores, para tratar de influir en ellos o por alguna otra razón.\n- Los miembros de los equipos de Ryzom están sujetos a una cláusula de confidencialidad y, por lo tanto, se conservan sus identidades (personal y de jugador). Además, cada miembro sigue un código de conducta interno que le prohíbe favorecer a un jugador sobre otro.\n\n**Nota:** Si tal relación resulta ser cierta, el jugador y el CSR (o miembro de otro equipo de Ryzom) estarán sujetos a las sanciones previstas en sus respectivos códigos de conducta.',
  },
  notFound: {
    link: 'Volver a la página de inicio',
    text: 'La página que está tratando de visitar no existe.',
    title: 'Contenido no disponible',
  },
  account: {
    forums: 'Foros',
    logout: 'Cerrar Sesión',
    welcome: 'Hola',
    webig: 'Web-Apps',
    billing: 'Cuenta / Suscripción',
  },
  card: {
    playFree: {
      systemRequirements: 'Requisitos del sistema',
      premium: {
        before: 'También puede optar por el pago ',
        link: 'cuenta premium',
        after: 'para beneficiar aún más!',
      },
      otherPlatforms: 'Otras plataformas',
      text: 'Regístrate, descarga el juego y entra de lleno en la experiencia!',
      title: 'Juega gratis ahora!',
    },
    returning: {
      button: 'Volviendo al juego',
      getGoing: '¡Ahora vete, tus amigos te están esperando!',
      bold: '¿Mucho tiempo sin vernos?  \n¡Es bueno tenerte de vuelta!',
      text: 'Hemos preparado algo de ayuda para que empieces de nuevo.',
      title: '¿Regresando a Ryzom?',
    },
  },
  events: {
    noEvents:
      'Actualmente no hay eventos que mostrar. Por favor, vuelva más tarde.',
    title: {
      calendar: 'Calendario de eventos',
      spotlight: 'Evento Destacado',
    },
    info: {
      timezone: 'El Horario corresponde a su actual ubicación',
    },
  },
};
