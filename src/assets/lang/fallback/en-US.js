// 2020-05-09 15:00
export default {
  game: {
    atys: {
      text:
        'Atys is an enchanting and unique world, formed by the massive growth of vegetation with enormous roots. Populated by intelligent plants, harmless herbivores and fearsome predators; its marvels are matched by its dangers.',
      title: 'Atys: a living world!',
    },
    mobs: {
      text:
        "A powerful A.I. system allows for a realistic wildlife behavior: carnivores attack herbivorous herds, realistic and dynamic animal motions make them lively, sometimes endearing, other times worrisome. There are also mass migrations on season changes and different behavior for different species. Take your time to sit and observe the life around you, it's an unforgettable view. But beware of predators!",
      title: 'Living Mobs',
    },
    roleplay: {
      text:
        'Ryzom offers official interactive events in which live responsive NPCs are involved. As varied as they are regular, their progress and outcome depend on your actions: nothing is preordained!\n\nWould you like to create your own event? The Animation Team is always at your disposal and will gladly support and assist in logistics.\n\nIn addition, many guilds are prepared to open their doors to help your character evolve!\n\nFor lovers of imaginary languages, the players and Animation Team create unique languages. Roleplay immersion guaranteed!',
      title: "Roleplay for the win! It'll blow your mind.",
    },
    customization: {
      text:
        'Entirely customizable character appearance upon creation: shape your avatar the way you like it!',
      title: 'Full character customization',
    },
    classless: {
      text:
        "Ryzom characters are unusual in that they are not bound to a class. That means you don't have to choose between being a warrior, a mage, a harvester or a crafter. You can improve your skills in any of these classes with only one character; you do not need to focus on one path. For example, you can, at your leisure, either train your pike handling, put on your magic amplifiers to heal or cast offensive spells, pick up your pickaxe and harvest materials, or go into crafting a new armor or weapon.\n\nYou have absolute freedom!",
      title: 'Classless',
    },
    multiplatform: {
      text:
        'Ryzom, Free To Play, can be downloaded for Windows, Linux and Mac.',
      title: 'Windows, Linux & Mac',
    },
    forage: {
      text:
        "Ryzom features an advanced harvesting and crafting system.\n\nHarvesting - it allows you to dig materials from the ground depending on season and weather and your skill. It's not so simple once predators get involved!\n\nCrafting - Careful combination of materials depending on their characteristics and colors, allows production of unique weapons and armors. Thousands of recipes are possible.",
      title: 'Advanced forage & harvest',
    },
    pvp: {
      text:
        'You can fight everywhere, but only when you wish to do so. Duels, outposts battles, faction or region fights... You are free to engage in them... or abstain!',
      title: 'Consensual PvP',
    },
    community: {
      text:
        'Ryzom is available in German, English, Spanish, French and Russian, and features a great multilingual community that is mature, helpful and friendly. Everyone can talk and roleplay in their own language; therefore, it is not mandatory to speak a foreign language.\n\nJoin us to be part of the family!',
      title: 'A real community',
    },
    title:
      'Enter a unique science fantasy MMORPG and dive into a unique organic living world!',
    invasions: {
      text:
        'Picture yourself in front of a city gate, looking at a faraway hill and sighting the scouts of a Kitin swarm. Now picture hundreds of Kitins following them, moving like a united and swarming army.\n\nHuge battles call for the cooperation of a great part of the community to counter these threatening invasions. And what if, one day, the Kitin were to win again, threatening hominkind for the third time?',
      title: 'Invasions',
    },
    environment: {
      text:
        'Ryzom is a 3D persistent world consisting of five lush and varied ecosystems. Landscape, wildlife behavior and ground material availability, all change according to season, weather and time.\n\nDiscover the beauty of the Forest in the spring, wage war in the Jungle under a stormy summer sky streaked with lightning, lose yourself in the dunes of the Desert in the fall, or relax enjoying the reflections of a winter sun on the pure water of the Lakes.',
      title: 'Dynamic environments',
    },
    freedom: {
      text:
        "In Ryzom, there are no classes to choose. Develop all the skills of your character harmoniously or choose to concentrate on one skill and become a master. You are not forced to choose a faction when you create your character. Join a faction during your journeys, or not.\n\nYou don't have to fight in PvP, but you can. Join a guild whenever you are ready for it and build up alliances, or just start your adventures alone or with your friends.\n\nSearch for the most valuable materials on Atys to create your own unique equipment with which it is easier to defend yourself against your enemies. Hunt the mightiest bosses on Atys. Conquer an outpost and drill materials for your guild.\n\nDo your own thing or make an impact on the politics. Develop your character in the way you want to and start your own story.\n\nIt is up to you, to decide what to do. Feel free.",
      title: 'Freedom: nothing is more valuable!',
    },
    opensource: {
      text:
        'Ryzom is one of only a few commercial-grade MMORPGs that are fully open source: client, server, tools, and media. Being open source means that Ryzom relies on a vibrant community providing fixes and functionality above and beyond what the team can provide. It offers players a unique opportunity to get involved in the development of the game.\n\nFor more information on Ryzom Core, visit the [Ryzom Core Blog](https://ryzomcore.atlassian.net/wiki/spaces/RC/overview). To browse the artistic assets, please visit the [Ryzom Media Repository](https://github.com/ryzom/ryzomcore-assets).',
      title: 'Opensource',
    },
  },
  website: {
    meta: {
      description:
        'A freedom-oriented classless MMORPG, in a Science Fantasy setting focused on Roleplay, Live Events, complex crafting and dynamic environments.',
    },
    title: 'Ryzom - Free to Play Open-Source MMORPG | Windows, Mac & Linux',
  },
  footer: {
    owner: 'Winch Gate Property Limited',
    copyright: 'Copyright © {year}. All rights reserved.',
    pegi: {
      online: 'Online Gameplay',
      violence: 'Violence',
    },
    privacy: 'Privacy Policy',
    tos: 'Terms of Service',
    community: {
      bmsite: 'Ballistic Mystix',
      armory: 'Ryzom Armory',
      forge: 'Ryzom Forge',
      wiki: 'Ryzom Wiki',
      title: 'Community',
    },
    follow: {
      youtube: 'Youtube',
      twitter: 'Twitter',
      vimeo: 'Vimeo',
      flickr: 'Flickr',
      facebook: 'Facebook',
      title: 'Follow Ryzom',
    },
    support: {
      feedback: 'Feedback',
      forums: 'Forums',
      contact: 'Contact',
      title: 'Support',
    },
  },
  index: {
    calendar: {
      title: 'upcoming events',
      viewMore: 'view all events',
    },
    news: {
      noNews: 'There are currently no news to show. Please check back later.',
      title: 'latest news & updates',
      viewAll: 'view all news',
    },
    features: {
      atys: 'Atys: a living world!',
      roleplay: "Roleplay for the win! It'll blow your mind.",
      freedom: 'Freedom: nothing is more valuable!',
      community: 'A real community',
      title: 'explore ryzom',
      viewAll: 'view all features',
    },
    forge: {
      playersCreate: 'players create',
      openSource: 'open-source',
      supportRyzom: 'support ryzom',
    },
    wiki: {
      visit: 'visit Ryzom Wiki',
      title: 'discover',
    },
  },
  privacy: {
    preamble: {
      title: 'Privacy and Data Protection',
      content:
        'Winch Gate Property Limited (referred to herein as "Winch Gate"), located at 3030 LIMASSOL (Cyprus), runs the game "Ryzom“ and the website "[www.ryzom.com](https://www.ryzom.com)“. Winch Gate is adhering to the General Data Protection Regulation (GDPR, applied since 25th May, 2018), respects the privacy of its online visitors and recognizes the importance of providing a secure environment for the information it collects.\n\nWinch Gate commits itself to provide a document for its online visitors with all informations about which and how personal data is collected, processed and utilized ("Privacy Policy").\n\nBy using this website you accept the Privacy Policy.\n\nPlease note that the Winch Gate Privacy Policy does not apply when you access other sites via the web links located on Winch Gates internet sites, as we have no control over the activities of these other sites. Furthermore, this Privacy Policy may vary from time to time; please consult this document periodically so that you are aware of any changes.',
    },
    policy: {
      title: 'Privacy Policy',
      content:
        'Winch Gate will only collect personal data that are absolutely necessary for the creation and maintenance of accounts and characters within Ryzom. Winch Gate will never share any personal data from its online visitors and/or game users with third parties.\n\n## What is personal data?\nPersonal data means any information relating to an identified or identifiable natural person. This includes IP addresses and cookies data.\n\n## Users‘ rights\nThe user must be informed whether Winch Gate is collecting data, why the data is collected, how it is processed and utilized. The user must give his consent (read more under "Consent“). Furthermore, the user has the right to get information about their stored data on request. This service is free and can be requested at any time by any user (read more at "Right to access data“). In case of a leak, the user will be informed within 72 hours, as will the responsible data protection office (read more at "Breach Notification“). The user also has the right to be forgotten, which means that Winch Gate has to erase all personal data at the users‘ request (read more at "Right to be forgotten“).\n\n## Consent\nThere has to be a request for consent, in an understandable and easy language. It must be in an easily accessible form and the purpose for data processing has to be attached. The consent has to be distinguishable from other services, it must be freely given and it must be easy to withdraw, as easily as it was given.\n\n## Which data are we collecting?\nWinch Gate collects personal data mainly for the payment system for the game "Ryzom“. It is necessary, so the user is able to get the entire experience of a so-called "premium account“. This service is voluntary. In addition, Winch Gate also collects data from time to time for game-related surveys/polls and statistics. The surveys are voluntary and mostly anonymous. However, the user will be informed about the process of data collection before the survey commences and clear consent must be given. The statistics for "Ryzom“ are declared in the EULA and have to be accepted by the user. Winch Gate will inform the user if it is collecting personal data for statistics about other services ("[www.ryzom.com](https://www.ryzom.com)“) and will request consent, in the frame of the related service.\n\n## Why are we collecting data?\nThrough these data collections, Winch Gate is able to develop future strategies, to improve the user experience, to expand its offering and to target a more specific group. Overall, it is used to optimise the experience for Winch Gates‘ customers and users. Additionally, some data is necessary for the payment process.\n\n## Right to access data\nUsers have the right to gain access to their stored data. They can send a request at any time to [https://support.ryzom.com](https://support.ryzom.com). Winch Gate will respond with information about all stored personal data as soon as possible. This service is 100% free and has no limit in use. Please note, that Winch Gate has the right to refuse multiple requests, relating to the same natural person (or account), within a very short time-span.\n\n## Right to be forgotten\nThe user has the right to be forgotten, that means that Winch Gate has to erase all stored personal data at the users‘ request. Please note, that Winch Gate has the right (and the duty) to object to this, in the case of criminal activities, until the appropriate authorities are involved and they give their consent to the erasure of the personal data. Furthermore, in the case that you exercise your right to be forgotten, some Winch Gate products may no longer be available to you, due to the loss of the necessary data (for example "premium accounts"). Winch Gate will inform the user of the consequences before it erases all personal data permanently, and will require the users‘ consent.\n\n## Breach Notification\nIn case of a leak (hack), Winch Gate will send a breach notification to any involved user and to the responsible data protection office, within 72 hours of becoming aware of the breach. This notification will contain information about when the breach was noticed, which natural person and which data are affected, as well as which measures may have already been taken.',
    },
  },
  login: {
    password: 'Password',
    error: {
      server: 'server error',
      credentials: 'invalid credentials',
    },
    username: 'Account name',
  },
  menu: {
    forums: 'forums',
    gameInfo: 'game info',
    universe: 'universe',
    wiki: 'wiki',
    events: 'events',
    home: 'home',
  },
  button: {
    downloadRyzom: 'Download Ryzom',
    continueSaga: 'Continue the Saga',
    login: 'login',
    signUp: 'Create an account',
  },
  download: {
    systemRequirements: {
      os: {
        title: 'Operating System',
      },
      linux: {
        sound: 'OpenAL compatible',
        cpu: '1 GHz single core',
        hdd: '10 GB available space',
        graphics: 'OpenGL 1.2 with 32 MB VRAM',
        title: 'Linux',
        ram: '2 GB RAM',
      },
      sound: {
        title: 'Sound Card',
      },
      cpu: {
        title: 'Processor',
      },
      hdd: {
        title: 'Storage',
      },
      graphics: {
        title: 'Graphics',
      },
      title: 'System requirements',
      windows: {
        os: 'Windows Vista',
        sound: 'DirectX compatible',
        cpu: '1 GHz single core',
        hdd: '10 GB available space',
        graphics: 'OpenGL 1.2 or DirectX 9.0 with 32 MB VRAM',
        title: 'Windows',
        directx: 'Version 9.0c',
        ram: '2 GB RAM',
      },
      minimum: 'minimum',
      directx: {
        title: 'DirectX',
      },
      mac: {
        os: 'OS X 10.8',
        sound: 'OpenAL compatible',
        cpu: '1 GHz single core',
        hdd: '10 GB available space',
        graphics: 'OpenGL 1.2 with 32 MB VRAM',
        title: 'Mac OS',
        ram: '2 GB RAM',
      },
      ram: {
        title: 'Memory',
      },
    },
    title: 'Download Ryzom',
  },
  premium: {
    button: {
      manageSub: 'Manage your subscription',
    },
    sub: {
      button: 'add a subscription',
      title: 'Already  \nplaying?',
    },
    compare: {
      title: 'Compare! The choice is yours.',
    },
    comparison: {
      premium: {
        maxlevel: 'Maximum level 250',
        xp: 'Double XP',
        maxquality: 'Maximum quality of gear 250',
        storage:
          '* One mount & three packers\n* One apartment\n* One guild hall',
        title: 'premium',
        support:
          '* Assistance from the Support Team\n* Restoration of old accounts (subscribed prior to the merge of the\ngame servers) in order to recover the characters\n* Restoration of character(s) in case of voluntary or accidental deletion\n* Transfer of character(s) between two Premium accounts belonging to the same person\n* A change of pseudonym allowed every six months for each character',
      },
      free: {
        maxlevel: 'Maximum level 125',
        xp: 'Normal XP',
        maxquality: 'Maximum quality of gear 150',
        storage: 'One mount',
        title: 'free',
        support:
          '* Assistance from the Support Team\n* A change of pseudonym allowed for each character',
      },
    },
    intro: {
      text:
        'Ryzom has opened the game to the world of Free, releasing its code and becoming the only open source MMORPG, playable on Windows, Linux and Mac.\n\nHowever, maintaining Ryzom does involve costs, such as compensation for the small team of employees and payment for servers. That is why, even though Ryzom is playable as Free to Play, subscriptions are vital for it to continue.\n\nTo thank the players chosing to subscribe, certain features of the game are unlocked and certain bonuses are offered to them: an opportunity to level their characters to level 250 instead of 125, doubling all experience points gained, and access to several other means of storage.\n\n**To help Ryzom, make the choice to subscribe!**',
      title: 'Why pay?',
    },
    rates: {
      perMonth: ' month',
      m1: {
        title: '1-month subscription',
      },
      m3: {
        title: '3-months subscription',
      },
      y1: {
        title: '1 year subscription',
      },
      m6: {
        title: '6-months subscription',
      },
      title: 'Rates',
    },
    donate: {
      button: 'make a donation',
      title: 'Help the Saga  \ncontinue!',
    },
    title: 'Support Ryzom and benefit!',
    signup: {
      button: 'Sign up and start playing',
      title: 'Interested in  \nRyzom?',
    },
  },
  returning: {
    download: {
      text:
        'Need to get the game again? Download Ryzom for FREE and continue your adventures through Atys today!',
      title: 'Download  \nGame',
      modal: {
        onSteam: 'ryzom on Steam',
        forMac: 'for Mac',
        forWin: 'for Windows',
        forLinux: 'for Linux',
      },
    },
    recovery: {
      text: "Can't remember your account name or password? We can help!",
      title: 'Account  \nRecovery',
    },
    resub: {
      link: 'read more',
      text:
        "While you don't need an active subscription to log in and chat with your guild and friends, you do need one to play characters above level 125.",
      title: 'Renew  \nSubscription',
    },
    login: {
      title: 'Account  \nlogin',
    },
    title:
      "It's good to have you back! If you run into any issues, the three links below will help you. Now get going, your friends are waiting for you!",
    headline: 'Getting into the Game',
    register: {
      text:
        "New to Ryzom? Click here to sign up and register a new account. You'll be ready to play within minutes!",
      title: 'Create  \nnew account',
    },
  },
  universe: {
    inhabitants: {
      fauna: {
        text:
          'You will meet several animal species within the various Atysian ecosystems.\n\nIf you take time to observe them, you will discover an alive and realistic fauna, with its peaceful herds of herbivores migrating with the seasons, predators regularly trying isolated attacks, the prey defending at its best. You will be able to be in the heart of the action yourself as the predator… or the prey!',
        title: 'Fauna',
      },
      races: {
        fyros: {
          text:
            "Fyros claim to be the warrior people of Atys. Eager for adventure, fighting and hunting against those they consider worthy to defy. They are also hungry for knowledge and eager to uncover the mysteries of Atys' past. Not very fervent in nature, their relationship to religion has always been shaped by their pragmatism.\n\nMost of the Fyros inhabit the Desert, inside which they established their Empire. They value Truth, Honor, Discipline and Justice. Fulfilling their citizenship rite allows allows one to become a Fyros Patriot and benefit from various assets and rewards.\n\nTheir ancestral language is fyrk.",
          title: 'Fyros',
        },
        matis: {
          text:
            'The Matis are a sophisticated, cultured and ambitious people. They master botany and build their homes and tools from living plants. Both artists and conquerors, they believe each homin must earn their place inside society. They are the most faithful followers of the Karavan.\n\nMost of the Matis live inside the Forest, where they established their Kingdom, a monarchy based on divine right. They value Loyalty to the King and Jena, Aesthetic achievement, and Competition. The citizenship rite allows one to become a Matis Vassal and benefit from various assets and rewards.\n\nTheir language is Mateis.',
          title: 'Matis',
        },
        text:
          "Atys is inhabited by many sentient beings. There live the Kamis, mysterious shapeshifting creatures devoted to nature's protection; the apostles of the Karavan, armoured humanoids controlling an unknown technology; and finally, the homins, who, except for a handful of primitive tribes, are spread into four nations: Fyros, Matis, Tryker and Zoraï.",
        title: 'nations',
        tryker: {
          text:
            'The smallest people by height, Trykers are peaceful, dedicated to freedom, and bon vivant. They dream of a peaceful world without masters or slaves. Their curiosity makes them excellent explorers and inventors, and they fully master windmill technology. Their personal interpretation of religion often arouses the anger of dogmatists.\n\nMost of the Trykers call the Lakelands their home, where they established their Federation, headed by a Governor. They value Liberty, Equality and Sharing. Taking their citizenship rite allows one to become a Tryker Citizen and enjoy various benefits and rewards.\n\nThey have their own language, the Tryker Tyll.',
          title: 'Tryker',
        },
        zorai: {
          text:
            "By nature calm and spiritual, Zoraïs are the tallest of all homins. The Mask covers their face, a sacred gift from the Kami, who they serve fervently. Their holy duty is to ensure nature's equilibrium and to fight against the Goo, the plague of the Jungle. Always looking for knowledge, they master magnetism.\n\nMost of the Zoraï remain in the Jungle, where they established their Theocracy, led by the Great Sage. They value Spiritual achievement, Wisdom, and Respect for Nature. The citizenship rite allows one to become a Zoraï Initiate and benefit from various assets and rewards.\n\nThey speak the Taki Zoraï.",
          title: 'Zoraï',
        },
      },
      flora: {
        text:
          "Atys shelters an abundant flora, including several species of intelligent plants.\n\nThese plants will provide you with useful materials for your crafting, but you'll have to learn how to escape their formidable attacks, including magical ones!",
        title: 'Flora',
      },
      floraFauna: {
        text:
          'Atys is populated with many animal and plant species. Hunting them (or “picking” them) allows the homins to obtain the raw materials required for crafting: skins, fangs, bones, horns, ligaments, hooves, eyes, nails… Everything is usable to make clothes, weapons, jewellery…\n\nTo enjoy the spoils of the hunt, you will have to learn -from your elders or through trial and error- different hunting techniques, as each animal or plant species has its own behavior in combat.',
        title: 'Fauna and flora',
      },
      kitins: {
        text:
          'The most dangerous animals for the homins are undoubtedly the kitins. Organized, powerful and merciless, these giant insectoids with an elaborate social organization have already ravaged Atys twice, slaughtering homins indiscriminately and wiping out entire civilizations.\n\nSince then, hominkind has been rebuilding step by step, but the danger is always lurking...',
        title: 'Kitins',
      },
      title: 'The inhabitants',
    },
    planet: {
      text:
        'Exploring this world will lead you through its multiple ecosystems, from the burning desert to the sublime lakes, from the majestic forest to the luxuriant jungle ravaged by a deadly plague, and even into the dark depths of the planet, crisscrossed by rivers of sap.\n\nSet out to conquer Atys or come to uncover its mysteries and its legends! But be careful, because danger lurks around every bend in these idyllic landscapes.',
      title: 'The planet',
    },
    continents: {
      forest: {
        text:
          'The Forest is the domain of the Matis, who have built there their Kingdom and shaped their capital, Yrkanis, in living trees.\n\nKnown as the Verdant Heights, this continent is covered with impressive and majestic trees and benefits from a temperate climate punctuated by four pronounced seasons.',
        title: 'Forest',
      },
      jungle: {
        text:
          'The Zoraïs have built their cities in the plant maze of the Jungle, around their capital, Zora.\n\nThis continent is also called the Witherings, since it is infected by the stench of the Goo, a cancer which erodes life inexorably, ravaging the habitat, and mutating flora and fauna into horrible creatures.',
        title: 'Jungle',
      },
      lakes: {
        text:
          'The archipelago of the Lakes includes a myriad of islands inhabited by the Trykers. Here they have founded their Federation and built floating cities, with Fairhaven as their capital.\n\nThis continent, known as Aeden Aqueous, offers an idyllic scenery with its white sawdust beaches, its magnificent waterfalls, and its crystal clear water as far as the eye can see. Climate is sunny all year round.',
        title: 'Lakelands',
      },
      roots: {
        text:
          'The Prime Roots is the most mysterious ecosystem of Atys. Diving into the depths of the planet is the huge underground continent, its floor, walls and ceiling populated by phosphorescent plants and strange creatures.\n\nAs beautiful as they are dangerous, the Prime Roots hide within them the enigmatic remains of the past, and they host worrying Kitins nests. The climate is permanently fresh and wet.',
        title: 'Prime Roots',
      },
      desert: {
        text:
          'This continent, known as the Burning Desert, is the domain of the Fyros, who have founded their Empire around the capital fortress of Pyr.\n\nStrong violent winds, long periods of drought, suffocating temperatures during the day and freezing temperatures at night make these smouldering dunes one of the most inhospitable places of Atys.',
        title: 'Desert',
      },
      title: 'The continents',
    },
    factions: {
      kami: {
        text:
          'Includes all the Disciples of the Kamis, the protective bodies constantly linked to the supreme Kami, Ma-Duk.\n\nAny homin willing to enter within the Kami faction must complete a rite of allegiance and swear to respect the Laws of Ma-Duk.\n\nThe Kami offer various services to their Disciples, the two most important ones being resurrection and teleportation to all the regions of Atys. They also offer unique rewards to those who fight the Karavan Followers in their name.\n\nThe Kami Disciples face the Karavan faction regularly during outposts battles, with the aim of exploiting their valued resources, using impressive and ecological tree bores provided by the Kami.',
        title: 'kami',
      },
      ranger: {
        text:
          "Includes all the Homins with a desire to free Hominkind from the Kitin threat.\n\nThe guild of the Rangers of Atys, created after the First Great Swarming, is driven by an ideal of brotherhood. The Rangers believe that Homins should live in peace, without division; therefore, they forbid themselves to take part in the quarrels between homins, and refuse to lay hands on them, except in case of self-defense.\n\nIn addition to patrolling endlessly on Atys, the Rangers also hold strategic bases on the Silan island and in the Almati Wood. The Silan Camp, managed by Chiang, welcomes refugees that arrive to the New Lands. The camp in the Almati Wood, managed by the Guide Ranger Orphie Dradius, is used as a logistical center, and surveys the nearby Kitin's Lair.\n\nDuring the Second Great Swarming, the Rangers helped save Hominkind. Their battle plans allowed a safe rescue of the population by convincing the Kamis, the Karavan, the Nations and the Trytonists to collaborate.\n\nWhile doing everything possible to avoid a third Great Swarming, the Rangers keep close watch on the Kitins, eliminate their overly curious scouts, and develop new fighting techniques and discretionary tactics.",
        title: 'ranger',
      },
      neutral: {
        text:
          'Some homins have chosen not to ally with either of the two Powers, but neither to fight them: they are religiously neutral. \n\nAlthough they do not officially constitute a faction, the religious neutrality that brings them together far from Ma-Duk or Jena is often seen as such.',
        title: 'neutral',
      },
      karavan: {
        text:
          'Includes all the Followers of the Karavan, the hominid disciples of the goddess Jena.\n\nAny homin willing to enter within the Karavan faction must complete a rite of allegiance and swear to respect the Laws of Jena.\n\nThe Karavan offer various services to their Followers, the two most important ones being resurrection and teleportation to all the regions of Atys. They also offer unique rewards to those who fight the Kami Disciples in their name.\n\nThe Karavan Followers face the Kamis regularly during outposts battles, with the aim of exploiting their valued resources, using their huge metal drills provided by the Karavan.',
        title: 'karavan',
      },
      text:
        'Two Powers, different but both mysterious, seek to control the destiny of Atys: the Karavan, who watch over the homins and propagate the word of the goddess Jena, and the Kamis, keepers of the plants and of the world equilibrium. Both possess unique powers, through which they protect and help homins who follow their divine laws. For centuries, these two Powers have observed each other without confrontation. Their protection will be of great help to you during your journey on Atys, and their wrath may be deadly… So, chose your side carefully.\n\nHowever, if you choose, at your own risk, not to serve either of the two Powers, other doors are open to you. You can choose Religion Neutrality, and not take sides in the religious struggle, or become a Trytonist and fight in the shadows to free Hominkind from the Powers, or even join the ranks of the Marauders and fight both the Powers and the Nations. Your path could also take you to the way of the Rangers, who rely on the unification of both the Powers and the Nations to fight the Kitin enemy.',
      title: 'the factions',
      marauder: {
        text:
          'Includes all the Homins whose ambition is to conquer Atys by rebelling against the domination of the Powers and Nations. The Marauder Faction was born from the clans of the Homins abandoned in the Old Lands during the First Great Swarm.\n\nThis society, ruled by the survival of the fittest, is governed by Combativeness, Conquest and Survival. Over time, the Marauders have developed their own language: Marund.\n\nMarauders do not own any territory in the New Lands; hence, they have erected an encampment in a region claimed both by the Fyros and the Matis.\n\nAny homin willing to enter within the Marauder faction must complete a rite of allegiance and swear to answer the calls of Melkiar, the Black Varinx, war leader of the Marauder Clans. In exchange, they benefit from specific services for resurrection and teleportation all around Atys and its warriors are offered unique rewards.\n\nMarauders regularly try to conquer outposts through battles, with the aim of exploiting their valued resources.',
        title: 'marauder',
      },
      trytonist: {
        text:
          "Includes all Homins who want to free Atys from the yoke of the Powers, considering the appealing rewards they offer as mere lures to enslave Hominity.\n\nTrytonists' guide is Elias Tryton, a mysterious being considered by some as a Karavan renegade, and by others as the husband of the goddess Jena.\n\nSince they are hunted down by the Karavan and barely tolerated by the Kamis, Trytonists have to lie hidden in order to survive. Trytonists are currently spread through guilds of all religions and nations, and have developed a secret network using code names.\n\nTrytonists get their instructions from the Guild of Elias, whose leader, Nicho, is in direct contact with Elias Tryton.\n\n_Note: this faction is currently playable only through roleplay._",
        title: 'Trytonist',
      },
    },
  },
  header: {
    forums: 'Forums',
    logout: 'logout',
    playNow: 'play now!',
    login: 'login',
    cookies: {
      button: {
        decline: 'Continue with necessary cookies only.',
        accept: 'Accept all cookies.',
      },
      text: 'Ryzom is using cookies for tracking and marketing purposes.',
    },
    webig: 'Web-Apps',
    billing: 'Account',
  },
  tos: {
    coc: 'Code of Conduct',
    intro:
      'The following rules cover the most common situations but they are not exhaustive. The Customer Support team has the right to amend these rules if a new situation should arise which needs further clarification.\n\nIf a case is too specific to appear in the rules but nevertheless requires the intervention of the Support team, they can act and apply, at any time, warnings or sanctions without being required to inform the whole community.\n\nIf you witness a rule violation or infringement, please inform Customer Support by using one of the following means:\n- the in-game ticket system\n- an in-game private chat to an online CSR (/tell command)\n- Rocket Chat\n- an e-mail sent to support@ryzom.com',
    title: 'Terms of Service',
    content:
      '## The Support Team\nIts members are volunteers under non-disclosure agreement (NDA) and are not allowed to reveal their identity neither to other players nor to other members of the Ryzom teams. If a player thinks he knows the identity of the player behind the CSR (or any NDA members of a Ryzom team), he is prohibited from disclosing the information to others and doing so would be cause for sanctions to be applied. (major offense, see B. Warnings and sanctions)\n\nMembers of Support are in charge of solving the various problems related to the game (technical, disciplinary, etc.). They must behave with politeness and courtesy at all times. If you have a disagreement with a member, you can address a complaint, duly documented, to support@ryzom.com which will be forwarded to the head of Support. Any unfair complaint against a member of Support may be subject to a sanction.\n\nWhen a member of Support contacts you, you must follow their instructions and reply with courtesy and politeness. Any breach will be sanctioned according to our procedures for sanctions and escalation policy. (See **B. Warnings and sanctions**)\n\nWe remind you that members of Support dedicate their time (both game and real life time) to ensure that everyone can play in a peaceful environment: try to have this in mind when you contact a Team member. This is of course the same for all teams: CSR, dev, event, etc.\n\nThe Support Team is subject to strict rules that prohibit them from teaming, exchanging with, duelling with any player, or resurrecting them. These rules also forbid them from using their powers for any purpose other than those defined within the context of their function, under risk of removal of their CSR account, or even their player account. In addition, CSRs are not allowed to process tickets involving their own player guilds or characters.\n\nIn accordance with these rules, control tools record all critical commands used by CSRs (as well as members of other teams) and display them internally for real-time control by all. Each connection of a member of Support (and of other teams) is also visible so that everyone can verify that no one has usurped their account.\n\nAll decisions taken are collective ones and therefore you may be contacted by several members of Support about the same issue depending on its nature.\n\n## A. Disagreement with another player\nIn the case of a disagreement with another player, you are expected to try to find to an amicable settlement yourselves. A ticket should be a recourse of last resort; it should only be sent if you are not able to resolve the situation and a third party needs to become involved.\n\nIf you have to resort to sending the above-mentioned ticket, it is important to take screenshot(s), depending on the situation, for further reference. **It is essential that the screenshot(s) show(s) the entire in-game screen and has not been changed in any way (censorship or anything else) in order to be considered acceptable.** It has to show clearly the violation that you want to point out. Note that a screenshot showing the open map allows us to see the Atysian date, which is easily transposable to a real date. We also accept videos. For any other questions, you can contact us directly in game through a /tell if a CSR is online, through [Rocket Chat](https://chat.ryzom.com/), or through an e-mail sent to support@ryzom.com.\n\nN.B. Any abuse of the ticket system will be documented and sanctioned.\n\n## B. Warnings and sanctions\nThe Customer Support Team has the right to issue warnings or sanctions depending on the seriousness of an infraction, the reaction of the implicated player towards the Support team, and their in-game history (sanctions and previous warnings). A player might thus be given a reprimand, a warning, a suspension period ranging between 24h and several days, or a permanent ban.\n\n### Penalty scales\nThe penalty scale for a **minor offence** starts with an official warning followed by a 24h suspension, then three days, then one week and up to two weeks. It can be followed or preceded by a mute of increasing duration at the place of the infraction (channels of the game, or Rocket Chat, or forum etc.).\nThis penalty scale is reset after one year without sanctions.\n\nThe scale of penalties for a **major offence** starts with a week of suspension, then one month and then permanent banning of the account. It can be followed by a kick off the server if necessary.\nThis penalty scale is reset after three years without sanctions (except for permanent ban).\n\n**Critical offence**: Final banning of the account after warning the player by e-mail.\n\nN.B. In the case of a heavy and immediate sanction (suspension or permanent banning of an account), as the player in question can no longer be contacted in-game, he or she will be contacted only through e-mail **to the e-mail address filed in his/her personal account**. This is why it is important to enter a valid e-mail address when registering. If the player in question does not receive the e-mail, or if they wish to dispute the sanction, they can contact the Customer Support team at support@ryzom.com.\n\n## LEGAL NOTICES\nThe sanctions provided  in the Ryzom Code of Conduct shall be assigned as follows:\n- Mutes and verbal warnings may be applied by all CSRs.\n- Official warnings (by email) may be applied by all CSRs from the GM grade upwards.\n- Suspensions may only be applied by CSRs at the SGM level.\n- Account bans may only be decided and enforced by Winch Gate Limited.\n\n## II. Naming Policy\nThe naming policy defines the rules that govern the choice of the name of your character when it is created. It is established by the support team under the direction of its manager.\n\n### Name choice\nWhen creating your character, in the final phase you will have to choose a name for him/her. It is advisable to take inspiration from the the Lore to have a character that adheres better to the universe of Atys. If you have no ideas or the names you choose are already taken, you can use the random name generator available at this step. A name that is not Atys-inspired, but nonetheless in accordance with the charter below will be accepted.\n\n### Name change\nAt any time, a Support Team Member can contact you and ask you to choose another name if it does not comply with the rules below.\n\nYou can request a name change from a Support Member for reasons such as roleplay, wish to abandon a name that is attracting other players\' mockery, anonymity... (this list is not exhaustive). Your request must be reasoned and will be validated by the Support Team.\n\nThe maximum number of pseudonym changes will depend on the type of your account:\n- Free to Play account: 1 allowed name change for each character.\n- Premium account: 1 change of name allowed every six months for each character.\n\n### Naming Policy and penalties for violations\nN.B. If a player creates multiple characters with names that violate the naming policy in the "Very offensive names" section below, they may be subject to the sanctions provided in the Code of Conduct.\n\n#### 1. Very offensive names\nThe character is immediately renamed with a default name and an email is sent to the player to inform them of the name change.\n\n- You may not use coarse, racist or ethnically offensive, obscene, sexual, drug or homonymous names, or names that include abusive common names, referring to a part of the anatomy and this covers any language used in the game.\n- You may not use a name chosen to harm, in any way, or impersonate another player, including Winch Gate Property Limited Employees or a Ryzom Team volunteers.\n\n#### 2. Offensive names\nThe character is renamed with 24h of delay to allow time for the the player to propose a new name. This for characters created after the publication of the present Naming Policy.\n\n- You may not use names that are phonetically offensive or would go against the Naming Policy.\n- You may not use names of religious, occult or significant historic origin (e.g.: Jesus, Lucifer, Nosferatu)\n- You may not use names containing titles such as:  Master, King, Queen, Lord, Sir, Mother, God... (this list is not exhaustive).\n \n#### 3. Forbidden names:\nThe character is renamed with 3 days of delay to allow time for the the player to propose a new name. This concerns only characters created after the publication of the present Naming Policy.\n\n- You may not use names that are copyrighted, trademarks or names of materials or products (e.g.: Fujitsu, Whiskey, Tylenol, Toshiba).\n- You may not use names of famous people, politicians and popular fictional characters (e.g.: Angelina Jolie, Donald Trump, James Bond.).\nYou may not use proper nouns of Ryzom (e.g.: Jena, Tryton, Yrkanis, Mabreka).\n- You may not use a name which is the reverse spelling of a name in contradiction with the Naming Policy (e.g.: Anej, Notyrt, Sinakry, etc.).\n- You may not use easily recognized names that come from another universe such as medieval fantasy or science fiction or others, either fictional or non-fictional, even if hidden by a slightly different spelling (e.g.: Skywolker, Gondalf, Flintstains, Obywan) .\n- You may not use common words or phrases not pertaining to the Ryzom universe, as names (e.g.: cell phone, microwave, truck).\n \n### Characteristics of Guild Names\nFor the most part, guild names must comply with all the rules of names and are subject to the same rules as character names in terms of renaming, depending on whether the elements that make up the name of the guild are very offensive, offensive or just forbidden.\nThere are, however, several exceptions:\n- The name of a guild can use the proper noun of places on Atys in order to facilitate the role play (e.g.: The Protectors of Fairhaven).\n- The name of a guild can have in its title, for roleplay purposes, the name of a character of the Lore if the whole does not break any point of the Naming Policy and is consistent with the Lore. So, "The children of Elias", "The disciples of Mabreka" conform to the charter of names, but "The Jar of Tryton" does not.\n \nIf you think that a player\'s name is inappropriate, disturbing, or incompatible with the game, you can report it to us by ticket or directly in game by contacting a member of the Support Team (the list of CSRs in-game at the moment is displayed under the SYSTEM INFO tab of the chat window when you type /who gm).\n\nIf you want to challenge a rename request (any abuse will be sanctioned), you can contact the head of support via e-mail to support@ryzom.com.\nAny request will be studied very carefully, any abuse will be recorded on the player’s profile and, if necessary, sanctioned.\n\n## III. Game Integrity Infringement\n \n\n### 1. Servers data hacked, decrypted or assimilated (Critical Offence)\n\nChanging the client or its means of communicating with the server in order to obtain an advantage is strictly prohibited and is punishable by a permanent ban.\n\n\n### 2. Account Piracy (Critical Offence)\n\nTaking control over an account that does not belong to you, by any computer means, is strictly prohibited and is subject to a permanent ban.\n\n\n### 3. Utilization of a bot (Major Offence)\n\nUsing a third-party program to play through scripted actions.\n\nUsing macros which are not part of Ryzom\'s integrated macro system in order to make your character partly or completely independent of your direct control is strictly prohibited.\nThis rule also applies to the Wheels of Fortune of the capital cities.\n\nControl of several characters by the use of a third-party program is strictly prohibited. If, during a check, the Support Team is not able to discern whether a player is abusing this rule, the player will have to provide the proof that they\'re not using any third-party software to control several characters at the same time.\n\n\n### 4. Multi-boxing (for more than 4 characters) (Major Offence)\n\nThe number of accounts connected at the same time for one IP address is limited to four. However, Customer Support may make exceptions in individual cases (such as a large family, public computer etc). If you believe you should be exempted, please contact Support at support@ryzom.com, mentioning the reason for your request along with the names of all the accounts concerned.\n\n\n### 5. Exploit (Critical, Major or Minor Offence, depending upon the case)\n\nThe concept of exploit covers any use of a third-party program, a client modification or recompilation or a bug to gain an advantage over the other players.\n\nExploits are forbidden and punishable by the Customer Support Team, which has the right to decide the severity of the noticed exploit. On this subject, repeated offences are an aggravating factor.\nIf you suspect the use of an exploit, you must contact us using one of the means mentioned above.\n\n\n### 6. Aggro dragging, except at outpost battles (Minor Offence)\n\nIntentionally dragging one or several mobs towards another player in order to kill or harass them is prohibited. If a player sees or is informed that he has unintentionally committed aggro dragging, he must apologize and do his best to resurrect the concerned characters, if required.\n\nIf you are victim of intentional aggro dragging and can prove it (for instance a screenshot of a tell showing that, when being contacted by you, the person dragging didn\'t show any sign of remorse and didn\'t want to come back and revive you), you can contact Support Team through one of ways mentioned above. We will then open a file and act accordingly. Here again, repeated offences will be taken into consideration.\n\nAggro dragging is only tolerated during an outpost battle (see III.9. Disrespect of the PvP rules).\n\n\n### 7. Kill stealing (Minor Offence)\n\nKilling a mob which is already being attacked by another player or group of players, is considered as kill stealing and is prohibited.\n\nIf, after trying to resolve the situation between the parties involved, no solution is found, you may contact Support Team through the means mentioned above. However, before contacting any CSR, first try to communicate with the player(s) involved and resolve it yourself!\n\nIn the case of a Boss or a Named mob, please refer to the following paragraph.\n\n\n### 8. Rules regarding Bosses and Named mobs (Minor Offence)\n\n#### a) Theft of Bosses or Named\n\nA Boss or Named belongs to the first team that has locked it, as long as the lock is active. (See https://app.ryzom.com/app_forum/index.php?page=topic/view/27212 for more information.)\nA second team may assume the Boss or Named only if it is not yet locked or it is unlocked. To do this, it can exclusively:\n- Fight the first team in PvP;\n- Take advantage of the departure of the first team, which automatically unlocks the Boss or Named. Note that if there is only one player is left alive or present from the team that has locked the Boss or Named, it will be unlocked.\nAggro dragging aimed at weakening the other team is prohibited and is therefore punishable (see III.6).\n\nA Boss or Named is considered free as long as it is not locked by any team.\n\nThe pull of Boss or of Named by a single person, that is to say without team, is possible but insufficient to cause the locking. To re-acquire the Boss or Named in this case is not considered to be kill stealing.\n\n#### b) Camping of Boss or Named spots\n\nCamping of Boss or Named mobs’ spots is not regulated because it is often impossible to prove.\n\n\n### 9. Disrespect of the PvP rules (Minor Offence)\n\nIn Ryzom, PvP is at all times consensual. Activating your PvP tag, entering a PvP zone or engaging in an outpost battle implies that you accept the consequences. To put it simply, if you do not want to take part in PvP action, then it is your responsibility to not activate your tag or not go into PvP zones.\n\n#### a) Faction PvP\n\nA player who activates their PvP tag accepts the consequences, whatever the circumstances.\nE.g.: being killed while foraging, at the stables, while afk, at a respawn point near a vortex.\n\nIf, despite your tag, you wish to avoid the fight, you can wait for detagging to occur, hide, use a TP, change place or region, etc.\n\n#### b) Zone PvP\n\nThe player who enters a PvP zone accepts the consequences, whatever the circumstances. However, as a specific exception, killings at respawn points near vortices are prohibited and punishable if proven. If you are tagged for Zone PvP and you are the victim of a kill at a vortex respawn, you can contact us through one of the ways described above. In this case, it is strongly recommended that you add a screenshot to support your claim (please ensure that the SYSTEM INFO window is visible in the shot).\n\n#### c) Outpost battle PvP\n\nThe player who enters an Outpost battle area and chooses a side, accepts the consequences, whatever the circumstances.\nHowever, as a specific exception, kills at respawn points near a vortex are prohibited and punished if proven. If you are under OP battle tag and you are the victim of a kill at a vortex or respawn, you can contact us through one of the ways described above. In this case, it is strongly recommended that you include a screenshot to support your claim.\nUntagged characters in the OP may be removed during the battle as they distort the targeting of the protagonists. CSRs can move them if required.\nIf, despite your tag, you wish to avoid the fight, you can wait for detagging to occur, hide, use a TP, change place or region, etc.\n\n#### Rules regarding outposts (OP)\n\na) CSRs will, as a general rule, not be present at outpost battles. However, if you notice a violation of the rules of decency and behaviour, you can immediately contact the Support team.\n\nb) If you have become stuck in the landscape in a region where an outpost battle is taking place, first try to teleport. If you don\'t succeed, you must contact Customer Support, describing your situation. A CSR will then help to free you.\n\nc) Everything that falls under the normal mechanics of the game is allowed in combat zones, provided that all those involved are tagged for PvP.\n1. A Mektoub packer might die. By bringing it into a combat zone, you are exposing it to risks.\n2. Spawn camping is almost impossible because of the security zone around the respawn points, and is thus allowed. If you die and your enemies are near your respawn point, you will have either to wait for regeneration in the safe zone or to choose another respawn point/Teleportation pact to rejoin the battle.\n3. During outpost battles, using aggro dragging and killing yelks near the fight is a valid and allowed game strategy.\n\n#### d) Outpost Blocking\n\nDeclaring war on your own OP or that of one of your allies, or having your allies declare war on your OP(s) in order to prevent someone else declaring war on it, is considered an exploit and is prohibited.\nThe transferring of ownership of an OP from one guild to another is allowed; raising the threshold during the transfer is forbidden, as is the handing over of the OP multiple times in a row, this being considered as blocking the OP.\n\n#### e) Repeated fake declarations\n\nWhen declaring war on several OPs at once, the failure to participate in any of these announced attacks with a sufficient number of players to reach the threshold against squads alone is considered harassment. The same applies to repeated false declarations of war on a single outpost.\n\nN.B. Every point above aims at giving a certain amount of flexibility to tactical choices during attacks while maintaining a balance with the right of OP owners to be free of harassment. If you think that your guild might be charged with one of these forbidden actions, take a screenshot showing the number of players present with you during the attack in case you need to show it to Support Team.\n\n## IV. Inappropriate Advertising\n### 1. Gold selling (Critical Offence)\n\nGold sellers have nothing to do with the game and its environment. Thus, they are strictly forbidden. The guilty player will immediately be kicked and banned.\n\n\n### 2. Advertising of a product or a website unrelated to Ryzom (Major/Minor Offence: depending on the type of advertising, the Support team will examine all cases individually.)\n\nAdvertising for websites not related to Ryzom and its environment is prohibited. The guilty player will immediately be muted and sanctioned as appropriate.\nThe support team will review each violation and decide on the sanction depending on the nature of the advertisement published.\n\n## V. General Fraud\n### 1. Impersonating a member of a Ryzom Team or Winch Gate (Major Offence)\n\nIt is strictly forbidden to pretend to be a member of the game teams or Winch Gate, whether in-game, on the forum of Ryzom, on IRC, Rocket Chat or elsewhere on the web.\n\n\n### 2. Attempting to deceive a Ryzom Team member (Major Offence if the player gets a benefit)\n\nAttempting to deceive a member of the Ryzom Team, either in order to retrieve items already won (e. g.: event reward, mission item, Boss\' loot, Marauder Boss’ piece or plan...), or for any other purpose is strictly forbidden. Any player convicted of this transgression will be punished in accordance with paragraph B. Warnings and sanctions.\n\n\n### 3. Impersonating another player (Minor Offence)\n\nIt is forbidden to impersonate another player in any way (including pretending to be his reroll or alt and speaking on his behalf).\n\n\n### 4. Claiming a special relationship with a member of a Ryzom Team (Minor Offence)\n\nIt is strictly forbidden to pretend to have a privileged relationship with a member of the Ryzom team (including a member of Support), whether to harm them, to make oneself look good to other players, to try to influence others, or for some other reason.\nThe members of the Ryzom teams are subject to a non-disclosure clause and therefore their identities (personal and player) are preserved. In addition, each member follows an internal code of conduct that prohibits them from favouring one player over another.\n\nNB: If such a relationship proves to be true, the player and the CSR (or member of another Ryzom team) would be subject to the sanctions provided by their respective codes of conduct.',
  },
  notFound: {
    link: 'back to HOME',
    text: 'The page you are trying to visit does not exist.',
    title: 'Content not available',
  },
  account: {
    forums: 'Forums',
    logout: 'Logout',
    welcome: 'Welcome',
    webig: 'Web-Apps',
    billing: 'Account / Subscription',
  },
  card: {
    playFree: {
      systemRequirements: 'System requirements',
      premium: {
        before: 'You can also opt-in for a paid',
        link: 'premium account',
        after: 'to benefit even more!',
      },
      otherPlatforms: 'Other platforms',
      text: 'Sign up, download the game and hop right into the experience!',
      title: 'Play Free Now!',
    },
    returning: {
      button: 'Getting Back in the Game',
      getGoing: 'Now get going, your friends are waiting for you!',
      bold: "Long time no see?  \nIt's good to have you back!",
      text: "We've prepared some help to get you started again.",
      title: 'Returning to Ryzom?',
    },
  },
  events: {
    noEvents: 'There are currently no events to show. Please check back later.',
    title: {
      calendar: 'Event Calendar',
      spotlight: 'Event Spotlight',
    },
    info: {
      timezone: 'Times displayed are adjusted to your current time-zone',
    },
  },
};
