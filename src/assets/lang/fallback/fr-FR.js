// 2020-05-09 15:00
export default {
  game: {
    atys: {
      text:
        "Atys est un monde enchanteur et unique, formé par la croissance massive d'une végétation aux racines énormes. Peuplé de plantes intelligentes, d'herbivores inoffensifs et de prédateurs redoutables, ses merveilles n'ont d'égal que ses dangers.\n",
      title: 'Atys : un monde vivant !',
    },
    mobs: {
      text:
        "Un puissant système d'I.A. permet un comportement réaliste de la faune : les carnivores attaquent les troupeaux herbivores, les mouvements réalistes et dynamiques des animaux les rendent vivants, parfois attachants, parfois inquiétants. Il y a aussi des migrations massives lors des changements de saison et des comportements différents selon les espèces. Prenez votre temps pour vous asseoir et observer la vie autour de vous, c'est une vue inoubliable. Mais attention aux prédateurs !\n",
      title: 'Une faune remuante',
    },
    roleplay: {
      text:
        "Ryzom propose des évènements interactifs officiels mettant en jeu des PNJ réagissant en direct. Aussi variés que réguliers, leur déroulement et leur issue dépendent de vos actions : rien n'est écrit ! \n\nVous souhaitez créer votre propre évènement ? L'équipe d'animation est toujours à votre disposition et se fera un plaisir de vous soutenir et de vous aider dans la logistique.\n\nDe plus, de nombreuses guildes sont prêtes à vous ouvrir leurs portes pour faire évoluer votre personnage!\n\nPour les amateurs de langues imaginaires, les joueurs et l'équipe d'animation créent des langues uniques. Immersion dans le jeu de rôle garantie !\n",
      title: 'Jeu de rôle pour vaincre ! Vous en serez époustouflé.',
    },
    customization: {
      text:
        'Apparence du personnage entièrement personnalisable à la création : donnez à votre avatar la forme que vous souhaitez !',
      title: 'Personnalisation complète des personnages',
    },
    classless: {
      text:
        "Les personnages Ryzom sont inhabituels en ce sens qu'ils ne sont pas liés à une classe. Cela signifie que vous n'avez pas à choisir entre être un guerrier, un mage, un récolteur ou un artisan. Vous pouvez améliorer vos compétences dans toutes ces classes avec un seul personnage ; vous n'avez pas besoin de vous concentrer sur une seule voie. Par exemple, vous pouvez, à votre guise, vous entraîner au maniement de la pique, mettre vos amplificateurs magiques pour soigner ou lancer des sorts offensifs, ramasser votre pioche et récolter des matériaux, ou vous lancer dans la fabrication d'une nouvelle armure ou d'une nouvelle arme. Vous avez une liberté absolue !\n\n\n",
      title: 'Sans classe',
    },
    multiplatform: {
      text:
        'Ryzom, Free To Play, peut être téléchargé pour Windows, Linux et Mac.',
      title: 'Windows, Linux & Mac',
    },
    forage: {
      text:
        "Ryzom dispose d'un système de forage et d'artisanat avancé.\n\nRécolte - il vous permet de forer des matériaux dans le sol en fonction de la saison et du temps et de votre compétence. Ce n'est pas si simple une fois que les prédateurs s'en mêlent !\n\nArtisanat - Une combinaison minutieuse de matériaux en fonction de leurs caractéristiques et de leurs couleurs, permet de produire des armes et des armures uniques. Des milliers de recettes sont possibles.\n\n\n\n",
      title: 'Forage & récolte avancés',
    },
    pvp: {
      text:
        "Vous pouvez vous battre partout, mais seulement quand vous le souhaitez. Duels, batailles d'avant-postes, combats de factions, de régions… Vous êtes libre d'y participer… ou de vous abstenir !\n",
      title: 'JcJ consensuel',
    },
    community: {
      text:
        "Ryzom est disponible en allemand, anglais, espagnol, français et russe, et présente une grande communauté multilingue qui est mature, serviable et amicale. Chacun peut parler et jouer des rôles dans sa propre langue ; il n'est donc pas obligatoire de parler une langue étrangère. Rejoignez-nous pour faire partie de la famille !\n",
      title: 'Une véritable communauté',
    },
    title:
      'Entrez dans un MMORPG de science-fiction extraordinaire et plongez dans son monde organique vivant unique !',
    invasions: {
      text:
        "Imaginez-vous, devant une porte de ville, regardant une colline lointaine et apercevant les éclaireurs d'un essaim de Kitin. Imaginez maintenant des centaines de kitins qui les suivent, se déplaçant comme une armée unie et grouillante.\n\nLes grandes batailles exigent la coopération d'une grande partie de la communauté pour contrer ces invasions menaçantes. Et si, un jour, les Kitins gagnaient à nouveau, menaçant l'hominité pour la troisième fois ?\n",
      title: 'Invasions',
    },
    environment: {
      text:
        "Ryzom est un monde persistant en 3D composé de cinq écosystèmes luxuriants et variés. Le paysage, le comportement de la faune et la disponibilité des matériaux dans le sol changent selon la saison, le temps et l'heure. Découvrez la beauté de la Forêt au printemps, livrez bataille dans la Jungle pendant un orage sous un ciel d'été strié d'éclairs, perdez-vous dans les dunes du Désert à l'automne, ou détendez-vous en profitant des reflets d'un soleil d'hiver sur l'eau pure des Lacs.\n\n\n",
      title: 'Des environnements dynamiques',
    },
    freedom: {
      text:
        "Dans Ryzom, il n'y a pas de classes à choisir. Développez harmonieusement toutes les compétences de votre personnage ou choisissez de vous concentrer sur une compétence et devenez un maître. Vous n'êtes pas obligé de choisir une faction lorsque vous créez votre personnage. Rejoignez une faction pendant vos voyages, ou non.\n\nVous n'êtes pas obligé de combattre en JcJ, mais vous le pouvez. Rejoignez une guilde dès que vous êtes prêt à le faire et construisez des alliances, ou commencez simplement vos aventures seul ou avec vos amis.\n\nCherchez les matériaux les plus précieux sur Atys pour créer votre propre équipement unique avec lequel il devient plus facile de vous défendre contre vos ennemis. Chassez les rois les plus puissants sur Atys. Gagnez un avant-poste et forez des matériaux pour votre guilde.\n\nFaites votre propre chemin ou ayez un impact sur la politique. Développez votre personnage comme vous le souhaitez et commencez votre propre histoire.\n\nC'est à vous de décider quoi faire. Vous êtes libre.",
      title: "La liberté : rien n'est plus important !",
    },
    opensource: {
      text:
        "Ryzom est l'un des rares MMORPG commercial qui soit entièrement open source : client, serveur, outils et médias. Être open source signifie que Ryzom s'appuie sur une communauté dynamique qui fournit des correctifs et des fonctionnalités au-delà de ce que l'équipe peut fournir. Cela offre aux joueurs une opportunité unique de s'impliquer dans le développement du jeu.\n\nPour plus d'informations sur Ryzom Core, visitez le [Ryzom Core Blog](https://ryzomcore.atlassian.net/wiki/spaces/RC/overview). Pour parcourir les ressources artistiques, veuillez visiter le [Ryzom Media Repository](https://github.com/ryzom/ryzomcore-assets).\n",
      title: 'Opensource',
    },
  },
  website: {
    meta: {
      description:
        'Un MMORPG sans classe vous laissant la liberté, un cadre de science fantasy favorisant le jeu de rôle, des événements en direct, un artisanat élaboré et des environnements dynamiques.',
    },
    title: 'Ryzom - Free to Play Open-Source MMORPG | Windows, Mac & Linux',
  },
  footer: {
    owner: 'Winch Gate Property Limited',
    copyright: 'Copyright © {year}. Tous droits réservés.',
    pegi: {
      online: 'Jeu en ligne',
      violence: 'Violence',
    },
    privacy: 'Politique de confidentialité',
    tos: "Conditions d'utilisation",
    community: {
      bmsite: 'Ballistic Mystix',
      armory: 'Ryzom Armory',
      forge: 'Ryzom Forge',
      wiki: 'Wiki',
      title: 'Communauté',
    },
    follow: {
      youtube: 'Youtube',
      twitter: 'Twitter',
      vimeo: 'Vimeo',
      flickr: 'Flickr',
      facebook: 'Facebook',
      title: 'Suivre Ryzom',
    },
    support: {
      feedback: 'Vos retours',
      forums: 'Forums',
      contact: 'Nous contacter',
      title: 'Assistance',
    },
  },
  index: {
    calendar: {
      title: 'prochains évènements ',
      viewMore: 'voir tous les évènements',
    },
    news: {
      noNews:
        "Il n'y a actuellement aucune nouvelle à montrer. Veuillez vérifier plus tard.",
      title: 'dernières nouvelles & mises à jour',
      viewAll: 'voir toutes les nouvelles',
    },
    features: {
      atys: 'Atys : un monde vivant !',
      roleplay: 'Jeu de rôle pour vaincre ! Vous en serez époustouflé.',
      freedom: "La liberté : rien n'est plus important !",
      community: 'Une véritable communauté',
      title: 'explorez Ryzom',
      viewAll: 'voir toutes les caractéristiques',
    },
    forge: {
      playersCreate: 'les joueurs créent',
      openSource: 'open-source',
      supportRyzom: 'soutenez Ryzom',
    },
    wiki: {
      visit: 'visitez le wiki de Ryzom',
      title: 'partez à la découverte',
    },
  },
  privacy: {
    preamble: {
      title: 'Confidentialité et protection des données',
      content:
        "Winch Gate Property Limited (ci-après dénommé « Winch Gate »), domicilée à 3030 LIMASSOL (Chypre), gère le jeu « Ryzom » et le site Internet « [www.ryzom.com](https://www.ryzom.com) ». Winch Gate adhère au Règlement Général sur la Protection des Données (GDPR, applicable au 25 mai 2018), respecte la vie privée de ses visiteurs en ligne et reconnaît l'importance de fournir un environnement sécurisé pour les informations qu'il collecte.\n\nWinch Gate s'engage à fournir à ses visiteurs en ligne un document contenant toutes les informations sur la nature et les modalités de la collecte, du traitement et de l'utilisation des données personnelles (« Politique de confidentialité »).\n\nEn utilisant ce site web, vous acceptez la Politique de confidentialité.\n\nVeuillez noter que la Politique de confidentialité de Winch Gate ne s'applique pas lorsque vous accédez à d'autres sites via les liens web situés sur les sites Internet de Winch Gate, car nous n'avons aucun contrôle sur les activités de ces autres sites. En outre, la présente Politique de confidentialité peut varier de façon ponctuelle ; veuillez consulter ce document périodiquement afin d'être informé de toute modification.",
    },
    policy: {
      title: 'Politique de confidentialité',
      content:
        "Winch Gate ne collectera que les données personnelles absolument nécessaires à la création et à la maintenance des comptes et des personnages au sein de Ryzom. Winch Gate ne partagera jamais les données personnelles de ses visiteurs en ligne et/ou des utilisateurs du jeu avec des tiers.\n\n## Qu'est ce que sont les données personnelles ?\nPar données personnelles, on entend toute information relative à une personne physique identifiée ou identifiable. Cela comprend les adresses IP et les données des cookies.\n\n## Droits des utilisateurs\nL'utilisateur doit être informé si Winch Gate collecte des données, pourquoi les données sont collectées, comment elles sont traitées et utilisées. L'utilisateur doit donner son consentement (pour en savoir plus, voir « Consentement »). En outre, l'utilisateur a le droit d'obtenir, sur simple demande, de l'information sur ses données enregistrées. Ce service est gratuit et peut être requis à tout moment par tout utilisateur (pour en savoir plus, voir « Droit d'accès aux données »). En cas de fuite, l'utilisateur sera informé dans les 72 heures, tout comme le bureau responsable de la protection des données (pour en savoir plus, voir « Notification de violation de confidentialité »). L'utilisateur a également le droit d'être oublié, ce qui signifie que Winch Gate doit effacer toutes les données personnelles à la demande de l'utilisateur (pour en savoir plus, voir « Droit à l'oubli »).\n\n## Consentement\nUne demande de consentement doit être formulée, dans un langage simple et intelligible. Elle doit être présentée sous une forme aisément accessible et la finalité du traitement des données doit y être jointe. Le consentement doit pouvoir être distingué des autres services, il doit être donné librement et être facile à retirer, aussi facilement qu'il a été donné.\n\n## Quelles données collectons-nous ?\nWinch Gate collecte des données personnelles principalement pour le système de paiement du jeu « Ryzom ». Ceci nécessaire pour que l'utilisateur puisse bénéficier de l'expérience complète d'un compte dit « premium ». Ce service est volontaire. En outre, Winch Gate collecte de manière ponctuelle des données pour des enquêtes/sondages et des statistiques liées au jeu. Les sondages sont volontaires et la plupart du temps anonymes. Cependant, l'utilisateur est informé du processus de collecte des données avant le début de l'enquête et doit y consentir explicitement. Les statistiques de « Ryzom » sont déclarées dans le CLUF et doivent être acceptées par l'utilisateur. Winch Gate informera l'utilisateur s'il collecte des données personnelles pour des statistiques sur d'autres services (« [www.ryzom.com](https://www.ryzom.com) ») et demandera son consentement dans le cadre du service concerné.\n\n## Pourquoi collectons-nous des données ?\nGrâce à ces collectes de données, Winch Gate est en mesure de développer des stratégies futures, d'améliorer l'expérience utilisateur, d'élargir son offre et de cibler un groupe plus spécifique. Globalement, ces données sont utilisées pour optimiser l'expérience des clients et utilisateurs de Winch Gate et de ses services. De plus, certaines données sont nécessaires au processus de paiement.\n\n## Droit d'accès aux données\nLes utilisateurs ont le droit d'accéder à leurs données stockées. Ils peuvent envoyer une demande à tout moment à [https://support.ryzom.com](https://support.ryzom.com). Winch Gate y répondra en fournissant dans les plus brefs délais des informations sur toutes les données personnelles enregistrées. Ce service est 100% gratuit et n'a aucune limite d'utilisation. Veuillez noter cependant que Winch Gate se réserve le droit de refuser plusieurs demandes formulées dans un laps de temps très court et qui concerneraient la même personne physique (ou le même compte).\n\n## Droit à l'oubli\nL'utilisateur a le droit à l'oubli, ce qui signifie que Winch Gate doit, à la demande de l'utilisateur, effacer toutes ses données personnelles stockées. Veuillez noter que, en cas de suspicion d'activités criminelles, Winch Gate a le droit (et le devoir) de refuser cette demande jusqu'à ce que les autorités compétentes informées donnent leur accord pour l'effacement des données personnelles. En outre, dans le cas où vous exercez votre droit à l'oubli, certains produits Winch Gate peuvent ne plus vous être accessibles, en raison de la perte des données qu'ils requièrent (par exemple les « comptes premium »). Winch Gate informera l'utilisateur des conséquences avant d'effacer définitivement toutes ses données personnelles et exigera son consentement.\n\n## Notification de violation de confidentialité\nEn cas de fuite (piratage), Winch Gate enverra une notification de violation à tout utilisateur concerné et au bureau de protection des données responsable, dans les 72 heures suivant la prise de connaissance de la violation de confidentialité. Cette notification contiendra des informations sur la date à laquelle la violation a été constatée, la personne physique et les données concernées, ainsi que les mesures éventuellement déjà prises.",
    },
  },
  login: {
    password: 'Mot de passe',
    error: {
      server: 'erreur de serveur',
      credentials: 'des pouvoirs non valables',
    },
    username: 'Nom du compte',
  },
  menu: {
    forums: 'forums',
    gameInfo: 'le jeu',
    universe: 'univers',
    wiki: 'wiki',
    events: 'événements',
    home: 'accueil',
  },
  button: {
    downloadRyzom: 'Télécharger Ryzom',
    continueSaga: 'Continuer la Saga',
    login: 'se connecter',
    signUp: 'Créer un compte',
  },
  download: {
    systemRequirements: {
      os: {
        title: "Système d'exploitation",
      },
      linux: {
        sound: 'Compatible avec OpenAL',
        cpu: 'Un processeur simple cœur à 1GHz',
        hdd: "10 GB d'espace disque disponible",
        graphics: 'OpenGL 1.2 avec 32 MB VRAM',
        title: 'Linux',
        ram: '2 GB de mémoire',
      },
      sound: {
        title: 'Carte son',
      },
      cpu: {
        title: 'Processeur',
      },
      hdd: {
        title: 'Espace disque',
      },
      graphics: {
        title: 'Graphiques',
      },
      title: 'Configuration requise',
      windows: {
        os: 'Windows Vista',
        sound: 'Compatible avec DirectX',
        cpu: 'Un processeur simple cœur à 1GHz',
        hdd: "10 GB d'espace disque disponible",
        graphics: 'OpenGL 1.2 ou DirectX 9.0 avec 32 MB VRAM',
        title: 'Windows',
        directx: 'Version 9.0c',
        ram: '2 GB de mémoire',
      },
      minimum: 'minimale',
      directx: {
        title: 'DirectX',
      },
      mac: {
        os: 'OS X 10.8',
        sound: 'Compatible avec OpenAL',
        cpu: 'Un processeur simple cœur à 1GHz',
        hdd: "10 GB d'espace disque disponible",
        graphics: 'OpenGL 1.2 avec 32 MB VRAM',
        title: 'Mac OS',
        ram: '2 GB de mémoire',
      },
      ram: {
        title: 'Mémoire vive',
      },
    },
    title: 'Télécharger Ryzom',
  },
  premium: {
    button: {
      manageSub: 'Gérer votre abonnement',
    },
    sub: {
      button: 'prendre un abonnement',
      title: 'Vous jouez  \ndéjà ?',
    },
    compare: {
      title: 'Comparez ! Le choix vous appartient.',
    },
    comparison: {
      premium: {
        maxlevel: 'Niveau maximal 250',
        xp: 'XP double',
        maxquality: "Qualité maximale de l'équipement 250",
        storage:
          '* Une monture & trois animaux de bât\n* Un appartement\n* Un hall de guilde',
        title: 'premium',
        support:
          "* Aide de l'équipe d'assistance\n* Restauration des anciens comptes (ouverts avant la fusion des serveurs) pour retrouver les personnages\n* Restauration de personnage(s) en cas d'effacement volontaire ou accidentel\n* Transfert de personnage(s) entre comptes Premium souscrits par la même personne\n* Changement de pseudonyme autorisé tous les six mois pour chaque personnage",
      },
      free: {
        maxlevel: 'Niveau maximal 125',
        xp: 'XP ordinaire',
        maxquality: "Qualité maximale de l'équipement 150",
        storage: 'Une monture',
        title: 'gratuit',
        support:
          "* Aide de l'Équipe d'Assistance\n* Un changement de pseudonyme autorisé pour chacun de vos personnages",
      },
    },
    intro: {
      text:
        "En publiant son code, Ryzom a ouvert le jeu au monde du Libre, ce qui en fait le seul MMORPG open-source, jouable sur Windows, LInux et Mac.\n\nAssurer la maintenance de Ryzom implique cependant des coûts, tels que la rémunération de la petite équipe d'employés et l'achat de serveurs. C'est pourquoi, même si on peut jouer à Ryzom en _Free to Play_, les souscriptions d'abonnement sont, pour lui, vitales.\n\nPour remercier les joueurs choisissant de s'abonner, certaines fonctionnalités du jeu sont débloquées pour eux et certains bonus leurs sont offerts : la possibilité d'emmener leurs personnages au niveau 250 au lieu de 125, le doublement des gains de points d'expérience et l'accès à des moyens de stockage supplémentaires.\n\n**Pour soutenir Ryzom, faites le choix de l'abonnement !**",
      title: 'Pourquoi payer ?',
    },
    rates: {
      perMonth: ' mois',
      m1: {
        title: 'Abonnement mensuel ',
      },
      m3: {
        title: 'Abonnement trimestriel  ',
      },
      y1: {
        title: 'Abonnement annuel ',
      },
      m6: {
        title: 'Abonnement semestriel  ',
      },
      title: 'Tarifs',
    },
    donate: {
      button: 'faire un don',
      title: 'Aidez la Saga\nà se poursuivre !',
    },
    title: "Soutenez Ryzom et bénéficiez d'avantages !",
    signup: {
      button: 'Souscrivez et commencez à jouer',
      title: 'Intéressé par  \nRyzom ?',
    },
  },
  returning: {
    download: {
      text:
        "Désireux de reprendre le jeu ? Télécharge Ryzom GRATUITEMENT et continue tes aventures sur Atys dès aujourd'hui !",
      title: 'Télécharger\nle jeu',
      modal: {
        onSteam: 'Ryzom sur Steam',
        forMac: 'pour Mac',
        forWin: 'pour Windows',
        forLinux: 'pour Linux',
      },
    },
    recovery: {
      text:
        "Tu ne te souviens pas de ton nom de compte ou de ton mot de passe ? Nous pouvons t'aider !",
      title: 'Restauration\nde compte',
    },
    resub: {
      link: 'en savoir plus',
      text:
        "Tu n'as pas besoin d'abonnement pour te connecter et discuter avec ta guilde et tes amis, mais il t'en faudra un pour pouvoir jouer des personnages de niveau supérieur à 125.",
      title: "Renouveler\nl'abonnement",
    },
    login: {
      title: 'Compte  \nlogin',
    },
    title:
      "C'est bon de te revoir ! Si tu rencontres des problèmes, les trois liens ci-dessous t'aideront. Maintenant, vas-y, tes amis t'attendent !",
    headline: 'Entrer dans le jeu',
    register: {
      text:
        'Nouveau à Ryzom ? Cliquez ici pour vous inscrire et créer un nouveau compte. Vous serez prêt à jouer en quelques minutes !',
      title: 'Créer  \ncompte',
    },
  },
  universe: {
    inhabitants: {
      fauna: {
        text:
          "Vous rencontrerez plusieurs espèces animales au sein des différents écosystèmes Atysiens.\n\nSi vous prenez le temps de les observer, vous découvrirez une faune vivante et réaliste, avec ses paisibles troupeaux d'herbivores qui migrent au gré des saisons, des prédateurs qui tentent régulièrement des attaques isolées, la proie se défendant au mieux. Vous pourrez être vous-même au cœur de l'action en tant que prédateur… ou proie !",
        title: 'Faune',
      },
      races: {
        fyros: {
          text:
            "Les Fyros se revendiquent comme le peuple guerrier d'Atys. Avides d'aventure, ils chassent et se battent contre ceux qu'ils considèrent comme dignes de leurs défis. Ils sont également avides de connaissance et fort désireux de découvrir les mystères du passé d'Atys. De nature peu fervente, leur rapport à la religion a toujours été marqué par leur pragmatisme.\n\nLa plupart des Fyros habitent le Désert, au sein duquel ils ont établi leur Empire. Ils valorisent la Vérité, l'Honneur, la Discipline et la Justice. Accomplir leur rite de citoyenneté permet de devenir Patriote Fyros et de bénéficier de divers atouts et récompenses.\n\nLeur langue ancestrale est le fyrk.",
          title: 'Fyros',
        },
        matis: {
          text:
            "Les Matis sont un peuple sophistiqué, cultivé et ambitieux. Ils maîtrisent la botanique et construisent leurs maisons et leurs outils à partir de plantes vivantes. À la fois artistes et conquérants, ils croient que chaque homin doit gagner sa place dans la société. Ils sont les plus fidèles Suivants de la Karavan.\n\nLa plupart des Matis vivent dans de la Forêt, où ils ont établi leur Royaume, une monarchie de droit divin. Ils accordent de l'importance à la Loyauté envers le Roi et Jena, à l'Accomplissement esthétique et à la Compétition. Accomplir leur rite de citoyenneté permet de devenir Vassal Matis et de bénéficier de divers atouts et récompenses.\n\nLeur langue est le Mateis",
          title: 'Matis',
        },
        text:
          'Atys est habitée par de nombreux êtres pensants. Y vivent les Kamis, mystérieuses créatures de forme changeante, vouées à la protection de la nature ; les apôtres de la Karavan, humanoïdes cuirassés contrôlant une technologie inconnue ; les homins enfin, qui, hors une poignée de tribus primitives, se répartissent, quant à eux, en quatre nations : Fyros, Matis, Tryker et Zoraï.',
        title: 'nations',
        tryker: {
          text:
            "Les plus petits par la taille, les Trykers sont pacifiques, dévoués à la liberté et bon vivants. Ils rêvent d'un monde pacifique, sans maîtres ni esclaves. Leur curiosité fait d'eux d'excellents explorateurs et inventeurs et ils maîtrisent parfaitement la technologie des moulins à vent. Souvent, leur interprétation personnelle de la religion suscite la colère des dogmatiques.\n\nLa plupart des Trykers considèrent les Lacs comme leur foyer, où ils ont établi leur Fédération, dirigée par un Gouverneur. Ils valorisent la Liberté, l'Égalité et le Partage. Accomplir leur rite de citoyenneté permet de devenir Citoyen Tryker et de bénéficier de divers atouts et récompenses.\n\nIls ont leur propre langue, le Tyll Tryker.",
          title: 'Tryker',
        },
        zorai: {
          text:
            "De nature calme et spirituelle, les Zoraïs sont les plus grands de tous les homins. Le Masque couvre leur visage, un cadeau béni des Kamis, qu'ils servent avec ferveur. Leur devoir sacré est d'assurer l'équilibre de la nature et de lutter contre la Goo, le fléau de la Jungle. Toujours à la recherche de la connaissance, ils maîtrisent le magnétisme.\n\nLa plupart des Zoraï restent dans la Jungle, où ils ont établi leur Théocratie, dirigée par le Grand Sage. Ils valorisent l'Accomplissement spirituel, la Sagesse et le Respect de la Nature. Accomplir leur rite de citoyenneté permet de devenir un Initié Zoraï et de bénéficier de divers atouts et récompenses.\n\nIls parlent le Taki Zoraï.",
          title: 'Zoraï',
        },
      },
      flora: {
        text:
          'Atys abrite une flore abondante parmi laquelle on compte plusieurs espèces de plantes intelligentes.\n\nCes plantes vous fourniront des matériaux utiles pour votre artisanat, mais vous devrez apprendre à échapper à leurs formidables attaques, y compris magiques !',
        title: 'Flore',
      },
      floraFauna: {
        text:
          "Atys est peuplé de nombreuses espèces animales et végétales. Leur chasse (ou leur « cueillette ») permet aux homins de rassembler les matières premières nécessaires à la fabrication d'objets artisanaux : peaux, crocs, os, cornes, ligaments, sabots, yeux, ongles… Tout est utilisable pour fabriquer des vêtements, des armes, des bijoux…\n\nPour profiter de ces dépouilles vous devrez apprendre, par vous-même ou de vos aînés, différentes techniques de chasse, car chaque espèce animale ou végétale a son propre comportement au combat.",
        title: 'Faune et flore',
      },
      kitins: {
        text:
          "Les animaux les plus dangereux pour les homins sont, sans aucun doute, les kitins. Organisés, puissants et impitoyables, ces géants insectoides dotés d'une organisation sociale élaborée ont ravagé Atys à deux reprises, massacrant les homins sans discernement et effaçant des civilisations entières.\n\nDepuis lors, l'Hominité se reconstruit pas à pas, mais le danger est constamment présent…",
        title: 'Kitins',
      },
      title: 'Les habitants',
    },
    planet: {
      text:
        "L'exploration de ce monde vous conduira dans ses multiples écosystèmes, du désert brûlant aux lacs sublimes, de la forêt majestueuse à la jungle luxuriante ravagée par un fléau mortel, et même dans les profondeurs obscures de la planète, parcourues de rivières de sève.\n\nPartez à la conquête d'Atys ou venez découvrir ses mystères et ses légendes ! Mais prenez garde, car le danger guette à chaque détour de ces paysages idylliques.",
      title: 'La planète',
    },
    continents: {
      forest: {
        text:
          "La Forêt est le domaine des Matis, qui ont construit là leur Royaume et façonné leur capitale, Yrkanis, dans des arbres vivants.\n\nConnu sous le nom de Sommets Verdoyants, ce continent est couvert d'arbres impressionnants et majestueux et bénéficie d'un climat tempéré ponctué par quatre saisons prononcées",
        title: 'Forêt',
      },
      jungle: {
        text:
          "Les Zoraïs ont construit leurs villes dans le labyrinthe végétal de la Jungle, autour de leur capitale, Zora.\n\nCe continent est aussi appelé le Pays Malade, car il est infecté par la puanteur de la Goo, un cancer qui érode inexorablement la vie, ravageant l'habitat, et faisant muter la flore et la faune en d'horribles créatures.",
        title: 'Jungle',
      },
      lakes: {
        text:
          "L'archipel des Lacs comprend une myriade d'îles habitées par les Trykers. Ils y ont fondé leur Fédération et construit des villes flottantes, dont la capitale est Fairhaven.\n\nCe continent, connu sous le nom d'Aeden Aqueous, offre un paysage idyllique avec ses plages de sciure blanche, ses magnifiques chutes d'eau et ses eaux cristallines à perte de vue. Le climat y est ensoleillé toute l'année",
        title: 'Lacs',
      },
      roots: {
        text:
          "Les Primes Racines constituent l'écosystème le plus mystérieux d'Atys. Plongeant dans les profondeurs de la planète, c'est l'immense continent souterrain, dont le sol, les murs et le plafond sont peuplés de plantes phosphorescentes et d'étranges créatures.\n\nAussi belles que dangereuses, les Primes Racines cachent en leur sein les vestiges énigmatiques du passé et abritent d'inquiétants nids de Kitins. Le climat y est en permanence frais et humide.",
        title: 'Primes Racines',
      },
      desert: {
        text:
          "Ce continent, connu sous le nom de Désert Ardent, est le domaine des Fyros qui ont fondé leur Empire autour de la capitale forteresse de Pyr.\n\nDes vents forts et violents, de longues périodes de sécheresse, des températures étouffantes pendant la journée et glaciales durant la nuit, font de ces dunes brûlantes un des endroits les plus inhospitaliers d'Atys.",
        title: 'Désert',
      },
      title: 'Les continents',
    },
    factions: {
      kami: {
        text:
          "Rassemble tous les Disciples des Kamis, les êtres protecteurs en communion permanente avec le Kami suprême, Ma-Duk.\n\nTout homin désireux d'entrer dans la faction Kami doit accomplir un rite d'allégeance et jurer de respecter les Lois de Ma-Duk.\n\nLes Kamis offrent divers services à leurs Disciples, les deux plus importants étant la résurrection et la téléportation dans toutes les régions d'Atys. Ils offrent également des récompenses uniques à ceux qui  combattent en leur nom les Suivants de la Karavan.\n\nLa faction affronte régulièrement la faction Karavan lors des batailles d'avant-postes, dans le but d'exploiter leurs précieuses ressources à l'aide des imposantes vrilles écologiques fournies par les Kamis.",
        title: 'kami',
      },
      ranger: {
        text:
          "Rassemble tous les homins qui ont le désir de libérer l'Hominité de la menace kitine.\n\nLa guilde des Rangers d'Atys, créée après le Premier Grand Essaim, est animée par un idéal de fraternité. Les Rangers croient que les homins doivent vivre en paix, sans division ; ils s'interdisent donc de prendre part aux querelles entre homins, et refusent de lever la main sur eux, sauf en cas de légitime défense.\n\nEn plus de patrouiller sans cesse sur Atys, les Rangers tiennent également des bases stratégiques sur l'île de Silan et dans le Bois d'Almati. Le camp de Silan, géré par Chiang, accueille les réfugiés qui arrivent sur les Nouvelles Terres. Le camp du Bois d'Almati, géré par la Guide Ranger Orphie Dradius, sert de centre logistique et surveille la Kitinière toute proche.\n\nPendant le Deuxième Grand Essaim, les Rangers ont aidé à sauver l'Hominité. Leurs plans de bataille ont permis de sauver la population en convainquant les Kamis, la Karavan, les Nations et les Trytonistes d'y collaborer.\n\nTout en faisant tout leur possible pour éviter un troisième Grand essaim, les Rangers surveillent de près les Kitins, éliminent leurs éclaireurs trop curieux, et développent de nouvelles techniques de combat et des tactiques discrétionnaires.",
        title: 'ranger',
      },
      neutral: {
        text:
          "Certains homins ont choisi de ne s'allier avec aucune des deux Puissances, sans pour autant les combattre : ils sont neutres sur le plan religieux.\n\nBien qu'ils ne constituent pas officiellement une faction, cette neutralité religieuse qui les rassemble loin de Ma-Duk et de Jena est souvent considérée comme telle.",
        title: 'neutre',
      },
      karavan: {
        text:
          "Rassemble tous les Suivants de la Karavan, les apôtres hominoïdes de la déesse Jena.\n\nTout homin désireux d'entrer dans la faction Karavan doit accomplir un rite d'allégeance et jurer de respecter les Lois de Jena.\n\nLa Karavan offre divers services à ses suivants, les deux plus importants étant la résurrection et la téléportation dans toutes les régions d'Atys. Elle offre également des récompenses uniques à ceux qui combattent en son nom les Disciples des Kamis.\n\nLa faction affronte régulièrement la faction Kami lors des batailles d'avant-postes, dans le but d'exploiter leurs précieuses ressources à l'aide des énormes foreuses métalliques fournies par la Karavan.",
        title: 'karavan',
      },
      text:
        "Deux Puissances, différentes mais également mystérieuses, cherchent à contrôler le destin d'Atys : la Karavan, qui veille sur les Homins et propage la parole de la déesse Jena, et les Kamis, gardiens des plantes et de l'équilibre du monde. Tous deux possèdent des pouvoirs uniques, grâce auxquels ils protègent et aident les homins qui suivent leurs lois divines. Depuis des siècles, ces deux Puissances s'observent mutuellement sans confrontation. Leur protection vous sera d'une grande aide lors de votre voyage sur Atys, et leur colère pourrait être mortelle… Alors, choisissez votre camp avec soin.\n\nCependant, si vous choisissez, à vos risques et périls, de ne servir aucune des deux Puissances, d'autres portes vous sont ouvertes. Vous pouvez choisir la Neutralité Religieuse, et ne pas prendre parti dans la lutte religieuse, ou devenir un Trytoniste et combattre dans l'ombre pour libérer l'Hominité des Puissances, ou même rejoindre les rangs des Maraudeurs et combattre à la fois les Puissances et les Nations. Votre chemin pourrait aussi vous mener sur la voie des Rangers, qui comptent sur l'unification des Puissances et des Nations pour combattre l'ennemi Kitin.",
      title: 'les factions',
      marauder: {
        text:
          "Rassemble tous les homins rebelles dont l'ambition est  de mettre fin à la domination des Puissances et des Nations pour conquérir Atys. La faction Maraudeur est issue de l'alliance des clans formés par les homins abandonnés dans les Vieilles Terres lors du Premier Grand Essaim.\n\nCette société, gouvernée par la survie du plus fort, est régie par la Combativité, la Conquête et la Survie. Au fil du temps, les Clans Maraudeurs ont développé leur propre langage : le Marund.\n\nLes Clans Maraudeurs ne possèdent aucun territoire dans les Nouvelles Terres, c'est pourquoi ils ont installé un campement dans une région revendiquée à la fois par les Fyros et les Matis.\n\nTout homin qui veut entrer dans la faction Maraudeur doit accomplir un rite d'allégeance et jurer de répondre aux appels de Melkiar, le Varinx Noir, chef de guerre des Clans Maraudeurs. En échange, ils bénéficient de services spécifiques de résurrection et de téléportation sur toute la planète et ses guerriers se voient offrir des récompenses uniques.\n\nDans le but d'exploiter leurs précieuses ressources, les Clans Maraudeurs tentent régulièrement de conquérir des avant-postes en livrant bataille.",
        title: 'maraudeur',
      },
      trytonist: {
        text:
          "Rassemble tous les Homins qui veulent libérer Atys du joug des Puissances, considérant les récompenses attrayantes qu'ils offrent comme de simples appâts pour asservir l'Hominité.\n\nLe guide des Trytonistes est Elias Tryton, un être mystérieux considéré par certains comme un renégat de la Karavane, et par d'autres comme le mari de la déesse Jéna.\n\nComme ils sont traqués par la Karavane et à peine tolérés par les Kamis, les Trytonistes doivent se cacher pour survivre. Les Trytonistes sont actuellement répartis dans des guildes de toutes les religions et nations, et ont développé un réseau secret utilisant des noms de code.\n\nLes Trytonistes reçoivent leurs instructions de la Guilde d'Elias, dont le chef, Nicho, est directement en contact avec Elias Tryton.\n\n_Attention : cette faction n'est actuellement jouable qu'en mode roleplay._",
        title: 'Trytoniste',
      },
    },
  },
  header: {
    forums: 'Forums',
    logout: 'se déconnecter',
    playNow: 'jouez !',
    login: 'se connecter',
    cookies: {
      button: {
        decline: 'Continuez avec les cookies nécessaires uniquement.',
        accept: 'Acceptez tous les cookies.',
      },
      text: 'Ryzom utilise des cookies à des fins de suivi et de marketing.',
    },
    webig: 'Web-Apps',
    billing: 'Compte',
  },
  tos: {
    coc: 'Code de conduite',
    intro:
      "Les règles suivantes concernent les situations les plus courantes, non l'ensemble des situations imaginables. L'équipe d'assistance clientèle se réserve le droit de modifier ces règles si devait se présenter une nouvelle situation qui nécessiterait des éclaircissements supplémentaires.\n\nSi un cas est trop spécifique pour relever de ces règles mais nécessite néanmoins l'intervention de l'équipe d'assistance clientèle, cette dernière peut agir et prononcer, à tout moment, des avertissements ou des sanctions sans être tenue d'en informer l'ensemble de la communauté.\n\nSi vous êtes témoin d'une violation ou d'une infraction aux règles, veuillez en informer le service d'assistance clientèle en utilisant l'un des moyens suivants :\n— envoi d'un ticket *via* le système accessible en jeu ;\n— conversation privée dans le jeu (commande /tell) avec un membre de équipe d'assistance clientèle (*CSR*) en ligne ;\n— demande formulée dans Ryzom.Chat (Rocket Chat) ;\n— envoi d'un courrier électronique à support@ryzom.com.",
    title: "Conditions générales d'utilisation",
    content:
      "## L’équipe d'assistance\nSes membres (dits aussi CSR) sont des joueurs bénévoles sous clause de non divulgation (NDA) et sont tenus de ne pas révéler leur identité aux autres joueurs ni aux autres membres des équipes Ryzom. Si un joueur pense connaître l’identité du joueur derrière un CSR (ou derrière tout autre membre sous NDA d’une équipe Ryzom ), il lui est interdit de le divulguer sous peine de sanctions (infraction majeure, voir **B. Avertissements et sanctions**).\n\nLes membres de l'équipe d'assistance ont en charge de résoudre les différents problèmes liés au jeu (techniques, disciplinaires, etc). Ils doivent se comporter avec politesse et courtoisie en toute circonstance. Si malgré tout vous aviez un différend avec l'un d'entre eux, vous pouvez adresser une plainte dûment documentée à support@ryzom.com qui sera transmise au responsable de l'équipe. Toute plainte abusive à l’encontre d’un CSR pourra faire l’objet d’une sanction.\n\nQuand un membre de l'équipe d'assistance vous contacte, vous devez suivre ses instructions et lui répondre avec politesse et courtoisie. Tout manquement sera sanctionné suivant l’échelle de sanctions prévue (voir **B. Avertissements et sanctions**).\n\nNous vous rappelons que les membres de l'équipe d'assistance donnent de leur temps (temps de jeu et temps de vie) pour que chacun puisse jouer en toute tranquillité : essayez d’avoir cela à l’esprit quand vous en contactez un. Ceci est naturellement valable pour tous les membres de toutes les équipes Ryzom : CSR, développeurs, animateurs, etc.\n\nL’équipe d'assistance est soumise à des règles strictes qui interdisent à ses membres en jeu d'intégrer une équipe de joueurs, d'échanger ou de se battre en duel avec un joueur, ou de le ressusciter. Ces règles leur interdisent aussi d’utiliser ses pouvoirs à toute fin autre que celles prévues dans le cadre de leur fonction sous peine de suppression de leur compte de CSR, voire de leur compte joueur. De plus les CSR n’ont pas le droit de traiter des tickets impliquant leurs propres personnages-joueurs ou les guildes de ces derniers.\nEn accord avec ces règles, des outils de contrôle enregistrent toutes les commandes critiques utilisées par les CSR (ainsi que par les membres des autres équipes) et les affichent en interne pour permettre un contrôle par tous en temps réel. Chaque connexion d’un membre du Support (et des autres équipes Ryzom) est également visible afin que chacun puisse vérifier que nul n’a usurpé son compte.\n\nToutes les décisions prises par l’équipe d'assistance sont des décisions collectives et de ce fait vous pouvez, selon la nature du problème, être contacté par plusieurs membres de l'équipe pour le même problème.\n\n## A. Différend avec un joueur\nEn cas de différend avec un autre joueur, nous vous conseillons d'essayer de dialoguer avec ce dernier et de régler le différend à l'amiable avec lui. Un ticket doit constituer un recours de dernière instance, à utiliser seulement si le dialogue est impossible à établir et qu'il faut qu'un tiers intervienne.\n\nSi vous en venez à envoyer ledit ticket, nous vous prions d'y joindre au moins une capture d'écran à titre de référence. **Pour être recevable, il est indispensable que la capture jointe montre l'écran complet du jeu et n'ait pas été retouchée (censure ou autre).** Elle doit montrer clairement l'abus que vous souhaitez dénoncer. Notez à ce propos qu’une capture d’écran montrant la fenêtre carte ouverte nous permet de vérifier la date (date atysienne aisément traduite en date réelle). Nous acceptons également les vidéos. Pour toute question, vous pouvez nous contacter directement en jeu (via /tell) si un CSR est connecté, sur Rocket Chat (via https://chat.ryzom.com/) ou, enfin, via courriel à l'adresse suivante : support@ryzom.com.\n\nN.B. Toute utilisation abusive du système de tickets sera documentée et sanctionnée.\n\n## B. Avertissements et sanctions\n\nL'équipe d'assistance se réserve le droit d'appliquer des avertissements ou des sanctions selon la gravité de la faute, la réaction du joueur incriminé vis-à-vis de l'équipe et ses antécédents (sanctions ou avertissements antérieurs). Ainsi un joueur peut recevoir une simple remontrance, un avertissement officiel, une suspension allant de 24h à plusieurs jours, ou un bannissement définitif.\n\n### Échelles de sanctions\nL’échelle des sanctions pour une **infraction mineure** est la suivante : avertissement officiel, suspension pour 24h, suspension pour trois jours, suspension pour une semaine et enfin suspension pour deux semaines. Chacune de ces sanctions peut être précédée ou suivie d’une privation d'expression (ou \"mute\") sur le canal où la été constatée l'infraction (canal en jeu, Rocket Chat, forum, etc.) pour la durée de la sanction.\nCette échelle des sanctions est réinitialisée après un an sans sanction.\n\nL’échelle des sanctions pour une **infraction majeure** est la suivante : suspension pour une semaine, suspension pour un mois, bannissement définitif du compte. Chacune de ces sanctions est suivie si nécessaire d'une éjection immédiate (kick) du serveur.\nCette échelle des sanctions est, pour les joueurs non bannis, réinitialisée après trois ans sans sanction.\n\nUne **Infraction critique** provoque le bannissement définitif du compte après avertissement du joueur par courriel.\n\nN.B. En cas de sanction grave et immédiate (suspension ou bannissement définitif), le joueur incriminé ne pouvant plus être contacté en jeu, le sera par l’intermédiaire d'un courriel **à la boîte électronique indiquée par son compte personnel**, d'où l'importance de donner une adresse mél réelle lors de l'inscription. Si le joueur en question ne reçoit pas le courriel ou s'il conteste la sanction, il peut contacter l'équipe d'assistance à l'adresse suivante : support@ryzom.com.\n\n## ANNONCES LÉGALES\nLes sanctions prévues par le Code de Conduite de Ryzom seront délivrées comme suit :\n- Les \"mutes\" et les avertissements verbaux pourront être appliqués par tout CSR.\n- Les avertissements officiels (par courriel) pourront être appliqués par tout CSR de grade supérieur ou égal à GM.\n- Les suspensions ne pourront être appliquées que par les CSR au grade de SGM.\n- Les bannissements de compte ne pourront être décidés et appliqués que par Winch Gate Limited.\n\n## II. Charte des noms\nLa charte des noms définit les règles qui régissent le choix du pseudonyme de votre personnage lorsqu'il est créé. Elle est établie par l'équipe d'assistance sous la direction de son responsable. \n\n### Choix du nom\nDans la phase finale du processus de création d'un de vos personnages, il vous est demandé de lui choisir un nom. Il est conseillé pour ce choix de s'inspirer de la Lore, afin que votre personnage s’intègre au mieux à l'univers de Ryzom. Si les idées vous manquent ou que les noms que vous choisissez sont déjà portés par d'autres, vous pouvez utiliser le générateur de noms aléatoires disponible durant cette phase. Un nom moins inspiré d'Atys sera néanmoins accepté pour peu qu'il soit conforme à la charte ci-dessous.\n\n### Changement de nom\nÀ tout moment, un membre du Support peut vous contacter et vous demander de choisir un autre nom s'il ne se conforme pas aux règles ci-dessous.\n\nVous pouvez demander un changement de nom à un membre de l'équipe d'assistance pour des raisons telles que jeu de rôle, désir d’abandonner un nom moqué par d’autres joueurs, anonymat… (liste non exhaustive). Votre demande doit être motivée et sera validée (ou non) par l'équipe d'assistance. \n\nLe nombre maximum de changements de pseudonyme dépend du type de votre compte :\n- Compte Free to Play : un changement de nom autorisé pour chaque personnage.\n- Compte Premium : un changement de nom autorisé tous les six mois pour chaque personnage.\n\n### Charte des noms et sanctions en cas d’entorse\nN.B. Si un joueur multiplie la création de personnages dont les noms enfreignent la charte dans la catégorie « Noms très offensants » décrite ci-après, il est passible des sanctions prévues par le Code de Conduite.\n\n#### 1. Noms très offensants : \nLe personnage est renommé immédiatement avec un nom par défaut et le joueur en est averti par courriel.\n\n- Vous ne pouvez pas utiliser des noms grossiers, racistes ou ethniquement offensants, obscènes, sexuels, des noms de drogues ou homonymes, ni des noms incluant des noms communs injurieux ou faisant référence à une partie de l'anatomie, et ceci dans n'importe quelle langue utilisée en jeu.\n- Vous ne pouvez pas utiliser un nom choisi pour vous faire passer pour un autre joueur, un employé de Winch Gate Property Limited ou un bénévole de l'équipe Ryzom, ni pour leur nuire de quelque manière que ce soit.\n\n#### 2. Noms offensants :\nLe personnage est renommé avec 24h de délai pour laisser le temps au joueur de proposer un nouveau nom. Ceci ne concerne que les personnages créés après la publication de la présente charte des noms.\n\n- Vous ne pouvez pas utiliser des noms qui sont offensants phonétiquement ou iraient à l'encontre de la charte des noms.\n- Vous ne pouvez pas utiliser des noms d'origine historique, religieuse, occulte ou assimilé (exemples : Jésus, Lucifer, Nosferatu, Crucifixion, Exorcisme).\n- Vous ne pouvez pas utiliser des noms contenant des titres tels que Maître, Roi, Reine, Seigneur, Monsieur, Mère, Dieu, etc. (liste non exhaustive).\n\n#### 3. Noms interdits : \nLe personnage est renommé avec trois jours de délai pour laisser le temps au joueur de proposer un nouveau pseudonyme. Ceci ne concerne que les personnages créés après la publication de la présente charte des noms.\n\n- Vous ne pouvez pas utiliser des noms protégés par un droit d'auteur ou des noms de marque, de matériaux ou de produits (exemples : Fujiansu, Whisky, Tylenol, Toshiba).\n- Vous ne pouvez pas utiliser des noms de personnes célèbres, politiciens et personnages de fiction populaires (exemples : Angelina Jolie, Donald Trump, James Bond).\n- Vous ne pouvez pas utiliser les noms propres de l’univers de Ryzom (exemples : Jena, Tryton, Yrkanis, Mabreka).\n- Vous ne pouvez pas utiliser un nom orthographié à l’inverse d’un nom contrevenant à la charte des noms (exemples : Anej, Notyrt, Sinakry).\n- Vous ne pouvez pas utiliser des noms aisément reconnaissables provenant d’autres univers comme la fantasy médiévale, la science-fiction ou autres, qu'ils soient fictifs ou non et ce même s’ils sont travestis par un léger changement d’orthographe (exemples : Skiwalker, Gandolf, Fluntstones, Obiwon).\n- Vous ne pouvez pas utiliser des noms communs (ou des chaînes de noms communs) ne correspondant pas à l'univers de Ryzom (exemples : smartphone, microonde, camion).\n\n### Particularité des noms de guilde\nPour l’essentiel, les noms de guilde reprennent l’ensemble des règles des noms et sont soumis au même règles que les noms de personnage en terme de renommage, suivant que les éléments qui constituent le nom de la guilde sont très offensants, offensants ou simplement interdits. \nIl y a cependant plusieurs exceptions :\n- Le nom d’une guilde peut utiliser le nom propre de lieux d’Atys afin de faciliter le jeu de rôle (Les Protecteurs de Fairhaven, par exemple).\n- Le nom d’une guilde peut avoir dans son intitulé, à des fins de jeu de rôle, le nom d’un personnage de la Lore si l’ensemble n’enfreint aucun point de la charte des noms et est cohérent avec la Lore. Ainsi « Les enfants d’Elias » et « Les disciples de Mabreka », par exemple, sont conformes à la charte des noms, mais « Le Bocal de Tryton » ne l’est pas.\n\nSi vous trouvez que le nom d'un joueur est inapproprié, dérangeant ou incompatible avec le jeu, vous pouvez le signaler par ticket ou directement en jeu en contactant un CSR (la liste de ceux présents en jeu avec vous s'affiche sous l’onglet SYS.INFOS de la fenêtre de conversation lorsque vous tapez /who gm).\n\nSi vous souhaitez contester une demande de renommage, vous pouvez contacter le chef de l’équipe d'assistance via courriel (support@ryzom.com).\nToute demande sera étudiée très attentivement, tout abus sera enregistré sur le profil du joueur et le cas échéant, sanctionné.\n\n## III. Utilisation abusive du jeu\n\n### 1. Piratage, décryptage ou assimilé des données des serveurs de Ryzom (infraction critique)\n\nModifier le client ou son moyen de communiquer avec le serveur en vue d’en tirer un avantage est strictement interdit et passible d’un bannissement définitif.\n\n\n### 2. Piratage de compte (infraction critique)\n\nPrendre les commandes d’un compte qui ne vous appartient pas, par tout moyen informatique, est strictement interdit et passible d’un bannissement définitif.\n\n\n### 3. Utilisation de bot (infraction majeure)\n\nUtilisation d'un programme tiers pour jouer via des actions automatisées par scripts.\n\nLe fait de rendre votre personnage en partie ou tout à fait autonome par le biais de macros externes au système intégré à Ryzom est totalement interdit. \nCette règle s’applique aussi aux Roues de la Fortune présentes dans les capitales.\n\nLe contrôle de plusieurs personnages par l'utilisation d'un programme tiers est strictement interdit. Si, lors d'une vérification, l'équipe d'assistance ne peut pas discerner si un joueur contrevient à cette règle, le joueur devra fournir la preuve qu'il n'utilise aucun logiciel tiers pour contrôler plusieurs personnages en même temps.\n\n \n### 4. Multiboxing au-delà de 4 personnages (infraction majeure)\n\nLe nombre de comptes connectés simultanément est limité à quatre par adresse IP. Cependant, l'équipe d'assistance étudiera les cas particuliers (famille nombreuse, ordinateur public, etc.). Si vous pensez vous trouver dans un tel cas, veuillez contacter le Support à support@ryzom.com en précisant la raison de votre demande ainsi que les comptes concernés.\n\n\n### 5. Exploit (infraction critique, majeure ou mineure, selon les cas)\n\nLa notion d'\"exploit\" couvre toute utilisation d'un programme tiers, d’une modification, d’une recompilation du client ou d'un bogue dans le but d'obtenir un avantage par rapport aux autres joueurs.\n\nLes exploits sont interdits et punissables par l'équipe d'assistance, qui a le droit de décider de la gravité de l'exploit découvert. À ce sujet, les infractions répétées sont un facteur aggravant.\n\nSi vous soupçonnez l’utilisation d’un exploit, vous devez nous contacter en utilisant l'un des moyens mentionnés ci-dessus.\n\n\n### 6. Pull de mobs hors des batailles d'avant-poste (infraction mineure)\n\nPuller (attirer) un ou plusieurs mobs sur un joueur ou un groupe de joueurs dans l'objectif de le(s) tuer ou de le(s) harceler est interdit. Si un joueur s'aperçoit ou est informé qu'il a involontairement violé cette interdiction, il doit s'excuser et tout faire pour relever, le cas échéant, le(s) personnage(s) tombé(s) du fait de son pull.\n\nSi vous êtes victime d'un pull volontaire et que vous en avez la preuve (capture d’écran d'une conversation en /tell, montrant qu'après avoir été aussitôt contacté par vous, l'auteur du pull n'a montré aucun signe de regret et n'a pas souhaité vous relever), vous pouvez contacter l'équipe d'assistance via un des moyens décrits plus haut. Nous ouvrirons alors un dossier et agirons en conséquence. Là encore les récidives seront prises en compte.\n\nLe pull de mob(s) n'est toléré que s'il est utilisé durant une bataille d’avant-poste (voir III.9. Non respect des règles régissant le PvP)\n\n\n### 7. Vol de mort (infraction mineure)\n\nTuer un mob déjà attaqué par un autre joueur ou groupe de joueurs est considéré comme du vol de mort et est interdit.\n\nSi après tentative d'arrangement, la situation ne s'améliore pas, il vous est possible de contacter  l'équipe d'assistance via les moyens décrits en introduction. Mais avant tout contact avec un CSR, contactez les joueurs en question !\n\nDans le cas d'un Roi ou d'un Nommé, se référer au paragraphe suivant.\n\n### 8. Règles relatives aux Rois et Nommés (infraction mineure)\n\n#### a) Vol de roi ou de nommé\n\nUn Roi ou Nommé appartient à la première équipe l'ayant verrouillé et ce, tant que le verrouillage est actif. (Voir https://app.ryzom.com/app_forum/index.php?page=topic/view/27212 pour plus d'explications.)\nUne deuxième équipe peut reprendre le Roi ou Nommé uniquement si celui-ci n’est pas encore verrouillé ou est déverrouillé. Pour cela, elle peut seulement :\n- combattre la première équipe en mode joueur contre joueur (PvP)\n- Profiter du départ ou de la mort de la première équipe, ce qui déverrouille automatiquement le Roi ou Nommé. À noter que s’il ne reste qu’un seul joueur vivant ou présent de l’équipe qui a verrouillé le Roi ou le Nommé, celui-ci sera déverrouillé.\nLe pull de mobs visant à affaiblir l’autre équipe est interdit et donc sanctionnable (voir point III.6).\n\nUn Roi ou Nommé est considéré comme libre tant qu’il n’est verrouillé par aucune équipe.\n\nLe pull de Roi ou de Nommé par une personne seule, c'est-à-dire sans équipe, est possible mais insuffisant pour provoquer le verrouillage. Reprendre le Roi ou le Nommé n’est alors pas considéré comme un vol de mort.\n\n#### b) Camping de spots de roi ou de nommé\n\nLe camping de spots de Roi et de Nommé n'est pas réglementé car il est trop souvent impossible à prouver.\n\n\n### 9. Non respect des règles régissant le PvP (infraction mineure)\n\nDans Ryzom, le PvP est consensuel. Il doit donc être assumé par celui qui active son tag PvP, qui entre dans une zone de PvP ou qui participe à une bataille d'avant-poste. Autrement dit, si vous ne souhaitez pas participer au PvP, c’est à vous qu’il appartient de prendre les dispositions nécessaires pour l'éviter.\n\n#### a) PvP de faction\n\nLe joueur ayant activé son tag PvP-faction doit l'assumer, et ce quelles que soient les circonstances.\nExemples : être tué pendant qu'on pioche, à l’étable, alors qu'on est loin du clavier (AFK), à un point de résurrection de vortex.\n\nSi malgré un tag PvP activé, vous désirez fuir le combat, vous pouvez attendre que le détag soit effectif, vous cacher, utiliser un pacte de téléportation, changer de lieu ou de région, etc.\n\n#### b) PvP de zone\n\nLe joueur qui choisit de pénétrer dans une zone PvP doit assumer ce choix, et ce, quelles que soient les circonstances. Néanmoins, tuer un personnage taggé PvP-zone au point de résurrection près d'un vortex est interdit et sanctionné si avéré. Si vous êtes tué dans ces conditions il vous est donc possible de nous contacter via un des moyens décrits plus haut. Joindre une capture d’écran à l'appui de votre envoi est alors fortement recommandé (assurez-vous que la fenêtre SYS.INFOS y est visible).\n\n#### c) PvP d’avant-poste\n\nLe joueur qui choisit son camp en pénétrant dans une zone de bataille d’avant-poste doit assumer ce choix, et ce quelles que soient les circonstances.\n\nNéanmoins, tuer un personnage taggé PvP-avant-poste au point de résurrection près d'un vortex est interdit et sanctionné si avéré. Si vous êtes tué dans ces conditions il vous est donc possible de nous contacter via un des moyens décrits plus haut. Joindre une capture d’écran à l'appui de votre envoi est alors fortement recommandé (assurez-vous que la fenêtre SYS.INFOS y est visible).\n\nLes personnages sans tag PVP-avant-poste (ceux entrés Neutres sur la zone de combat) ne sont pas autorisés à rester sur les avant-postes durant la bataille en raison de possibles abus. Les CSR peuvent les déplacer au besoin.\nSi malgré un tag PvP activé, vous désirez fuir le combat, vous pouvez attendre que le détag soit effectif, vous cacher, utiliser un pacte de téléportation, changer de lieu ou de région, etc.\n\n#### Règles relatives aux avant-postes\n\na) En règle générale, les CSR ne sont pas présents aux batailles d'avant-poste. Néanmoins, si vous constatez un manquement à des règles de bienséance et de comportement, vous pouvez immédiatement contacter l'équipe d'assistance.\n\nb) Si votre personnage est bloqué (stuck) dans une région où se déroule une bataille d'avant-poste, vous devez essayer de le téléporter. Si vous n'y parvenez pas, vous devez contacter l'équipe d'assistance en précisant quelle est votre situation. Un CSR viendra alors le débloquer.\n\nc) Tout ce qui relève de la mécanique normale du jeu est permis dans les zones de combat, pourvu que tous les concernés soient taggés PvP.\n- Les mektoubs de bât sont mortels. En les amenant dans une zone de combat, vous les exposez à de grands risques.\n - Camper un point de résurrection est fort souvent inefficace du fait de la zone de sécurité qui l'accompagne, et est donc permis. Si vous mourez et que vos adversaires campent près de votre point de résurrection, vous devrez soit patienter à l’intérieur de la zone de sécurité pour vous régénérer ou choisir un autre point ou pacte pour rejoindre la bataille. \n- Lors des batailles d'avant-poste, le pull de mobs se trouvant dans la région où a lieu le combat fait partie des stratégies de jeu autorisées. \n\n#### d) Blocage d’avant-poste\n\nDéclarer la guerre sur un de ses propres avant-postes ou un avant-poste allié, ou faire en sorte que ses alliés déclarent la guerre sur un de ses propres avant-postes dans le but d'empêcher d'autres de le faire est interdit et considéré comme un exploit. \n\nLa passation d'un avant-poste d'une guilde à une autre est autorisée. L'augmentation du seuil est alors interdite, tout comme il est interdit de procéder à plusieurs passations de suite (ceci constituerait en effet un blocage).\n\n#### e) Fausses déclarations de guerre répétées\n\nAprès avoir déclaré la guerre à plusieurs avant-postes à la fois, le fait de ne participer à aucune des attaques annoncées avec un nombre de joueurs suffisant pour atteindre le seuil face aux seules escouades est considéré comme du harcèlement. Il en va de même en cas de fausses déclarations répétées sur un même avant-poste.\n\nN.B. Tous les points cités ci-dessus visent à vous donner un niveau de flexibilité dans vos choix de tactique lors des attaques, tout en gardant l'équilibre avec le droit qu'ont les propriétaires d'avant-poste de ne pas être harcelés. Si vous pensez que votre guilde peut être accusée de l'une de ces actions interdites, faites une capture d'écran (à transmettre à l'équipe d'assistance si cela s'avérait nécessaire) montrant le nombre de joueurs que vous avez amenés avec vous pour l'attaque.\n\n## IV. Publicité inappropriée\n### 1. Vente de dappers contre monnaie réelle (infraction critique)\n\nLes vendeurs de dappers n'ont rien à faire sur le jeu et son environnement. Ils sont donc strictement interdits. Le joueur coupable sera immédiatement éjecté du serveur et banni.\n\n\n### 2. Publicité pour un produit ou un site web sans lien avec Ryzom (infraction majeure ou mineure selon le type de publicité – l’équipe d'assistance statuera au cas par cas)\n\nLa publicité pour les sites web n'ayant aucun rapport avec Ryzom et son environnement est interdite. Le joueur s'en rendant coupable sera immédiatement rendu muet (mute) et sanctionné à proportion.\n\nL’équipe de Support examinera chaque infraction et décidera de la sanction en fonction de la nature de la publicité publiée.\n\n## V. Fraude générale\n### 1. Se faire passer pour un membre de l’Équipe Ryzom ou de Winch Gate (infraction majeure)\n\nIl est strictement interdit de se faire passer pour un membre des équipes du jeu ou de Winch Gate, que ce soit en jeu, sur les forums de Ryzom, sur IRC, Rocket Chat où ailleurs sur la Toile.\n\n\n### 2. Tentative de duperie envers un membre de l’Équipe Ryzom (infraction majeure si le joueur en retire un avantage)\n\nTenter de duper un membre de l’Équipe Ryzom, soit dans le but de recevoir à nouveau des objets déjà obtenus (récompense d’event, objet de mission, dépouille de Roi, pièce ou plan de Chef Maraudeur, etc.), soit dans un autre but, est strictement interdit. Tout joueur convaincu de cette transgression sera sanctionné conformément au paragraphe **B. Avertissements et sanctions**.\n\n\n### 3. Se faire passer pour un autre joueur (infraction mineure)\n\nIl est interdit de se faire passer pour un autre joueur de quelque manière que ce soit (notamment prétendre être son alt ou reroll et parler en son nom).\n\n\n### 4. Prétendre à une relation privilégiée avec un membre de l’Équipe Ryzom (infraction mineure)\n\nIl est formellement interdit de prétendre avoir une relation privilégiée avec un membre de l’équipe Ryzom (y compris un membre de l'équipe d'assistance), que ce soit pour lui nuire, se faire bien voir de certains joueurs, tenter de les influencer ou pour quelque autre raison.\n\nLes membres des équipes de Ryzom sont soumis à une clause de non divulgation et leur identité ainsi que les noms de leurs personnage-joueurs sont donc préservés. En outre chacun d'entre eux s'est engagé à respecter l'interdiction de privilégier un joueur par rapport à un autre qui lui est faite par un code de conduite interne à l'équipe Ryzom.\n\nNB: Si une telle relation était avérée, le joueur et le CSR (ou membre d’une autre équipe de Ryzom) seraient soumis aux sanctions prévues par leur code de conduite respectif.",
  },
  notFound: {
    link: "Retour à l'ACCUEIL",
    text: "La page que vous tentez d'atteindre n'existe pas.",
    title: 'Contenu non disponible',
  },
  account: {
    forums: 'Forums',
    logout: 'Se déconnecter',
    welcome: 'Salut',
    webig: 'Web-Apps',
    billing: 'Compte / Abonnement',
  },
  card: {
    playFree: {
      systemRequirements: 'Configuration requise',
      premium: {
        before: 'Vous pouvez aussi choisir un',
        link: 'compte premium',
        after: 'pour en profiter encore plus !',
      },
      otherPlatforms: 'Autres plates-formes',
      text: "Inscrivez-vous, téléchargez le jeu et plongez dans l'expérience !",
      title: 'Jouer gratuitement maintenant !',
    },
    returning: {
      button: 'Revenir dans le jeu',
      getGoing: 'Maintenant, allez-y, vos amis vous attendent !',
      bold:
        "Ça fait longtemps qu'on ne s'est pas vu ?  \nC'est bon de vous revoir !",
      text: "Nous avons préparé de l'aide pour vous aider à redémarrer.",
      title: 'De retour sur Ryzom ?',
    },
  },
  events: {
    noEvents:
      "Il n'y a actuellement aucun événement à montrer. Veuillez vérifier plus tard.",
    title: {
      calendar: 'Calendrier des évènements',
      spotlight: "Pleins feux sur l'évènement",
    },
    info: {
      timezone:
        'Les heures affichées sont celles de votre fuseau horaire actuel',
    },
  },
};
