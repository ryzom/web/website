// 2020-05-09 15:00
export default {
  game: {
    atys: {
      text:
        'Atys ist eine magische und einzigartige Welt, die durch das massive Wachstum der Vegetation mit enormen Wurzeln entstanden ist. Beherrscht von intelligenten Pflanzen, harmlosen Pflanzenfressern und gefürchteten Raubtieren; dessen Wunder ebenso beeindruckend wie seine Gefahren sind.',
      title: 'Atys: eine lebendige Welt!',
    },
    mobs: {
      text:
        'Ein leistungsfähiges KI-System ermöglicht ein realistisches Verhalten der Tierwelt: Fleischfresser greifen pflanzenfressende Herden an, realistische und dynamische Tierbewegungen machen sie lebendig, manchmal liebenswert, manchmal bedrohlich. Es gibt auch massive Wanderungen bei Wechsel der Jahreszeiten und unterschiedliches Verhalten bei verschiedenen Tierarten. Nehmt euch Zeit, um das Leben um euch herum zu beobachten, es ist ein unvergesslicher Anblick. Doch hütet euch vor Raubtieren!',
      title: 'Lebendige Herden',
    },
    roleplay: {
      text:
        'Ryzom bietet offizielle interaktive Veranstaltungen an, an denen in Echtzeit reagierende NSCs beteiligt sind. So unterschiedlich sie auch regelmäßig sind, ihr Fortschritt und ihr Ergebnis hängen von Euren Aktionen ab: nichts ist vorherbestimmt!\n\nMöchtet Ihr Eure eigene Veranstaltung gestalten? Das Event-Team ist jederzeit für Euch da und unterstützt und hilft Euch gerne bei der Organisation.\n\nDarüber hinaus sind viele Gilden bereit, ihre Türen zu öffnen, um Eure Charaktere bei der Weiterentwicklung zu unterstützen!\n\nFür Liebhaber von imaginären Sprachen schaffen die Spieler und das Event-Team einzigartige Sprachen. Garantiertes Eintauchen in das Rollenspiel!',
      title: 'Rollenspiel um zu gewinnen! Das wird Euch umhauen.',
    },
    customization: {
      text:
        'Das Aussehen des Charakters kann bei der Erstellung vollständig angepasst werden: Gestalte deinen Charakter so, wie du es gerne möchtest!',
      title: 'Volle Anpassung des Charakters',
    },
    classless: {
      text:
        'Charaktere in Ryzom sind unüblich, da diese nicht an eine bestimmte Klasse gebunden sind. Das bedeutet, dass man nicht wählen muss, ob man ein Krieger, ein Magier, ein Ernter oder ein Handwerker sein will. Ihr könnt eure Fähigkeiten in all diesen Klassen mit nur einem Charakter entwickeln; ihr müsst euch nicht auf einen Weg konzentrieren. Ihr könnt zum Beispiel nach Belieben entweder die Beherrschung eurer Pike trainieren, eure magischen Verstärker zum Heilen oder für offensive Zaubersprüche einsetzen, eure Hacke in die Hand nehmen und Rohstoffe ernten oder sich mit der Herstellung einer neuen Rüstung oder Waffe beschäftigen.\n\nIhr habt die absolute Freiheit!',
      title: 'Klassenlos',
    },
    multiplatform: {
      text:
        'Ryzom kann kostenlos gespielt und für Windows, Linux und Mac heruntergeladen werden.',
      title: 'Windows, Linux & Mac',
    },
    forage: {
      text:
        'Ryzom verfügt über ein fortschrittliches Ernte- und Handwerk-System.\n\nERNTEN - Es ermöglicht Euch, je nach Jahreszeit und Wetter und Euren Fertigkeiten, Materialien aus dem Boden zu gewinnen. Wenn Raubtiere erst einmal dabei sind, ist es nicht so einfach!\n\nHandwerk - Eine sorgfältige Kombination von Materialien, abhängig von ihren Eigenschaften und Farben, ermöglicht die Fertigung von einzigartigen Waffen und Rüstungen. Tausende von Rezepturen sind möglich.',
      title: 'Fortschrittliches Ernten & Plündern',
    },
    pvp: {
      text:
        'Man kann überall kämpfen, aber nur, wenn man es wünscht. Duelle, Außenpostenkämpfe, Fraktions- oder Regionalkämpfe... Es steht Euch frei, ob Ihr Euch an diesen Kämpfen beteiligen wollt... oder Euch enthalten wollt!',
      title: 'Einvernehmliches PvP',
    },
    community: {
      text:
        'Ryzom ist in Deutsch, Englisch, Spanisch, Französisch und Russisch verfügbar und verfügt über eine tolle mehrsprachige Community, die ausgereift, hilfsbereit und freundlich ist. Jeder kann sich in seiner eigenen Sprache unterhalten und Rollenspiele veranstalten, daher ist es nicht zwingend notwendig, eine Fremdsprache zu sprechen.\n\nKommen sie dazu und werden sie ein Teil der Familie!',
      title: 'Eine wirkliche Gemeinschaft',
    },
    title:
      'Betretet ein einzigartiges MMORPG aus Elementen der Wissenschaft und Fantasie. Lasst euch in eine einzigartige organische Lebenswelt entführen!',
    invasions: {
      text:
        'Stellt euch vor, ihr seht vor einem Stadttor auf einen weit entfernten Hügel und seht die Späher eines Kitin-Schwarms. Stellt euch nun vor, dass ihnen Hunderte von Kitins folgen, die sich wie eine vereinte und in Scharen kämpfende Armee bewegen.\n\nRiesige Schlachten erfordern die Zusammenarbeit eines großen Teils der Gemeinschaft, um diesen bedrohlichen Invasionen entgegenzutreten. Und was wäre, wenn die Kitin eines Tages erneut siegen und die Hominheit zum dritten Mal bedrohen würden?',
      title: 'Invasionen',
    },
    environment: {
      text:
        'Ryzom ist eine fortwährende 3D-Welt, die aus fünf prächtigen und unterschiedlichen Ökosystemen besteht. Die Landschaft, das Verhalten der Tierwelt und die Verfügbarkeit von Bodenmaterial ändern sich je nach Jahreszeit, Wetter und Zeit.\n\nEntdecken Sie die Schönheit des Waldes im Frühling, führen Sie Krieg im Dschungel unter einem stürmischen, von Blitzen durchzogenen Sommerhimmel, vergessen Sie sich selbst in den Dünen der Wüste im Herbst, oder entspannen Sie sich, indem Sie die Spiegelungen der Sonne im Winter auf dem glasklaren Wasser des Seenlandes genießen.',
      title: 'Dynamische Umgebungen',
    },
    freedom: {
      text:
        'In Ryzom gibt es keine Klassen zur Auswahl. Entwickelt alle Fertigkeiten eures Charakters harmonisch weiter oder konzentriert euch auf eine Fertigkeit und werdet zum Meister. Ihr seid nicht gezwungen, eine Fraktion zu wählen, wenn ihr euren Charakter erstellt. Tretet während eurer Reisen einer Fraktion bei, oder auch nicht.\n\nIhr müsst im PvP nicht kämpfen, aber Ihr könnt es. Tretet einer Gilde bei, wann immer Ihr bereit seid, und schließt Euch mit ihr zusammen, oder beginnt Eure Abenteuer allein oder mit Euren Freunden.\n\nSucht nach den wertvollsten Materialien auf Atys, um Eure eigene, einzigartigen Ausrüstungen herzustellen, mit der Ihr Euch leichter gegen Eure Feinde verteidigen könnt. Jagt die stärksten Bosse auf Atys. Erobert einen Außenposten und bohrt Material für eure Gilde.\n\nMacht euer eigenes Ding oder nehmt Einfluss auf die Politik. Entwickelt euren Charakter so, wie ihr es wollt, und beginnt eure eigene Geschichte.\n\nEs liegt an Euch, zu entscheiden, was Ihr tun wollt. Fühlt euch frei.',
      title: 'Freiheit: Es gibt nichts Wertvolleres!',
    },
    opensource: {
      text:
        'Ryzom ist eines von nur wenigen kommerziellen MMORPGs, die vollständig quelloffen sind: Client, Server, Werkzeuge und Medien. Quelloffen bedeutet, dass Ryzom auf eine lebendige Gemeinschaft angewiesen ist, die Korrekturen und Funktionen anbietet, die über das hinausgehen, was das Team anbieten kann. Es bietet den Spielern eine einzigartige Gelegenheit, sich an der Entwicklung des Spiels zu beteiligen.\n\nFür weitere Informationen über Ryzom Core besucht den [Ryzom Core Blog](https://ryzomcore.atlassian.net/wiki/spaces/RC/overview). Um den grafischen Bereich zu durchsuchen, besucht bitte das [Ryzom Media Repository](https://github.com/ryzom/ryzomcore-assets).',
      title: 'Quelloffen',
    },
  },
  website: {
    meta: {
      description:
        'Ein auf Freiheit ausgerichtetes klassenloses MMORPG in einer Umgebung aus wissenschaftlichen und phantastischen Elementen mit Schwerpunkt auf Rollenspiel, Echtzeit-Veranstaltungen, komplexem Handwerk und dynamischen Umgebungen.',
    },
    title:
      'Ryzom - kostenloses (F2P) und quelloffenes MMORPG | Windows, Mac & Linux',
  },
  footer: {
    owner: 'Winch Gate Property Limited',
    copyright: 'Copyright © {year}. Alle Rechte vorbehalten.',
    pegi: {
      online: 'Online-Spiel',
      violence: 'Gewalt',
    },
    privacy: 'Datenschutzbestimmungen',
    tos: 'Nutzungsbedingungen',
    community: {
      bmsite: 'Ballistic Mystix',
      armory: 'Ryzom Armory',
      forge: 'Ryzom Forge (das Gemeinschaftsprojekt)',
      wiki: 'Ryzom-Wiki',
      title: 'Gemeinschaft',
    },
    follow: {
      youtube: 'Youtube',
      twitter: 'Twitter',
      vimeo: 'Vimeo',
      flickr: 'Flickr',
      facebook: 'Facebook',
      title: 'Folge Ryzom',
    },
    support: {
      feedback: 'Feedback',
      forums: 'Foren',
      contact: 'Kontakt',
      title: 'Support',
    },
  },
  index: {
    calendar: {
      title: 'Events demnächst',
      viewMore: 'alle Events anzeigen',
    },
    news: {
      noNews: 'Keine News gefunden. Versuche es später noch einmal.',
      title: 'letzte neuigkeiten & updates',
      viewAll: 'alle Neuigkeiten anzeigen',
    },
    features: {
      atys: 'Atys: eine lebendige Welt!',
      roleplay: 'Rollenspiel um zu gewinnen! Das wird Euch den Atem rauben.',
      freedom: 'Freiheit: Nichts ist wertvoller!',
      community: 'Eine echte Gemeinschaft',
      title: 'entdecke Ryzom',
      viewAll: 'alle Features anzeigen',
    },
    forge: {
      playersCreate: 'gestalte mit',
      openSource: 'Open-Source',
      supportRyzom: 'unterstütze Ryzom',
    },
    wiki: {
      visit: 'besuche Ryzom Wiki',
      title: 'entdecke',
    },
  },
  privacy: {
    preamble: {
      title: 'Privatsphäre und Datenschutz',
      content:
        'Winch Gate Property Limited (im Folgenden als "Winch Gate" bezeichnet), mit Sitz in 3030 LIMASSOL (Zypern), betreibt das Spiel "Ryzom" und die Website "[www.ryzom.com](https://www.ryzom.com)". Winch Gate hält sich an die allgemeine Datenschutzverordnung (GDPR, gültig ab 25. Mai 2018), respektiert die Privatsphäre seiner Online-Besucher und anerkennt die Notwendigkeit, eine sichere Umgebung für die gesammelten Informationen zu gewährleisten.\n\nWinch Gate verpflichtet sich, seinen Online-Besuchern ein Dokument mit allen Informationen darüber zur Verfügung zu stellen, welche und wie persönliche Daten gesammelt, verarbeitet und genutzt werden (" Datenschutzerklärung).\n\nMit der Nutzung dieser Website akzeptieren Sie die Datenschutzrichtlinie.\n\nBitte beachten Sie, dass die Winch Gate Datenschutzerklärung nicht gilt, wenn Sie über die Weblinks auf den Winch Gates Internetseiten auf andere Seiten zugreifen, da wir keine Kontrolle über die Aktivitäten dieser anderen Seiten haben. Darüber hinaus kann diese Datenschutzerklärung von Zeit zu Zeit variieren; bitte konsultieren Sie dieses Dokument in regelmäßigen Abständen, damit Sie über alle Änderungen informiert sind.',
    },
    policy: {
      title: 'Datenschutzerklärung',
      content:
        'Winch Gate sammelt nur persönliche Daten, die für die Erstellung und Pflege von Konten und Charakteren innerhalb von Ryzom absolut notwendig sind. Winch Gate wird niemals persönliche Daten von seinen Online-Besuchern und/oder Spielern an Dritte weitergeben.\n\n## Was sind persönliche Daten?\nPersönliche Daten sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person beziehen. Dazu gehören IP-Adressen und Cookie-Daten.\n\n## Rechte der Benutzer\nDer Benutzer muss darüber informiert werden, ob Winch Gate Daten sammelt, warum die Daten gesammelt werden, wie sie verarbeitet und genutzt werden. Der Benutzer muss seine Einwilligung geben (mehr dazu unter "Einwilligung"). Darüber hinaus hat der Nutzer das Recht, auf Anfrage Auskunft über seine gespeicherten Daten zu erhalten. Dieser Service ist kostenlos und kann jederzeit von jedem Nutzer angefordert werden (mehr dazu unter "Zugriffsrecht auf Daten"). Im Falle eines Lecks wird der Benutzer innerhalb von 72 Stunden benachrichtigt, ebenso wie das zuständige Büro für den Datenschutz (mehr dazu unter "Benachrichtigung bei Verstößen"). Der Benutzer hat auch das Recht, vergessen zu werden, was bedeutet, dass Winch Gate auf Verlangen des Benutzers alle persönlichen Daten löschen muss (mehr dazu unter "Recht auf Vergessen").\n\n## Einverständnis\nEs muss ein Antrag zur Zustimmung gestellt werden, in einer verständlichen und einfachen Sprache. Er muss in einer leicht zugänglichen Form erfolgen und der Zweck der Datenverarbeitung muss beigefügt werden. Die Einwilligung muss sich von anderen Dienstleistungen unterscheiden, sie muss freiwillig gegeben werden und sie muss so leicht zu widerrufen sein, wie sie gegeben wurde.\n\n## Welche Daten sammeln wir?\nWinch Gate sammelt persönliche Daten hauptsächlich für das Bezahlungssystem für das Spiel "Ryzom". Es ist notwendig, damit der Benutzer die gesamte Erfahrung eines so genannten "Premium-Kontos" erhalten kann. Dieser Dienst ist freiwillig. Darüber hinaus sammelt Winch Gate auch von Zeit zu Zeit Daten für spielrelevante Umfragen/Befragungen und Statistiken. Die Umfragen sind freiwillig und meist anonym. Der Benutzer wird jedoch vor Beginn der Umfrage über den Prozess der Datenerhebung informiert, und es muss eine klare Zustimmung gegeben werden. Die Statistiken für "Ryzom" werden in der EULA deklariert und müssen vom Benutzer akzeptiert werden. Winch Gate wird den Benutzer darüber informieren, wenn es persönliche Daten für Statistiken über andere Dienste ("[www.ryzom.com](https://www.ryzom.com)") sammelt und um Zustimmung bitten, im Rahmen des entsprechenden Dienstes.\n\n## Warum sammeln wir Daten?\nDurch diese Datensammlungen ist Winch Gate in der Lage, zukünftige Strategien zu entwickeln, die Benutzererfahrung zu verbessern, das Angebot zu erweitern und eine zielgerichtete Gruppe anzusprechen. Insgesamt wird sie dazu verwendet, die Erfahrung für die Kunden und Benutzer von Winch Gates zu optimieren. Zusätzlich sind einige Daten für den Zahlungsvorgang notwendig.\n\n## Zugriffsrecht auf Daten\nDie Benutzer haben das Recht, Zugang zu ihren gespeicherten Daten zu erhalten. Sie können jederzeit eine Anfrage an [https://support.ryzom.com](https://support.ryzom.com) senden. Winch Gate wird Ihnen so schnell wie möglich Auskunft über alle gespeicherten persönlichen Daten geben. Dieser Dienst ist 100% kostenlos und hat keine Nutzungsbeschränkung. Bitte beachten Sie, dass Winch Gate das Recht hat, mehrere Anfragen, die sich auf die gleiche natürliche Person (oder das gleiche Konto) beziehen, innerhalb einer sehr kurzen Zeitspanne abzulehnen.\n\n## Das Recht, vergessen zu werden\nDer Benutzer hat das Recht, vergessen zu werden, das heißt, dass Winch Gate auf Wunsch des Benutzers alle gespeicherten persönlichen Daten löschen muss. Bitte beachten Sie, dass Winch Gate das Recht (und die Pflicht) hat, im Falle von kriminellen Aktivitäten Einspruch zu erheben, bis die zuständigen Behörden involviert sind und ihre Zustimmung zur Löschung der persönlichen Daten geben. Darüber hinaus können einige Winch Gate Produkte, falls Sie von Ihrem Recht Gebrauch machen, vergessen zu werden, aufgrund des Verlustes der notwendigen Daten nicht mehr zur Verfügung stehen (z.B. "Premium-Konten"). Winch Gate wird den Benutzer über die Folgen informieren, bevor es alle persönlichen Daten dauerhaft löscht, und wird die Zustimmung der Benutzer einholen.\n\n## Benachrichtigung bei Verletzung\nIm Falle eines Lecks (Hack) sendet Winch Gate innerhalb von 72 Stunden, nachdem es von der Verletzung Kenntnis erlangt hat, eine Benachrichtigung über die Verletzung an jeden beteiligten Benutzer und an das zuständige Datenschutzbüro. Diese Benachrichtigung enthält Informationen darüber, wann die Verletzung bemerkt wurde, welche natürliche Person und welche Daten betroffen sind, sowie welche Maßnahmen möglicherweise bereits ergriffen wurden.',
    },
  },
  login: {
    password: 'Passwort',
    error: {
      server: 'Serverfehler',
      credentials: 'Zugangsdaten ungültig',
    },
    username: 'Account-Name',
  },
  menu: {
    forums: 'foren',
    gameInfo: 'spiel',
    universe: 'universum',
    wiki: 'wiki',
    events: 'events',
    home: 'start',
  },
  button: {
    downloadRyzom: 'Ryzom herunterladen',
    continueSaga: 'Setze die Saga fort',
    login: 'login',
    signUp: 'Account erstellen',
  },
  download: {
    systemRequirements: {
      os: {
        title: 'Betriebssystem',
      },
      linux: {
        sound: 'OpenAL kompatibel',
        cpu: '1 GHz Einkernprozessor',
        hdd: '10 GB verfügbarer Speicherplatz',
        graphics: 'OpenGL 1.2 mit 32 MB VRAM',
        title: 'Linux',
        ram: '2 GB RAM',
      },
      sound: {
        title: 'Soundkarte',
      },
      cpu: {
        title: 'Prozessor',
      },
      hdd: {
        title: 'Speicherplatz',
      },
      graphics: {
        title: 'Grafik',
      },
      title: 'Systemanforderungen',
      windows: {
        os: 'Windows Vista',
        sound: 'DirectX kompatibel',
        cpu: '1 GHz Einkernprozessor',
        hdd: '10 GB verfügbarer Speicherplatz',
        graphics: 'OpenGL 1.2 oder DirectX 9.0 mit 32 MB VRAM',
        title: 'Windows',
        directx: 'Version 9.0c',
        ram: '2 GB RAM',
      },
      minimum: 'minimum',
      directx: {
        title: 'DirectX',
      },
      mac: {
        os: 'OS X 10.8',
        sound: 'OpenAL kompatibel',
        cpu: '1 GHz Einkernprozessor',
        hdd: '10 GB verfügbarer Speicherplatz',
        graphics: 'OpenGL 1.2 mit 32 MB VRAM',
        title: 'Mac OS',
        ram: '2 GB RAM',
      },
      ram: {
        title: 'Arbeitsspeicher',
      },
    },
    title: 'Ryzom herunterladen',
  },
  premium: {
    button: {
      manageSub: 'Abonnement verwalten',
    },
    sub: {
      button: 'Abonnement abschließen',
      title: 'Schon am  \nSpielen?',
    },
    compare: {
      title: 'Vergleiche und entscheide selbst!',
    },
    comparison: {
      premium: {
        maxlevel: 'Maximaler Level 250',
        xp: 'doppelte Erfahrung',
        maxquality: 'Maximale Stufe der Ausrüstung 250',
        storage: '* 1 Reittier + 3 Packtiere\n* 1 Apartment\n* 1 Gildenhalle',
        title: 'premium',
        support:
          '* Unterstützung durch das Support-Team.\n* Wiederherstellung alter Konten (die vor der Zusammenführung der\nSpielserver existierten), um einen oder mehrere Charaktere wiederherzustellen.\n* Wiederherstellung von einen oder mehreren Charakteren im Falle einer absichtlichen oder versehentlichen Löschung\n* Übertragung von einen oder mehreren Charakteren zwischen zwei Premium-Konten, die derselben Person gehören\n* Ein Wechsel des Pseudonyms ist alle sechs Monate für jeden Charakter erlaubt',
      },
      free: {
        maxlevel: 'Maximaler Level 125',
        xp: 'normale Erfahrung',
        maxquality: 'Maximale Stufe der Ausrüstung 150',
        storage: '* 1 Reittier',
        title: 'gratis',
        support:
          '* Unterstützung durch das Support-Team.\n* Eine Änderung des Pseudonyms ist für jeden Charakter erlaubt',
      },
    },
    intro: {
      text:
        'Ryzom hat das Spiel für die freie Welt geöffnet, seinen Quellcode veröffentlicht und ist das einzige quelloffene MMORPG geworden, das unter Windows, Linux und Mac gespielt werden kann.\n\nDie Pflege von Ryzom ist jedoch mit Kosten verbunden, wie zum Beispiel die Vergütung für das kleine Team von Mitarbeitern und die Bezahlung der Server. Deshalb, obwohl Ryzom kostenlos spielbar (F2P) ist, sind Abonnements unerlässlich, damit das Spiel weitergeführt werden kann.\n\nUm sich bei den Spielern zu bedanken, die sich für ein Abonnement entschieden haben, werden bestimmte Funktionen des Spiels freigeschaltet und gewisse Vorteile angeboten: die Möglichkeit, Charaktere auf Stufe 250 statt 125 zu erhöhen, die Verdoppelung aller gewonnenen Erfahrungspunkte und der Zugang zu verschiedenen anderen Aufbewahrungsmöglichkeiten für Gegenstände.\n\n**Um Ryzom zu helfen, entscheidet euch für ein Abonnement!**',
      title: 'Warum bezahlen?',
    },
    rates: {
      perMonth: 'Monat',
      m1: {
        title: '1-Monat Abonnement',
      },
      m3: {
        title: '3-Monate Abonnement',
      },
      y1: {
        title: '1-Jahr Abonnement',
      },
      m6: {
        title: '6-Monate Abonnement',
      },
      title: 'Preise',
    },
    donate: {
      button: 'spenden',
      title: 'Hilf uns die Saga  \nfortzuführen!',
    },
    title: 'Unterstütze Ryzom und profitiere!',
    signup: {
      button: 'Anmelden und loslegen',
      title: 'Interesse an  \nRyzom?',
    },
  },
  returning: {
    download: {
      text:
        'Ihr möchtet wieder Spielen? Ladet Ryzom GRATIS herunter und stürzt euch hinein in die Abenteuer auf Atys!',
      title: 'Spiel  \nHerunterladen',
      modal: {
        onSteam: 'Ryzom auf Steam',
        forMac: 'für Mac',
        forWin: 'für Windows',
        forLinux: 'für Linux',
      },
    },
    recovery: {
      text: 'Account-Name oder Passwort vergessen? Wir helfen euch!',
      title: 'Account  \nWiederherstellung',
    },
    resub: {
      link: 'weiterlesen',
      text:
        'Ihr benötigt zwar kein aktives Abonnement zum Einloggen oder zum Chatten mit eurer Gilde, aber ihr braucht ein Abonnement, wenn ihr einen Charakter über Stufe 125 spielen möchtet.',
      title: 'Abonnement  \nErneuern',
    },
    login: {
      title: 'Account  \nlogin',
    },
    title:
      'Wir freuen uns sehr, dass ihr wieder zurück seid! Folgt diesen drei Schritten, um euch euren Freunden im Spiel wieder anzuschließen.',
    headline: 'So gelangt ihr ins Spiel',
    register: {
      text:
        'Klick hier um einen neuen Spieleraccount anzumelden. Es dauert nur wenige Minuten und du kannst loslegen!',
      title: 'Account  \nerstellen',
    },
  },
  universe: {
    inhabitants: {
      fauna: {
        text:
          'Ihr werdet auf verschiedene Tierarten innerhalb der unterschiedlichen atysianischen Ökosysteme treffen.\n\nWenn Ihr Euch die Zeit nehmt, sie zu beobachten, werdet Ihr eine lebendige und realistische Fauna entdecken, mit friedlichen Herden von Pflanzenfressern, die mit den Jahreszeiten wandern, Raubtiere, die regelmäßig vereinzelt Angriffe wagen und ihre Beute verteidigen. Ihr könnt selbst als Raubtier am Geschehen teilnehmen... oder als Beute! ',
        title: 'Fauna',
      },
      races: {
        fyros: {
          text:
            'Fyros behaupten, das kriegerische Volk von Atys zu sein. Begierig auf Abenteuer, Kampf und die Jagd gegenüber alle, die sie für würdig halten, sich zu wehren. Sie sind auch wissensdurstig und begierig darauf, die Geheimnisse der Vergangenheit von Atys aufzudecken. Ihr Verhältnis zur Religion war immer von ihrem Pragmatismus geprägt.\n\nDie meisten Fyros bewohnen die Wüste, in der sie ihr Imperium gegründet haben. Sie schätzen Wahrheit, Ehre, Disziplin und Gerechtigkeit. Die Erfüllung des Ritus ihrer Staatsbürgerschaft erlaubt es jedem, ein Fyros-Patriot zu werden und von verschiedenen Vorzügen und Belohnungen zu profitieren.\n\nDie Sprache ihrer Ahnen ist Fyrk.',
          title: 'Fyros',
        },
        matis: {
          text:
            'Die Matis sind ein anspruchsvolles, kultiviertes und ehrgeiziges Volk. Sie beherrschen die Botanik und bauen ihre Häuser und Werkzeuge aus lebenden Pflanzen. Sie sind Künstler und Eroberer zugleich und glauben, dass jeder Homin seinen Platz in der Gesellschaft verdienen muss. Sie sind die treuesten Anhänger der Karavan.\n\nDie meisten der Matis leben im Wald, in dem sie ihr Königreich, eine Monarchie göttlichen Rechts, errichtet haben. Sie schätzen die Loyalität zum König und zu Jena, ästhetische Errungenschaften und Wettbewerb. Der Ritus der Staatsbürgerschaft erlaubt es jedem, ein Vasall der Matis zu werden und von verschiedenen Vorzügen und Belohnungen zu profitieren.\n\nIhre Sprache ist Mateis.',
          title: 'Matis',
        },
        text:
          'Atys wird von vielen fühlenden Wesen bewohnt. Dort leben die Kamis, geheimnisvolle Gestaltwandler, die sich dem Schutz der Natur verschrieben haben; die Apostel der Karavan, gepanzerte Humanoide, die eine unbekannte Technologie kontrollieren; und schließlich die Homins, die bis auf eine Handvoll primitiver Stämme in vier Nationen verteilt sind: Fyros, Matis, Tryker und Zoraï.',
        title: 'Nationen',
        tryker: {
          text:
            'Die Tryker sind von der Größe her das kleinste Volk, sie sind friedlich, der Freiheit verpflichtet und ein Lebenskünstler. Sie träumen von einer friedlichen Welt ohne Herren und Sklaven. Ihre Neugier macht sie zu hervorragenden Entdeckern und Erfindern, und sie beherrschen die Technologie der Windmühlen vollkommen. Ihre persönliche Interpretation der Religion weckt oft den Zorn von Dogmatikern.\n\nDie meisten der Tryker nennen die Seenländer ihre Heimat, wo sie ihre Föderation mit einem Gouverneur an der Spitze gegründet haben. Sie schätzen Freiheit, Gleichheit und das Teilen. Durch den Ritus ihrer Staatsbürgerschaft kann man ein Tryker-Bürger werden und verschiedene Vorzüge und Belohnungen genießen.\n\nSie haben ihre eigene Sprache, das Tryker-Tyll.',
          title: 'Tryker',
        },
        zorai: {
          text:
            'Von Natur aus ruhig und spirituell sind die Zoraïs die größten aller Homins. Die Maske bedeckt ihr Gesicht, ein heiliges Geschenk der Kami, denen sie inbrünstig dienen. Ihre heilige Pflicht ist es, für das Gleichgewicht der Natur zu sorgen und gegen das Goo, die Plage des Dschungels, zu kämpfen. Immer auf der Suche nach Wissen, haben sie sich den Magnetismus zu eigen gemacht.\n\nDie meisten Zoraï bleiben im Dschungel, wo sie ihre Theokratie unter der Führung des Großen Weisen errichtet haben. Sie schätzen spirituelle Errungenschaften, Weisheit und Respekt für die Natur. Der Ritus der Staatsbürgerschaft erlaubt es einem, ein Eingeweihter der Zoraï zu werden und von verschiedenen Vorzügen und Belohnungen zu profitieren.\n\nSie sprechen das Taki Zoraï.',
          title: 'Zoraï',
        },
      },
      flora: {
        text:
          'Atys beherbergt eine reiche Flora, darunter mehrere Arten intelligenter Pflanzen.\n\nDiese Pflanzen werden euch mit nützlichen Materialien für euer Handwerk versorgen, aber ihr müsst lernen, wie ihr ihren gewaltigen Angriffen, einschließlich der magischen, aus dem Weg gehen könnt!',
        title: 'Flora',
      },
      floraFauna: {
        text:
          'Atys ist mit vielen Tier- und Pflanzenarten besiedelt. Ihre Jagd ermöglicht es den Homins, die für die Herstellung von Handwerksprodukten erforderlichen Rohstoffe zu sammeln: Häute, Zähne, Knochen, Hörner, Sehnen, Hufe, Augen, Nägel... Alles ist verwendbar, um Kleidung, Waffen und Juwelen herzustellen...\n\nUm diese Ressourcen nutzen zu können, müsst ihr, allein oder von euren Ältesten, verschiedene Jagdtechniken erlernen, da jede Tier- oder Pflanzenart im Kampf ein eigenes Verhalten aufweist.',
        title: 'Fauna und Flora',
      },
      kitins: {
        text:
          'Die gefährlichsten Tiere für die Homins sind zweifelsohne die Kitin. Organisiert, mächtig und unbarmherzig haben diese riesigen Insekten mit einer ausgeklügelten sozialen Organisation zweimal Atys zugrunde gerichtet, indem sie wahllos Homins niedergemetzelten und ganze Zivilisationen ausgelöscht haben.\n\nSeitdem baut sich die Hominheit schrittweise wieder auf, aber die Gefahr lauert fortwährend...',
        title: 'Kitin',
      },
      title: 'Die Bewohner',
    },
    planet: {
      text:
        'Die Erkundung dieser Welt führt euch durch die vielfältigen Ökosysteme, von der brennenden Wüste bis zu den herrlichen Seen, von den majestätischen Wäldern bis zum üppigen Dschungel, der von einer tödlichen Seuche verwüstet wurde, und sogar in die dunklen Tiefen des Planeten, die von Flüssen aus Sap durchzogen sind.\n\nMacht euch auf, Atys zu erobern oder kommt, um dessen Geheimnisse und Legenden zu ergründen! Aber Vorsicht, denn in diesen idyllischen Landschaften lauern überall Gefahren.',
      title: 'Der Planet',
    },
    continents: {
      forest: {
        text:
          'Der Wald ist die Heimat der Matis, die dort ihr Königreich errichtet und ihre Hauptstadt Yrkanis in Form von lebenden Bäumen nachgebildet haben.\n\nDieser Kontinent, bekannt als die grünen Anhöhen, ist mit beeindruckenden und majestätischen Bäumen bedeckt und ist geprägt von einem milden Klima, das von vier ausgeprägten Jahreszeiten unterbrochen wird.',
        title: 'Wald',
      },
      jungle: {
        text:
          'Die Zoraïs haben ihre Städte im Pflanzenlabyrinth des Dschungels um ihre Hauptstadt Zora herum gebaut.\n\nDieser Kontinent wird auch als die "Verdorrende Lande" bezeichnet, da dieser vom Gestank des Goo\'s infiziert ist, einem Krebsgeschwür, das das Leben unaufhaltsam vernichtet, den Lebensraum verwüstet und Flora und Fauna zu abscheulichen Kreaturen mutieren lässt.',
        title: 'Dschungel',
      },
      lakes: {
        text:
          'Der Archipel der Seen umfasst eine Vielzahl von Inseln, die von den Trykern bewohnt werden. Hier haben sie ihre Föderation gegründet und schwimmende Städte gebaut, deren Hauptstadt Fairhaven ist.\n\nDieser Kontinent, bekannt als Aeden Aqueous, bietet eine idyllische Landschaft mit seinen weißen Stränden aus Sägemehl, seinen prächtigen Wasserfällen und seinem kristallklaren Wasser, so weit das Auge reicht. Das Klima ist das ganze Jahr über sonnig.',
        title: 'Seenland',
      },
      roots: {
        text:
          'Die Urwurzeln sind das geheimnisvollste Ökosystem von Atys. In die Tiefe des Planeten taucht der riesige unterirdische Kontinent, dessen Boden, Wände und Decke von leuchtenden Pflanzen und seltsamen Kreaturen bevölkert sind.\n\nSo schön wie gefährlich, verbergen sich in den Urwurzeln die rätselhaften Überreste der Vergangenheit und beherbergen beängstigend viele Nester der Kitin. Das Klima ist ständig frisch und feucht.',
        title: 'Urwurzeln',
      },
      desert: {
        text:
          'Dieser Kontinent, bekannt als die Brennende Wüste, ist die Heimat der Fyros, die ihr Imperium in der Hauptstadt Pyr errichtet haben.\n\nStarke, heftige Winde, lange Dürreperioden, erstickende Hitze am Tag und eisige Temperaturen in der Nacht machen diese glühenden Dünen zu einem der lebensfeindlichsten Orte von Atys.',
        title: 'Wüste',
      },
      title: 'Die Kontinente',
    },
    factions: {
      kami: {
        text:
          'Umfasst alle Jünger der Kamis, der Schutzkörper, die ständig mit dem obersten Kami, Ma-Duk, verbunden sind.\n\nJeder Homin, der bereit ist, in die Kami-Fraktion einzutreten, muss einen Treueschwur leisten und schwören, die Gesetze von Ma-Duk zu respektieren.\n\nDie Kami bieten ihren Jüngern verschiedene Dienste an, wobei die beiden wichtigsten die Auferstehung und die Teleportation in alle Regionen von Atys sind. Sie bieten auch einzigartige Belohnungen für diejenigen, die in ihrem Namen gegen die Karawanen-Anhänger kämpfen.\n\nDie Kamis treten den Karavan regelmäßig während der Kämpfe auf den Außenposten gegenüber, mit dem Ziel, die kostbaren Ressourcen der Karavan auszubeuten, indem sie ihre eindrucksvollen und ökologischen Baumbohrer verwenden, die von den Kami zur Verfügung gestellt werden.',
        title: 'Kami',
      },
      ranger: {
        text:
          'Dazu gehören alle Homins, die den Wunsch haben, die Hominheit von der Kitin-Bedrohung zu befreien.\n\nDie Gilde der Ranger in Atys, die nach dem ersten großen Schwarm gegründet wurde, wird von einem Ideal der Brüderlichkeit angetrieben. Die Ranger glauben, dass die Homins in Frieden und ohne Spaltung leben sollten; deshalb verbieten sie sich, an den Streitigkeiten zwischen den Homins teilzunehmen, und weigern sich, die Hand gegen sie zu erheben, außer im Falle der Verteidigung.\n\nZusätzlich zu den endlosen Patrouillen auf Atys halten die Ranger auch strategische Stützpunkte auf der Insel Silan und im Almati-Wald. Das Silan-Lager, das von Chiang verwaltet wird, nimmt Flüchtlinge auf, die in die Neuen Lande kommen. Das Lager im Almati-Wald, das vom Pfadfinder Orphie Dradius geleitet wird, dient als logistisches Zentrum und überwacht das in der Nähe liegende Kitin-Nest.\n\nWährend des zweiten großen Schwarms halfen die Ranger bei der Rettung der Hominheit. Ihre Kriegspläne ermöglichten eine sichere Rettung der Bevölkerung, indem sie die Kamis, die Karavan, die Nationen und die Trytonisten zur Zusammenarbeit überredeten.\n\nWährend sie alles tun, um einen dritten Großen Schwarm zu vermeiden, beobachten die Ranger die Kitins genau, eliminieren ihre überaus neugierigen Späher und entwickeln neue Kampftechniken und Taktiken nach eigenem Ermessen.',
        title: 'Ranger',
      },
      neutral: {
        text:
          'Einige Homins haben sich entschieden, sich nicht mit einer der beiden Mächte zu verbünden, sie aber nicht zu bekämpfen: Sie sind in religiöser Hinsicht neutral. \n\nObwohl sie offiziell keine Fraktion bilden, wird die religiöse Neutralität, die sie weit weg von Ma-Duk und Jena zusammenführt, oft als solche gesehen.',
        title: 'Neutral',
      },
      karavan: {
        text:
          'Dazu gehören alle Anhänger der Karavan, die hominoiden Jünger der Göttin Jena.\n\nJeder Homin, der bereit ist, in die Karavan-Fraktion einzutreten, muss einen Treueschwur leisten und schwören, die Gesetze von Jena zu respektieren.\n\nDie Karavan bieten ihren Anhängern verschiedene Dienste an, wobei die beiden wichtigsten die Auferstehung und die Teleportation in allen Regionen von Atys sind. Sie bieten auch einzigartige Belohnungen für diejenigen, die in ihrem Namen gegen die Kami-Jünger kämpfen.\n\nDie Karavan treten den Kamis regelmäßig bei Kämpfen auf den Außenposten gegenüber, um ihre wertvollen Ressourcen mit Hilfe ihrer riesigen Metallbohrer, die von der Karavan zur Verfügung gestellt werden, auszubeuten.',
        title: 'Karavan',
      },
      text:
        'Zwei Mächte, unterschiedlich, aber beide geheimnisvoll, versuchen, das Schicksal von Atys zu kontrollieren: die Karavan, die über die Homins wachen und das Wort der Göttin Jena verbreiten, und die Kamis, die Bewahrer der Pflanzen und des weltweiten Gleichgewichts. Beide besitzen einzigartige Kräfte, durch die sie die Homins, die ihren göttlichen Gesetzen folgen, beschützen und ihnen helfen. Seit Jahrhunderten überwachen sich diese beiden Mächte gegenseitig ohne Konflikte. Ihr Schutz wird Euch auf Eurer Reise auf Atys eine große Hilfe sein, und ihr Zorn kann tödlich sein... Wählt also sorgfältig Eure Seite.\n\nWenn Ihr Euch jedoch auf eigenes Risiko entscheidet, also keinen der beiden Mächte zu dienen, so stehen Euch andere Türen offen. Ihr könnt euch für die Neutralität in der Religion entscheiden und im religiösen Kampf nicht Partei ergreifen, oder ihr könnt Trytonist werden und im Verborgenen kämpfen, um die Hominheit von den Mächten zu befreien, oder ihr könnt euch sogar den Reihen der Marodeure anschließen und sowohl gegen die Mächte als auch gegen die Nationen kämpfen. Euer Weg könnte euch sogar auf den Weg der Ranger führen, die sich auf die Vereinigung der Mächte und der Nationen verlassen, um den Feind, die Kitin, zu bekämpfen.',
      title: 'Die Fraktionen',
      marauder: {
        text:
          'Dazu gehören alle Homins, deren Ziel es ist, Atys zu erobern, indem sie gegen die Herrschaft der Mächte und Nationen rebellieren. Die Marodeur-Fraktion wurde aus den Klans der Homins geboren, die während des ersten großen Schwarms in den Alten Landen zurückgelassen wurden.\n\nDiese Organisation, die vom Überleben der Stärksten regiert wird, wird von Kampflust, Eroberung und Überleben beherrscht. Mit der Zeit haben die Marodeure ihre eigene Sprache entwickelt: Marund.\n\nDie Marodeure besitzen kein Territorium in den Neuen Landen; daher haben sie ein Lager in einer Region errichtet, die sowohl von den Fyros als auch von den Matis beansprucht wird.\n\nJeder Homin, der bereit ist, in die Marodeur-Fraktion einzutreten, muss einen Treue-Ritus ablegen und schwören, dem Ruf Melkiars, der Schwarzen Varnix, dem Kriegsführer der Marodeur-Klans, zu folgen. Im Gegenzug profitieren sie von speziellen Diensten für die Wiederauferstehung und Teleportation rund um Atys, und ihre Krieger werden mit einzigartigen Belohnungen belohnt.\n\nMarodeure versuchen regelmäßig, durch Kämpfe Außenposten zu erobern, um so ihre wertvollen Ressourcen zu nutzen.',
        title: 'Marodeure',
      },
      trytonist: {
        text:
          'Dazu gehören alle Homins, die Atys vom Joch der Mächte befreien wollen, da sie die attraktiven Belohnungen, die sie anbieten, als bloße Verlockungen zur Versklavung der Hominheit betrachten.\n\nDer Führer der Trytonisten ist Elias Tryton, ein mysteriöses Wesen, das von den einen als Abtrünniger der Karavan und von den anderen als Ehemann der Göttin Jena angesehen wird.\n\nDa die Trytonisten von den Karavan gejagt und von den Kamis kaum geduldet werden, müssen sie sich versteckt halten, um zu überleben. Die Trytonisten sind derzeit über Gilden aller Religionen und Nationen verbreitet und haben ein geheimes Netzwerk mit Codenamen aufgebaut.\n\nTrytonisten erhalten ihre Anweisungen von der Gilde von Elias, deren Anführer Nicho in direktem Kontakt mit Elias Tryton steht.\n\nWarnung: Diese Fraktion ist derzeit nur durch Rollenspiele spielbar.',
        title: 'Trytonist',
      },
    },
  },
  header: {
    forums: 'Forum',
    logout: 'ausloggen',
    playNow: 'jetzt spielen!',
    login: 'einloggen',
    cookies: {
      button: {
        decline: 'Nur notwendige Cookies.',
        accept: 'Alle Cookies akzeptieren.',
      },
      text: 'Ryzom verwendet Cookies für Analyse- und Marketingzwecke.',
    },
    webig: 'Web-Apps',
    billing: 'Account',
  },
  tos: {
    coc: 'Ryzom Verhaltenskodex',
    intro:
      'Die folgenden Regeln decken die meisten alltäglichen Situationen ab, sind aber dennoch nicht vollständig. Das Kundendienstteam hat das Recht, diese Regeln zu ändern, sollten neue Fälle auftreten, die einer weiteren Klärung bedürfen.\n\nSollte ein Fall zu spezifisch sein, als dass er in diesen Regeln auftaucht, dieser aber dennoch eine Intervention des Kundendiensts benötigt, kann dieser (der Kundendienst) handeln und jeder Zeit Verwarnungen oder Bestrafungen aussprechen und durchsetzen, ohne gezwungen zu sein, das gesamte restliche Ryzom-Team darüber zu informieren.\n\nWenn Sie einen Regelverstoß, bzw. eine Regelverletzung, feststellen, informieren Sie bitte umgehend den Kundendienst, indem sie eine der folgenden Möglichkeiten nutzen:\n\n- Das In-Game Ticket System;\n- Ein Privat Chat mit einem verfügbaren CSR: \n- In-Game Chat (/tell <CSR_name>)\n- Ryzom Chat\n- Eine E-Mail, adressiert an support@ryzom.com',
    title: 'Nutzungsbedingungen',
    content:
      '## Das Kundendienstteam\nDie Mitglieder des Kundendienstteams sind Freiwillige (Volontariat) und stehen unter der Geheimhaltungsvereinbarung (NDA). Es ist nicht erlaubt, die eigene Identität preiszugeben. Weder an andere Spieler, noch an andere Mitglieder des Ryzom-Teams. Wenn ein Spieler glaubt zu wissen, welche Identität sich hinter einem CSR (oder jedem anderem NDA Mitglied des Ryzom-Teams) befindet, ist es diesem Spieler strengstens untersagt diese Information an andere weiterzugeben. Ein Zuwiderhandeln würde bestraft werden. (Schwere Straftat, siehe [[#B. Verwarnungen und Bestrafungen|B. Verwarnungen und Bestrafungen]])\n\nDie Mitglieder des Kundendienstteams sind verantwortlich dafür die verschiedenen Probleme, bezogen auf das Spiel (technisch, disziplinarisch, usw.) zu lösen. Diese müssen sich jederzeit höflich und zuvorkommend verhalten. Sollten Sie eine Meinungsverschiedenheit mit einem Mitglied des Ryzom-Teams haben, können Sie eine Beschwerde an support@ryzom.com senden, welche dann ordnungsgemäß an den Leiter des Kundendiensts weitergeleitet wird. Jeglicher Missbrauch (z.B. ungerechtfertigte Beschuldigung) wird mit Bestrafungen geahndet.\n\nWenn ein Mitglied des Kundendiensts Sie kontaktiert, sind Sie verpflichtet den Anweisungen folge zu leisten und höflich und umgänglich zu handeln. Jeglicher Regelverstoß wird gemäß unserer Bestrafungs- und Eskalationsrichtlinien geahndet. (Siehe  **B. Verwarnungen und Bestrafungen**)\n\nWir erinnern Sie daran, dass die Mitglieder des Kundendiensts ihre Zeit (Spielzeit, aber auch reale Lebenszeit) dafür widmen, sicherzustellen, dass jeder in einer friedvollen Umgebung spielen kann: Versuchen Sie dies bitte in Erinnerung zu halten, wenn Sie ein Mitglied des Teams kontaktieren. Das gilt selbstverständlich für alle Teams: CSR-Team, Dev-Team, Event-Team, usw.\n\nDas Kundendienstteams unterliegt strikten Regeln, welche es den Mitgliedern des Teams untersagt mit anderen Spielern eine Gruppe zu gründen, mit diesen zu handeln, sich zu duellieren oder diese zu heilen/retten. Diese Regeln verbieten desweiteren das Ausnutzen der Befugnisse (als Mitglied des Support-Teams) für jegliche andere Zwecke, außer solchen, die im Rahmen der Funktion eines Mitglieds definiert sind. Jegliches Zuwiderhandeln würde zu einem Löschen des CSR-Accounts oder sogar des privaten Spiel-Accounts führen. Zusätzlich ist es den CSR’s nicht gestattet Tickets zu bearbeiten, die deren Gilde oder private Charaktere betreffen.\n\nAlle Entscheidungen die getroffen werden, sind kollektive Entscheidungen und daher können Sie, je nach Art des Problems, von mehreren Mitgliedern des Kundendiensts kontaktiert werden\n\n## A. Meinungsverschiedenheit mit einem Spieler\nIm Falle einer Meinungsverschiedenheit mit einem anderen Spieler wird erwartet, dass Sie selbst zu einer einvernehmlichen Einigung finden. Ein Ticket an den Kundendienst zu schicken sollte die allerletzte Maßnahme sein; es sollte nur geschickt werden, wenn Sie nicht in der Lage sind, die Situation selbst zu lösen und es einer dritten Partei bedarf.\n\nWenn Sie sich genötigt sehen ein Ticket zu schicken, ist es äußert wichtig, je nach Art des Problems, für weitere Bezugnahme, zunächst einmal Screenshots zu erstellen. **Damit diese akzeptiert werden, ist es essentiell wichtig, dass die Screenshots den gesamten Spiel-Bildschirm (In-Game) zeigen und in keinster Weise bearbeitet/verändert (beispielsweise zensiert) wurden.** Außerdem muss auf dem Bild (Screenshot) die Zuwiderhandlung, auf die Sie hinweisen wollen, deutlich zu erkennen sein. Bitte beachten Sie, dass eine offene Karte (im Spiel) auf dem Screenshot uns ermöglicht, das Spiel-Datum (Atys-Zeit) zu erkennen, welches wiederum in die echte Zeit umgerechnet werden kann. Neben Screenshots werden auch Videos akzeptiert. Für weitere Fragen, kontaktieren Sie uns bitte direkt im Spiel mit dem /tell Befehl, falls ein CSR online ist, im [Rocket-Chat](https://chat.ryzom.com), oder durch eine E-Mail an support@ryzom.com.\n\n**Bitte beachten**: Jeglicher Missbrauch des Ticket-Systems wird dokumentiert und bestraft.\n\n## B. Verwarnungen und Bestrafungen\nDas Kundendienstteam hat das Recht Verwarnungen oder Bestrafungen auszusprechen, abhängig von der Schwere eines Verstoß, der Reaktion des betroffenen Spielers auf das Kundendienstteam und dessen Vorgeschichte im Spiel (bezüglich vorheriger Verwarnungen und Bestrafungen). Einem Spieler kann ein Verweis, eine Verwarnung, eine Spielsperre zwischen 24 Stunden und mehreren Tagen, bis hin zu einem permanenten Bann (ein vollständiger Ausschluss aus dem Spiel) erteilt werden.\n\n\n### Strafmaß\n#### Geringfügigen Verstoß\n- Das Strafmaß für einen geringfügigen Verstoß beginnt mit einer offiziellen Verwarnung, dann folgt eine 24 Stunden Sperre, gefolgt von drei Tagen, gefolgt von einer Woche bis hin zu maximal zwei Wochen. Dies kann gefolgt von einer Stummschaltung (mute) sein, oder dieser vorausgegangen sein. Eine Stummschaltung wird am Ort des Geschehens/der Verletzung (Kanäle des Spiels, der Rocket Chat, das Forum, usw.) ausgesprochen.\n- Dieses Strafmaß wird nach einem Jahr ohne weitere Verstöße zurückgesetzt.\n\n#### Schwere Straftat\n- Das Strafmaß für eine schwere Straftat beginnt mit einer einwöchigen Sperre, gefolgt von einem Monat und schließlich einem permanenten Spielverweis (Bann). Im Anschluss kann ein Kick vom Server (erzwungene Verbindungsunterbrechung) erfolgen.\n- Dieses Strafmaß wird nach drei Jahren ohne weitere Verstöße (mit Ausnahme des permanenten Spielverweises) zurückgesetzt.\n\n#### Kritischer Verstoß\n- Endgültige Sperre des Accounts nach Abmahnung des Spielers per E-Mail.\n\n**Bitte beachten**: Sollte im Falle einer schweren und sofortigen Bestrafung (Sperre oder permanenter Spielverweis eines Accounts) der betreffende Spieler im Spiel nicht mehr erreichbar sein, wird dieser nur per E-Mail (die in seinem privatem Account hinterlegte E-Mail Adresse) kontaktiert. Deshalb ist es wichtig, dass Sie bei der Registrierung eine gültige E-Mail Adresse angeben. Wenn der betreffende Spieler keine E-Mail erhalten hat, oder er die Bestrafung anfechten möchte, kann dieser sich mit dem Kundendienstteam unter support@ryzom.com in Verbindung setzen.\n\n#### Rechtliche Informationen\nDie Sanktionen, die im Verhaltenskodex für Ryzom vorgesehen sind, gelten wie folgt:\n- Stummschaltungen und verbale Verwarnungen können durch jeden CSR ausgeführt werden.\n- Offizielle Verwarnungen (per E-Mail) werden durch alle CSR von GM aufwärts ausgesprochen.\n- Suspendierungen werden von CSR auf SGM-Niveau ausgesprochen.\n- Accountsperren werden von Winch Gate Limited beschlossen und durchgeführt.\n\n## C. Richtlinien des Verhaltenskodex\n### I. Höflichkeitsregeln\n#### I.1. Belästigung eines Spielers oder eines Mitglieds des Ryzom Teams\n**(Schwerer/leichter Straftatbestand: abhängig von dem Grad der Schwere des Vergehens entscheidet das Kundendienstteam über jeden einzelnen Fall)**\n\n- Provokation und Belästigung, ob verbal oder durch Aktionen im Spiel, sind innerhalb des Spiels und auch in Ryzoms Chatkanälen untersagt: Beleidigungen, Drohungen jeglicher Art (unter dem Deckmantel des Rollenspiels oder nicht), Provokationen per Emotes oder auf anderem Wege, das wiederholte Töten eines Charakters, dessen Lagerfeuer oder dessen Mektoubs (davon ausgenommen sind jegliche Handlungen während einer Schlacht um einen Außenposten), regelmäßiges oder wiederholtes Anlocken und Lenken von Mobs (“Train”) auf andere Spieler ist untersagt:… Diese Liste ist nicht ausschließlich.\n- Das Kundendienstteam behält sich das Recht vor eine Akte anzulegen, sollte es einen nachgewiesenen Fall von Belästigung geben, sowie die notwendigen Entscheidungen auf Grundlage der Härte der Beweise zu treffen.\n\n#### I.2. Verleumdung oder das Verbreiten von Gerüchten über ein Mitglied des Ryzom-Teams oder Winch Gate\n**(Schwerer/leichter Straftatbestand: abhängig von der Schwere des Vergehens entscheidet das Kundendienstteam über jeden einzelnen Fall)**\n- Es ist strengstens untersagt Gerüchte, falsche Anschuldigungen oder Lügen über ein Mitglied des Ryzom-Teams oder Winch Gate zu verbreiten, um deren Ruf zu schaden.\nDas Kundendienstteam behält sich das Recht vor, auf Grundlage der Härte der Beweise die entsprechenden Entscheidungen zu treffen.\n\n#### I.3. Beleidigungen, Unhöflichkeit, Respektlosigkeit gegenüber einem Spieler oder einem Mitglied des Ryzom-Teams\n**(Leichter Straftatbestand)**\n- Es ist strengstens untersagt, andere Spieler zu beleidigen, unhöflich zu sein oder ihnen gegenüber Respektlosigkeit zu zeigen, sei es direkt oder indirekt, unter dem Deckmantel des Rollenspiels oder nicht, und/oder in einer Sprache, die diese Spieler verstehen oder auch nicht verstehen können. Das Support-Team hat das Recht, die Schwere der Beleidigungen zu bestimmen und sie nach eigenem Ermessen zu bestrafen.\n\n#### I.4. Ungehorsam gegenüber den Anweisungen eines Kundendienstmitarbeiters\n**(Leichter Straftatbestand)**\n- Wenn sich ein Mitglied des Kundendiensts mit Ihnen in Verbindung setzt, müssen Sie so schnell wie möglich antworten und dessen Anweisungen sorgfältig Folge leisten, andernfalls droht Ihnen eine Strafe.\n\n#### I.5. Störung eines Ereignisses (Event) im Spiel\n**(Leichter Straftatbestand)**\n- Im Hinblick auf die Spieler, die an einer Veranstaltung teilnehmen, und auf das Eventteam, das sie vorbereitet hat, werden Spieler, die von dieser Veranstaltung nicht betroffen sind, gebeten, nicht zu kommen und die Veranstaltung nicht durch unangemessene oder irrelevante Äußerungen oder Handlungen zu stören. Jeder Missbrauch kann bestraft werden.\n- Wenn Sie an einer geplanten Veranstaltung (Event) teilnehmen möchten, wenden Sie sich bitte an das Veranstaltungsteam (events@ryzom.com), das die Zulässigkeit Ihrer Anfrage beurteilt.\n\n#### I.6. Gemeinsame Konto (Account) Benutzung\n**(Kein Straftatbestand)**\n- Vom Teilen eines Spielerkontos mit einem anderem Spieler wird dringend abgeraten. Das Kundendienstteam wird sich von den Problemen, die sich aus einer solchen Weitergabe ergeben könnten (Diebstahl von Gegenständen, Aneignung des Accounts durch einen anderen Spieler, usw.) distanzieren und kann dabei nicht helfen.\n\n#### I.7. Unangebrachtes Verhalten in einem Chat Kanal des Spiels (inklusive Spielserver, IRC, Foren und Rocket-Chat)\n**(Leichter Straftatbestand)**\n- Universumskanäle sind in erster Linie Kanäle der gegenseitigen Hilfe (Fragen & Antworten über das Spiel, Hilfeersuchen, usw.). Sie sind auch offen für Diskussionen, die für die meisten Mitglieder der Spielergemeinschaft von Interesse ist. Wenn nicht, benutzen Sie bitte /tell oder einen privaten Kanal. Die Kanäle werden moderiert, um eine Überflutung (Flooding) zu verhindern. Trolling, Spamming (Wiederholung oder Veröffentlichung mehrerer Nachrichten hintereinander innerhalb weniger Minuten) und Flaming sind verboten.\n- Belästigungen, Drohungen oder andere beunruhigende Handlungen gegenüber einem anderen Spieler oder Mitglied eines Ryzom-Teams ist verboten.\n- Unhöfliche, beleidigende, diffamierende, obszöne, fremdenfeindliche und antisemitische Ausdrücke sind verboten. Ebenso ist jede sexuell explizite Äußerung verboten.\n- Sie dürfen sich nicht als Mitarbeiter von Winch Gate, CSR oder anderen Mitgliedern des Ryzom-Teams ausgeben.\n- Sie dürfen keine Gesetze verletzen, egal ob lokal, national, europäisch oder international.\n- Die illegale Übertragung von Dokumenten, Dateien oder ähnlichem ist über das Winch Gate Netzwerk verboten.\n- Wenn sich ein CSR-Mitglied im Spiel oder über einen anderen Ryzom-Dienst mit Ihnen in Verbindung setzt, müssen Sie die Anweisungen des CSR-Mitglieds befolgen und wahrheitsgemäß antworten.\n- Sie dürfen die Dienste von Winch Gate nicht für andere als die in der Spielwelt erlaubten Aktivitäten nutzen.\n- Wenn die Informationen in Ihrem Profil unvollständig oder fehlerhaft sind, können Sie in einigen Fällen nicht in den Genuss der angebotenen Unterstützung kommen, da die CSRs Sie nicht richtig identifizieren konnten.\n- Persönlichen Daten von anderen Spielern, ob im Spiel, in den Foren von Winch Gate, im IRC oder im Rocket-Chat dürfen nicht bekanntgemacht werden.\n\n##### Höflichkeit im Spiel\n- Sie dürfen nicht gegen den Verhaltenskodex verstoßen.\n- Jegliche Belästigung, in welcher Form auch immer, ist untersagt.\n- Sie dürfen einen anderen Spieler während seiner Spielphasen nicht behindern.\n- Respektvolles Verhalten gegenüber anderen Spielern und den Mitgliedern des Ryzom Teams wird erwartet.\n- Sie dürfen sich nicht hinter einer "Rolle" verstecken, um die Regeln der Höflichkeit zu verletzen.\n\n##### Forum Regeln\n- Sie dürfen nicht gegen den Verhaltenskodex verstoßen.\n- Sie müssen sich klar ausdrücken und höflich handeln.\n- Sie dürfen weder andere Spieler noch ein Mitglied eines Ryzom-Teams belästigen oder beleidigen.\n- Cross-Posting (mehrmaliges Versenden derselben Nachricht, um Aufmerksamkeit zu erregen) ist verboten.\n- Sie sind verpflichtet, jede illegale Überschreitung der Namensrichtlinien zu melden.\n- Werbung für Websites und dergleichen ohne Verbindung zu Ryzom ist verboten.\n\n### II. Namensrichtlinien\nDiese Regeln zur Charakterbenennung dient als Grundlage zur Charaktererschaffung und der Wahl eines Pseudonyms für den Charakter. Diese Regeln wurden vom Supportteam erstellt und unterliegen der Verantwortung der Teamleitung.\n\n#### Wahl des Namens\n- Zum Ende des Charaktererschaffungsprozesses wird ein Name ausgewählt. Es wird empfohlen, diesen von der Spielwelt inspirieren zu lassen, zum Beispiel indem man sich mit den Überlieferungen (Lore) bekannt macht, wodurch sich der Charakter besser in das Universum von Atys einfügen kann.\n- Falls es an Ideen fehlt oder der Wunschname bereits vergeben ist kann auch der integrierte Namensgenerator verwendet werden. Weniger rollenspiellastige aber dennoch regelkonforme Namen werden ebenfalls akzeptiert.\n\n#### Ändern des Charakternamens\n- Falls euer Charaktername nicht mit den unterstehenden Regeln konform geht, so ist es möglich, daß ein Supportmitarbeiter euch zur Änderung des Namens auffordert.\n- Weiterhin ist auch möglich selbst einen Namenswechsel zu beantragen (Gründe hierfür können rollenspielbedingt sein, oder auch um Anonymität zu wahren oder Spott zu entgehen - die Liste dieser Gründe ist nicht restriktiv). Euer Ansuchen wird dann vom Supportteam gewertet.\n- Die Anzahl möglicher Namenswechsel hängt von eurem Account ab.\n    - Free to Play Account : Pro Charakter kann ein Namenswechsel erfolgen.\n    - Premium Account: Ein möglicher Namenswechsel pro Charakter alle sechs Monate.\n\n#### Namensregeln und Sanktionen bei Verstoß\nAnmerkung: Sollte ein Spieler während der Charaktererschaffung mehrfach im Bereich ‘sehr anstößig’ gegen die untenstehenden Namensregeln verstoßen, so sind Sanktionen wie im Verhaltenskodex beschrieben möglich.\n\n##### Sehr anstößige Namen:\n**Der Name wird umgehend durch einen Standardnamen ersetzt und der Spieler wird über die Änderung benachrichtigt.**\n\n- Die Verwendung von Namen darf nicht verroht, rassistisch, ethnisch beleidigend, obszön, sexuell oder drogenrelevant sein. Homonyme solcher Worte sind gleichfalls nicht gestattet. Namen die allgemeine Schimpfworte, Schimpfnamen oder Bezug zu Körperteilen enthalten dürfen nicht verwendet werden. Diese Regelung betrifft alle im Spiel verwendeten Sprachen.\n- Namen dürfen nicht dazu dienen einem anderen Spieler zu schaden oder diesen zu imitieren. Namen dürfen nicht dazu dienen einen Mitarbeiter von Winch Gate Property Limited oder einen Ryzom Team Mitarbeiter oder Freiwilligen zu imitieren oder diesen zu schaden.\n\n##### Anstößige Namen:\n**Der Charakter wird innerhalb von 24 Stunden umbenannt. Die Warteperiode dient dazu, dem Spieler die Möglichkeit zu geben einen neuen Namen vorzuschlagen. Dies betrifft ausschließlich Charaktere die nach der Veröffentlichung der Namensregelungen erschaffen wurden.**\n\n- Namen die phonetisch anstößig sind oder den hier beschriebenen Regeln widersprechen dürfen nicht verwendet werden.\n- Namen mit historischem, religiösen, okkulten oder ähnlichem Bezug dürfen nicht verwendet werden (Beispiele: Jesus, Lucifer, Nosferatu, Kreuzigung, Exorzismus)\n- Namen, die sich auf Titel beziehen, dürfen nicht verwendet werden. (Beispiele: Meister, König, Königin, Herr, Mutter, Gott - die Liste ist nicht restriktiv).\n\n##### Unzulässige Namen:\n**Der Charakter wird binnen dreier Tage umbenannt, wobei die Wartezeit dazu dient dem Spieler zu ermöglichen einen neuen Namen vorzuschlagen. Dies betrifft ausschließlich Charaktere die nach der Veröffentlichung der untenstehenden Regeln erschaffen wurden.**\n\n- Namen, die einem Copyright unterliegen, Markennamen, Waren- oder Produktnamen dürfen nicht verwendet werden. (Beispiele: Fujiansu, Whisky, Tylenol, Toshiba).\n- Die Verwendung von Eigennamen von Prominenten, Politikern oder populären fiktiven Personen ist untersagt (Beispiele: Angelina Jolie, Donald Trump, James Bond)\n- Eigennamen von Personen, die aus dem Universum von Atys stammen dürfen nicht verwendet werden (Beispiele: Jena, Tryton, Yrkanis, Mabreka).\n- Unter diese Regelung fallen ebenfalls rückwärts geschriebene Namen der gleichen Kategorie (Beispiele: Anej, Notyrt, Sinakry, etc.).\n- Namen, die einem anderen populären Genre entstammen (z.B. mittelalterliche Fantasy, Science Fiction oder ähnliche) dürfen, egal ob fiktiv oder real, nicht verwendet werden, auch dann nicht, wenn die Schreibweise leicht verändert wird (Beispiele: Skywalker, Gandalf, Flintstones, Obiwan).\n- Eigennamen, Gegenstandsnamen oder allgemeine Worte die nicht zum Universum von Ryzom passen dürfen nicht verwendet werden (Beispiele: Handy, Videorekorder, Auto)\n\n\n#### Besonderheiten bei Gildennamen\nPrinzipiell gelten alle Regeln die für Charaktere gelten, auch für Gilden. Es gibt jedoch einige Ausnahmen:\n- Der Name einer Gilde darf den Eigennamen von Orten auf Atys enthalten, um Rollenspielbezug herzustellen. (z.B. ‘Die Wächter von Fairhaven’)\n- Der Name einer Gilde darf, wiederum zu Rollenspielzwecken, den Eigennamen einer überlieferten atysianischen Persönlichkeit enthalten, sofern ansonsten alle Namensregeln eingehalten werden (z.B. ‘Die Kinder des Elias’ oder ‘Die Jünger Mabrekas’, jedoch nicht ‘Das Einmachglas von Tryton’).\n\nSollte euch im Spiel ein Name begegnen, der nicht den Namensregelungen entspricht, so könnt ihr dies mittels eines Tickets oder auch direkt durch Kontaktieren eines Supportmitarbeiters im Spiel mitteilen (Im Spiel: Tippt im Chat “/who gm” (ohne Anführungszeichen) um eine Liste aller Supportmitglieder die online sind zu erhalten. Diese Liste wird in eurer Systeminformation angezeigt).\nFalls ihr einer Umbenennung widersprechen möchtet, so könnt ihr dies per E-Mail an support@ryzom.com tun. Jedes Anliegen wird aufmerksam bearbeitet, Missbrauch wird gegebenenfalls mit Sanktionen versehen.\n\n### III. Verletzung der Spielintegrität\n#### III.1. Serverdaten gehackt, entschlüsselt oder assimiliert (Kritischer Straftatbestand)\nDie Änderung des Clients oder seiner Kommunikationsmittel mit dem Server, um einen Vorteil zu erlangen, ist strengstens untersagt und wird mit einem permanenten Spielverbot geahndet.\n\n#### III.2. Account-Piraterie (Kritischer Straftatbestand)\nDie Kontrolle über ein Konto, das nicht Ihnen gehört, mit jeglichen Mitteln zu übernehmen, ist strengstens verboten und wird mit einem permanenten Spielverbot geahndet.\n\n#### III.3. Nutzung eines Bots (Schwerer Straftatbestand)\n- Verwendung eines Drittanbieterprogramms zum Ausführen von geskripteten Aktionen.\n- Die Verwendung von Makros, die nicht Teil des integrierten Makrosystems von Ryzom sind, um Ihren Charakter ganz oder teilweise von Ihrer direkten Kontrolle unabhängig zu machen, ist strengstens untersagt.\n- Diese Regel gilt auch für die Glücksräder der Hauptstädte.\n- Die Kontrolle mehrerer Charaktere durch die Verwendung eines Drittanbieterprogramms ist strengstens untersagt. Wenn das Support-Team während einer Überprüfung nicht erkennen kann, ob ein Spieler diese Regel missbraucht, muss der Spieler den Nachweis erbringen, dass er keine Software von Drittanbietern verwendet, um mehrere Charaktere gleichzeitig zu kontrollieren.\n\n#### III.4. Multiboxing (Schwerer Straftatbestand)\nDie Anzahl der gleichzeitig verbundenen Konten für eine IP-Adresse ist auf vier begrenzt. Der Kundensupport kann jedoch im Einzelfall Ausnahmen machen (z.B. Großfamilie, öffentlicher Computer etc.). Wenn Sie der Meinung sind, dass Sie freigestellt werden sollten, wenden Sie sich bitte an den Support unter support@ryzom.com und geben Sie den Grund für Ihre Anfrage sowie die Namen aller betroffenen Konten/Accounts an.\n\n#### III.5. Exploit (Kritischer/Schwerer/Leichter Straftatbestand, je nach Fall)\n- Das Konzept des Exploits umfasst jede Nutzung eines Drittanbieterprogramms, eine Modifikation oder Neukompilierung des Clients oder einen Bug auszunutzen, um einen Vorteil gegenüber den anderen Spielern zu erlangen.\n- Die Verwendung von Exploits ist verboten und wird durch das Kundendienstteam abgestraft, welches das Recht hat, über die Schwere des festgestellten Exploits zu entscheiden. In diesem Zusammenhang sind wiederholte Verstöße ein erschwerender Faktor.\nWenn Sie den Verdacht haben, dass ein Exploit verwendet wird, sind Sie verpflichtet uns über einem der oben genannten Wege darüber in Kenntnis zu setzen.\n\n#### III.6. Ein “Aggro-Mob” (Aggressiver NPC Gegner) (Aggro-Schleppen) außerhalb von Aussenpostenschlachten (Leichter Straftatbestand)\n- Es ist verboten, einen oder mehrere Mobs absichtlich auf einen anderen Spieler zu ziehen, um ihn zu töten oder zu belästigen. Wenn ein Spieler sieht oder darüber informiert wird, dass er/sie unbeabsichtigtes Aggro-Schleppen begangen hat, muss er/sie sich entschuldigen und sein/ihr Bestes tun, um die betroffenen Charaktere bei Bedarf wiederzubeleben.\n- Wenn Sie Opfer von absichtlichem Aggro-Schleppen sind und es beweisen können (z.B. ein Screenshot eines Tells, der zeigt, dass die Person, die Sie kontaktiert haben, keine Anzeichen von Reue zeigte und nicht zurückkommen wollte, um Sie wiederzubeleben), können Sie das Kundendienstteam auf eine der oben genannten Weisen kontaktieren. Wir werden dann eine Akte anlegen und entsprechend handeln. Auch hier werden wiederkehrende Verstöße berücksichtigt.\n- Aggro-Dragging während eines Außenpostenkampfes wird toleriert (siehe **III.9 - Missachtung der PvP-Regeln**, siehe \'**III.6 - Allgemeine PvP-Regeln**).\n\n#### III.7. Diebstahl eines Mobs (Kill stealing) (Leichter Straftatbestand)\n- Das Töten eines Mobs, der bereits von einem anderen Spieler oder einer Gruppe von Spielern angegriffen wird, gilt als Diebstahl und ist verboten.\n- Wenn nach dem Versuch, die Situation zwischen den beteiligten Parteien zu klären, keine Lösung gefunden wird, können Sie sich über die oben genannten Wege an den Kundendienst wenden. Bevor Sie jedoch mit einem CSR Kontakt aufnehmen, sollten Sie zunächst versuchen, mit dem/den betroffenen Spieler(n) zu kommunizieren und es selbst zu lösen!\n- Im Falle eines Bosses oder eines Named Mobs lesen Sie bitte den folgenden Abschnitt.\n\n#### III.8. Regeln für Bosse und Namhafte Gegner (Leichter Straftatbestand)\na) Diebstahl von Bossen oder Namhaften Gegnern\n- Ein Boss oder Benannter Mob gehört dem ersten Team, das ihn angreift und damit reserviert, solange die Sperre aktiv ist. (Siehe https://app.ryzom.com/app_forum/index.php?page=topic/view/27212 für weitere Informationen.)\n- Ein zweites Team darf den Boss oder Namhaften Gegner nur angreifen, wenn dieser noch nicht reserviert ist. Um dies zu erreichen, kann das zweite Team folgendes tun:\n- *Kämpfe gegen das erste Team im PvP;\n- *Nutzen Sie den Vorteil der Abreise des ersten Teams, das automatisch den Boss oder Benannten Gegner freischaltet. Bitte beachten Sie, dass, wenn nur noch ein Spieler von dem Team, das den Boss oder den Namhaften Gegner reserviert hat, am Leben oder anwesend ist, so wird dieser Boss, bzw. der Namhafte Gegner, für andere Teams wieder freigegeben.\n- Aggro-Schleppen, das darauf abzielt die andere Mannschaft zu schwächen, ist verboten und daher strafbar (vgl. III.6).\n- Ein Boss oder Namhafter Gegner gilt als frei, solange er nicht von einem Team gesperrt ist.\n- Die Anziehungskraft von Boss oder Named durch eine einzelne Person, d.h. ohne Team, ist zwar möglich, aber nicht ausreichend, um die Sperre auszulösen. Das Wiederheranholen des Bosses oder Namhaften Gegners in diesem Fall gilt nicht als Diebstahl.\nb) Camping an Bossen oder Plätzen von Namhaften Gegnern\n- Das Warten (Campen) an Spawnplätzen von Bossen, bzw. Namhaften Gegnern, ist nicht reglementiert, da es oft unmöglich ist, dies nachzuweisen.\n\n#### III.9. Missachtung der PvP-Regeln (Leichter Straftatbestand)\nIn Ryzom ist PvP (Spieler-gegen-Spieler) zu jeder Zeit einvernehmlich. Die Aktivierung des PvP-Tags, das Betreten einer PvP-Zone oder die Teilnahme an einem Außenposten-Kampf impliziert, dass Sie die Konsequenzen akzeptieren. Um es einfach auszudrücken: Wenn Sie nicht an PvP-Aktionen teilnehmen wollen, dann liegt es in auch Ihrer Verantwortung den PvP-Tag nicht zu aktivieren und nicht in PvP-Zonen zu gehen.\n\na) Fraktions-PvP\n- Ein Spieler mit aktiviertem PvP-Tag akzeptiert die daraus entstehenden Konsequenzen unabhängig von den Umständen.\n- z.B.: während des Buddelns (Harvesting) getötet zu werden, in den Ställen, während man AFK (nicht am Computer) ist, an einem Wiederbelebungspunkt in der Nähe eines Vortex.\n\nWenn ihr trotz eurer Markierung den Kampf vermeiden wollt, könnt ihr warten, bis das Detagging stattfindet, euch verstecken, einen Teleport benutzen, den Ort oder die Region wechseln, etc.\n\nb) Regions-PvP\n- Der Spieler, der eine PvP-Zone betritt, akzeptiert die Konsequenzen, unabhängig von den Umständen. Eine Ausnahme sind jedoch Tötungen an Wiederbelebungspunkten in der Nähe von Vortexen. Diese sind verboten und strafbar, wenn sie nachgewiesen werden. Wenn Sie für Bereichs-PvP markiert sind und Opfer einer Tötung bei einem Vortex-Respawn (Wiederbelebungspunkt) sind, können Sie uns über einen der oben beschriebenen Wege kontaktieren. In diesem Fall wird dringend empfohlen, dass Sie einen Screenshot hinzufügen, um Ihren Anspruch zu untermauern (bitte stellen Sie sicher, dass das Fenster SYSTEM INFOS im Bild sichtbar ist).\n\nc) Außenpostenkämpfe (PvP)\n- Der Spieler, der in einen Außenpostenkampf eintritt und eine Seite wählt, akzeptiert die Konsequenzen, unabhängig von den Umständen.\n- Ausnahmen sind jedoch Tötungen an Wiederbelebungspunkten in der Nähe eines Vortexes. Diese sind verboten und werden bestraft, wenn sie nachgewiesen werden können. Wenn Sie als Außenpostenkämpfer markiert sind und Opfer eines Tötungsangriffs bei einem Wirbel oder einem Wiederbelebungspunkt sind, können Sie uns über einen der oben beschriebenen Wege kontaktieren. In diesem Fall wird dringend empfohlen, dass Sie einen Screenshot beifügen, um Ihren Anspruch zu untermauern.\n- Nicht als Kämpfer markierte Charaktere müssen während des Kampfes entfernt werden, da sie die Zielfindung der Protagonisten (der für den Kampf markierten Charaktere) behindern. CSR’s können diese nicht markierten Spieler bei Bedarf vom Ort des Geschehens entfernen.\n- Wenn Sie trotz Ihrer PvP-Markierung den Kampf vermeiden wollen, können Sie warten, bis das Detagging stattfindet, sich verstecken, einen Teleport benutzen, den Ort oder die Region wechseln, usw.\n\n##### Regeln für Außenposten (OP)\n\na) CSR’s werden in der Regel nicht bei Schlachten um einen Außenposten anwesend sein. Wenn Sie jedoch einen Verstoß gegen die Regeln des Anstands und des Verhaltens feststellen, können Sie sich sofort an den Kundendienst wenden.\n\nb) Wenn Sie in einer Region, in der ein Außenpostenkampf stattfindet, in der Landschaft stecken geblieben sind, versuchen Sie zuerst, sich zu teleportieren. Wenn Sie keinen Erfolg haben, müssen Sie sich an den Kundendienst wenden und Ihre Situation beschreiben. Ein CSR wird dann helfen, Sie zu befreien.\n\nc) Alles, was unter die normale Spielmechanik fällt, ist in Kampfzonen erlaubt, vorausgesetzt, dass alle Beteiligten für PvP markiert sind.\n\n- Ein Mektoub-Packer könnte sterben. Indem Sie es in eine Kampfzone bringen, setzen Sie es Risiken aus.\n- Spawn-Camping ist wegen der Sicherheitszone um die Respawn-Punkte (Wiederbelebungspunkte) fast unmöglich und somit erlaubt. Wenn Sie sterben und Ihre Feinde sich in der Nähe eures Wiederbelebungspunktes befinden, müssen Sie entweder auf die Regeneration in der sicheren Zone warten oder einen anderen Wiederbelebungspunkt/Teleportationspakt wählen, um wieder in den Kampf einzutreten.\n- Während der Außenpostenschlachten sind Aggro-Schleppen (Aggro Dragging) und das Töten von Yelks in der Nähe des Gefechts gültige und erlaubte Kampfstrategien.\n\nd) Blockierung von Außenposten\n- Eine Kriegserklärung gegen Ihren eigenen Außenposten oder gegen den eines Ihrer Verbündeten, oder eine Kriegserklärung Ihrer Verbündeten gegen Ihren eigenen Außenposten, um zu verhindern, dass jemand anderes diesem Außenposten den Krieg erklären kann, wird dies als Exploit betrachtet und ist strengstens verboten.\n- Die Übertragung der Eigentumsrechte eines Außenpostens von einer Gilde auf eine andere Gilde ist generell erlaubt. Die Übertragung gilt dann als verboten, wenn dieser Vorgang wiederholt auftritt und als Blockierung eines Außenposten gewertet werden kann.\n\ne) Wiederholte falsche Kriegserklärung/Scheinerklärung\n- Werden mehreren Kriegserklärungen gegen verschiedene Außenposten auf einmal ausgesprochen, und auch nur eine dieser Kriegserklärungen aufgrund von einer nicht ausreichenden Anzahl von Spielern nicht stattfinden kann, gilt dies als Belästigung. Dasselbe gilt für wiederholte falsche Kriegserklärungen auf einen einzelnen Außenposten.\n\n**Bitte beachten**: Jeder der oben genannten Punkte zielt darauf ab, den taktischen Entscheidungen bei Angriffen ein gewisses Maß an Flexibilität zu geben und gleichzeitig das Gleichgewicht mit dem Recht der Außenpostenbesitzer, ohne Belästigung spielen zu können, zu wahren. Wenn Sie der Meinung sind, dass Ihre Gilde mit einer dieser Straftaten belastet werden könnte, machen Sie bitte einen Screenshot, der die Anzahl der Spieler zeigt, die während des Angriffs anwesend waren, um diesen im Ernstfall dem Kundendienstteam zeigen zu können.\n\n### IV. Unangemessene Werbung\n#### IV.1. Gold verkaufen (Spielwährung)\n- Goldverkäufer haben nichts mit dem Spiel und dessen Umgebung/Inhalten zu tun. Daher sind diese strengstens verboten. Der schuldige Spieler wird mit einem sofortigen, permanenten Spielverbot (Bann) und einer ebenfalls sofortigen, erzwungenen Spielunterbrechung (Kick) bestraft.\n\n#### IV.2. Werbung für ein Produkt oder eine Website, die nichts mit Ryzom zu tun haben (Leichter/Schwerer Straftatbestand: je nach Art der Werbung wird das Kundendienstteam alle Fälle einzeln prüfen).\n- Werbung für Websites, die nichts mit Ryzom und dessen Inhalten zu tun haben, ist verboten. Der schuldige Spieler wird sofort stumm geschaltet und entsprechend bestraft.\n- Der Kundendienst prüft jeden Verstoß und entscheidet über die Bestrafung, je nach Art der veröffentlichten Werbung.\n\n### V. Allgemeiner Betrug\n#### V.1. Sich als Mitglied eines Ryzom Teams oder Winch Gate Mitarbeiter auszugeben (Schwerer Straftatbestand):\n- Es ist strengstens untersagt, sich als Mitglied des Ryzom Teams oder von Winch Gate auszugeben, sei es im Spiel, im Forum von Ryzom, im IRC, im Rocket-Chat oder anderswo.\n\n#### V.2. Täuschungsversuch gegenüber einem Mitglied des Ryzom-Teams (Schwerer Straftatbestand, wenn der Spieler einen Vorteil erhält)\n- Der Versuch, ein Mitglied des Ryzom-Teams zu täuschen, sei es, um bereits erhaltene Gegenstände (z. B.: Event-Belohnung, Missionsgegenstand, Boss-Gegenstände, Marauder Boss-Stücke oder -Pläne ...) zu duplizieren, oder um diese für einen anderen Zweck zu verwenden, ist strengstens untersagt. Jegliches Fehlverhalten wird gemäß Absatz B. Verwarnungen und Bestrafungen geahndet.\n\n#### V.3. Sich als ein anderer Spieler ausgeben (Leichter Straftatbestand)\n- Es ist verboten, sich in irgendeiner Weise als ein anderer Spieler auszugeben, einschließlich des Vortäuschens, sein Reroll oder Alt / Twink (Zweitcharakter) zu sein oder in dessen Namen zu sprechen.\n\n#### V.3. Beanspruchung einer besonderen Beziehung zu einem Mitglied des Ryzom-Teams (Leichter Straftatbestand)\n- Es ist strengstens verboten, vorzugeben, eine privilegierte Beziehung zu einem Mitglied des Ryzom-Teams (einschließlich des Kundendienstes) zu haben, um diesem damit zu schaden, sich damit selbst vor anderen Spielern gut dastehen zu lassen, oder zu versuchen diesen Mitarbeiter zu beeinflussen.\n- Die Mitglieder der Ryzom-Teams unterliegen einer Geheimhaltungsklausel, so dass ihre Identität (persönliche und Spieleridentität) gewahrt bleibt. Darüber hinaus folgt jedes Mitglied einem internen Verhaltenskodex, der es ihm/ihr untersagt, einen Spieler gegenüber einem anderen Spieler zu bevorzugen.\n\n**Bitte beachten**: Sollte sich eine solche Beziehung als wahr erweisen, würden der Spieler und der betroffene CSR (oder das Mitglied eines anderen Ryzom-Teams) den Bestrafungen unterliegen, die in ihren jeweiligen Verhaltenskodizes vorgesehen sind.',
  },
  notFound: {
    link: 'zurück zu Startseite',
    text: 'Diese Seite wurde leider nicht gefunden.',
    title: 'Inhalt nicht verfügbar',
  },
  account: {
    forums: 'Foren',
    logout: 'Logout',
    welcome: 'Willkommen',
    webig: 'Web-Apps',
    billing: 'Account / Abonnement',
  },
  card: {
    playFree: {
      systemRequirements: 'Systemanforderungen',
      premium: {
        before: 'Abonniere ein kostenpflichtiges',
        link: 'Premium-Konto,',
        after: 'um von mehr Vorteilen zu profitieren!',
      },
      otherPlatforms: 'weitere Plattformen',
      text:
        'Registriere dich, lade das Spiel herunter und stürz dich hinein ins Abenteuer!',
      title: 'Jetzt kostenlos spielen!',
    },
    returning: {
      button: "So geht's zurück ins Spiel",
      getGoing: 'Und jetzt los, deine Freunde warten auf dich!',
      bold: 'Schon ein Weilchen her, oder?  \nSchön dich wieder zu sehen!',
      text:
        'Wir haben ein Paar Dinge zusammengetragen, die dir weiterhelfen können.',
      title: 'Kehrst du zu Ryzom zurück?',
    },
  },
  events: {
    noEvents: 'Keine Events gefunden. Versuche es später noch einmal.',
    title: {
      calendar: 'Event-Kalender',
      spotlight: 'Events im Fokus',
    },
    info: {
      timezone: 'Die Zeiten sind Deiner aktuellen Zeitzone angepasst',
    },
  },
};
