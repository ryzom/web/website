import axios from 'axios';
import Lang from 'ryzom-web-common/src/lang/en-US';
import fallback from './fallback/en-US';

export default async (context) => {
  let branch = 'next';
  if (context.env.profile === 'live') {
    branch = 'dist';
  }

  const result = await axios
    .get(context.env.i18nURL + '/i18n/public/' + branch + '/default/en')
    .then((result) => {
      return result.data;
    })
    .catch(() => {
      return fallback;
    });

  const footer = {
    footer: Lang.footer,
    header: Lang.header,
  };

  return { ...footer, ...result };
};
