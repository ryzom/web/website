import axios from 'axios';
import Lang from 'ryzom-web-common/src/lang/es-ES';
import fallback from './fallback/es-ES';

export default async (context) => {
  let branch = 'next';
  if (context.env.profile === 'live') {
    branch = 'dist';
  }

  const result = await axios
    .get(context.env.i18nURL + '/i18n/public/' + branch + '/default/es')
    .then((result) => {
      return result.data;
    })
    .catch(() => {
      return fallback;
    });

  const footer = {
    footer: Lang.footer,
    header: Lang.header,
  };

  return { ...footer, ...result };
};
