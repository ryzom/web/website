const dateTimeFormats = {
  'de-DE': {
    hourMinute: {
      hour: 'numeric',
      minute: 'numeric',
      hour12: false,
    },
    monthDay: {
      month: 'long',
      day: 'numeric',
    },
    day: {
      day: 'numeric',
    },
  },
  'en-EN': {
    hourMinute: {
      hour: 'numeric',
      minute: 'numeric',
      hour12: false,
    },
    monthDay: {
      month: 'long',
      day: 'numeric',
    },
    day: {
      day: 'numeric',
    },
  },
  'es-ES': {
    hourMinute: {
      hour: 'numeric',
      minute: 'numeric',
      hour12: false,
    },
    monthDay: {
      month: 'long',
      day: 'numeric',
    },
    day: {
      day: 'numeric',
    },
  },
  'fr-FR': {
    hourMinute: {
      hour: 'numeric',
      minute: 'numeric',
      hour12: false,
    },
    monthDay: {
      month: 'long',
      day: 'numeric',
    },
    day: {
      day: 'numeric',
    },
  },
  'ru-RU': {
    hourMinute: {
      hour: 'numeric',
      minute: 'numeric',
      hour12: false,
    },
    monthDay: {
      month: 'long',
      day: 'numeric',
    },
    day: {
      day: 'numeric',
    },
  },
};

module.exports = {
  mode: 'universal',

  srcDir: 'src/',

  server: {
    host: '0.0.0.0',
    port: 3100,
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'Ryzom - Free to Play Open-Source MMORPG | Windows, Mac & Linux',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'A freedom-oriented classless MMORPG, in a Science Fantasy setting focused on Roleplay, Live Events, complex crafting and dynamic environments',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
    // script: [{ src: 'https://unpkg.com/vue' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/styles/styles.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
    '@fullcalendar/core/main.css',
    '@fullcalendar/daygrid/main.css',
    '@fullcalendar/list/main.css',
    '@fullcalendar/bootstrap/main.css',
    'plyr/dist/plyr.css',
    'vue-datetime/dist/vue-datetime.css',
    'vue-loading-overlay/dist/vue-loading.css',
  ],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~/plugins/font-awesome.js',
    '~/plugins/marked.js',
    '~/plugins/vue-loading-overlay.js',
    '~/plugins/vue-datetime.js',
    { src: '~/plugins/full-calendar', ssr: false },
    { src: '~/plugins/flag-icons.js', ssr: false },
    // { src: '~/plugins/ryzom-header.js', ssr: false },
    // { src: '~/plugins/ryzom-footer.js', ssr: false },
    { src: '~/plugins/vue-plyr.js', ssr: false },
    { src: '~/plugins/lib-nuxt-i18n.js', ssr: false },
  ],
  /*
   ** Nuxt.js build-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'nuxt-i18n',
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/device',
    '@nuxtjs/gtm',
    // '@nuxtjs/google-analytics'
  ],
 gtm: {
    enabled: true,
    id: 'GTM-MSQFC3Z'
  },
  router: {
    middleware: ['auth', 'usergroup'],
  },

  /* googleAnalytics: {
    id: 'UA-150200849-1',
    dev: true,
    disabled: false
  }, */

  optimizedImages: {
    optimizeImages: true,
    // optimizeImagesInDev: true
    // defaultImageLoader: 'responsive-loader'
  },

  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
  },

  i18n: {
    vueI18n: {
      dateTimeFormats,
      fallbackLocale: 'en',
    },
    locales: [
      {
        code: 'de',
        file: 'de-DE.js',
      },
      {
        code: 'en',
        file: 'en-US.js',
      },
      {
        code: 'es',
        file: 'es-ES.js',
      },
      {
        code: 'fr',
        file: 'fr-FR.js',
      },
      {
        code: 'ru',
        file: 'ru-RU.js',
      },
    ],
    defaultLocale: 'en',
    lazy: true,
    langDir: 'assets/lang/',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      alwaysRedirect: false,
    },
    strategy: 'prefix_except_default',
  },

  auth: {
    strategies: {
      jwt: {
        _scheme: '~/auth/JwtRollingAuthStrategy',
        endpoints: {
          login: {
            url: '/auth',
            method: 'post',
          },
          renew: {
            url: '/auth/renew',
            method: 'get',
          },
          logout: false,
          user: {
            url: '/user',
            method: 'get',
            propertyName: false,
          },
        },
      },
      /* local: {
      endpoints: {
        login: {
          url: 'https://app.ryzom.com/api/auth.php',
          method: 'post',
          withCredentials: true,
        },
        logout: {
          url: 'https://app.ryzom.com/api/logout.php',
          method: 'post',
          withCredentials: true,
        },
        user: {
          url: 'https://app.ryzom.com/api/user.php',
          method: 'get',
          withCredentials: true,
        },
      },
      tokenRequired: false,
      tokenType: false,
      autoFetchUser: false,
    }, */
    },
    redirect: {
      logout: '/admin/login',
      login: '/admin/login',
      home: '/admin',
    },
    watchLoggedIn: true,
    rewriteRedirects: true,
  },

  env: {
    profile: process.env.PROFILE || 'dev',
    baseURL: process.env.BASE_URL,
    i18nURL: process.env.I18N_URL,
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL,
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: true,

    transpile: ['vue-plyr.m.js'],

    extend(config, ctx) {
      config.resolve.alias['plyr$'] = 'plyr/dist/plyr.polyfilled.js';
    },
  },
};
