# ryzom_website

> Ryzom Website

## Requirements

* [NodeJs 12](https://nodejs.org)
* [Yarn](https://yarnpkg.com)

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3100
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
